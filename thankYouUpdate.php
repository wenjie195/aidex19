<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/thankYou.php" />
<meta property="og:title" content="Thank you for Updating | Aidex" />
<title>Thank you for Updating | Aidex</title>

<link rel="canonical" href="https://aidex.sg/thankYou.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
 	<div class="width100 overflow same-padding menu-distance min-height-with-menu-distance text-center">
		<h1 class="aidex-h1">Thank You for Updating!</h1>
        <p class="thankyou-p">Due to current high number of data processing, we will notify you when you have <b>passed our eKYC process.</b></p>
		<img src="img/verification.png" alt="Verification" title="Verification" class="verification-img text-center">
   </div>
<?php include 'js.php'; ?>


</body>
</html>                 