<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/waitingApproval.php" />
<meta property="og:title" content="Waiting for Approval | Aidex" />
<title>Waiting for Approval | Aidex</title>

<link rel="canonical" href="https://aidex.sg/waitingApproval.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
 	<div class="width100 overflow same-padding menu-distance min-height-with-menu-distance text-center">
		<h1 class="aidex-h1">Waiting for Approval</h1>
        <p class="thankyou-p">The trading feature will be ready once we approve your eKYC. Thank you for your time.</p>
		<img src="img/thanks-for-waiting.png" alt="Thank you for your time" title="Thank you for your time" class="verification-img text-center">
   </div>
<?php include 'js.php'; ?>


</body>
</html>                 