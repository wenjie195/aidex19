<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/userEditCurrentPass.php" />
<meta property="og:title" content="Edit Current Password | Aidex" />
<title>Edit Current Password | Aidex</title>

<link rel="canonical" href="https://aidex.sg/userEditCurrentPass.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
 	<div class="width100 overflow same-padding menu-distance min-height-with-menu-distance mobile-change-one-column">


        <div class="two-right-content-div two-left float-left">
       <?php 
        $secondaryPass = $userDetails->getPasswordTwo();

        if($secondaryPass != "")
        {?>
        <h2 class="tab-h2">Secondary Password Secured</h2>
        <?php
        }
        else
        {
        ?>
				<form action="utilities/userAddSecondPassFunction.php" method="POST">
                    <h2 class="tab-h2">Add Secondary Password</h2>

                    <div class="input-div">
                        <p class="input-top-text">Password</p>
                        <input class="aidex-input clean password-input" type="password" placeholder="New Password" id="new_second_password" name="new_second_password" required>
                        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                    </div>  

                    <div class="input-div">
                        <p class="input-top-text">Retype Password</p>
                        <input class="aidex-input clean password-input" type="password" placeholder="Retype New Password" id="retype_new_second_password" name="retype_new_second_password" required>
                        <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                    </div>  

                    <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="submit">Submit</button>
                </form>
        <?php
        }
        ?>
        </div>    
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/secondary-password.png" class="width100" alt="Edit Password" title="Edit Password">
        </div>

    </div>

<!-- -->
 

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Secondary password updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update secondary password !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Opps, somethings goes wrong !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Password must be same with re-type password !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Password length must be more than 5";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunctionA()
{
    var x = document.getElementById("new_second_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_new_second_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>