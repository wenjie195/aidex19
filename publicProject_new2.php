<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/CompanySelection.php';
require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$companyDetails = getCompanySelection($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>

<script>
    function showInput() {
        $input = document.getElementById("user_input").value;
        //$myarr = array(); // initialize
        // for($i = 0; $i < $input; $i++) {
        //     $image = "<img src='img/star.png'/>";
        //     $myarr[] = $i;
        // }
        if($input==1){
             $myarr= ["<img src='img/star.png'/>"];
        }
        else if($input==2){
             $myarr= ["<img src='img/star.png'/>","<img src='img/star.png'/>"];
        }
        else if($input==3){
             $myarr= ["<img src='img/star.png'/>","<img src='img/star.png'/>","<img src='img/star.png'/>"];
        }
        else if($input==4){
             $myarr= ["<img src='img/star.png'/>","<img src='img/star.png'/>","<img src='img/star.png'/>","<img src='img/star.png'/>"];
        }
        else if($input==5){
             $myarr= ["<img src='img/star.png'/>","<img src='img/star.png'/>","<img src='img/star.png'/>","<img src='img/star.png'/>","<img src='img/star.png'/>"];
        }
        document.getElementById('display').innerHTML = $myarr;
    }
  </script>
</head>

<body class="body">
<!-- <?php //include 'header.php'; ?> -->
<?php include 'header-after-login.php'; ?>
<div class="width100 overflow menu-distance same-padding min-height-with-menu-distance">
	<div class="width100 text-center">
	</div>
    <!-- <h1 class="title-h1 text-center">Current Projects</h1>  -->
    <h1 class="title-h1 text-center">Current Projects<?php echo $userDetails->getFavoriteProject();?></h1>
    <h1 class="title-h1 text-center" style="display:none;"><?php echo $userDetails->getFavoriteProject();?></h1>

    <form>
        <!-- <input type="text" id="myInput" onkeyup="myFunction()"> -->
        <input type="text" name="message" id="user_input" onkeyup="showInput()">
    </form>

    <!-- <input type="submit" onclick="showInput();"><br/> -->
    <p><span id='display'></span></p><div>
    
    <div class="tab">
    </div>

    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>

                    </thead>
                </tr>
               <tr>
                    	<td class="th0-color color-td company-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
						<td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>

						<td class="th7-color color-td"></td>

                        <td class="th8-color color-td"></td>

                </tr>
                <tr>
                    	<td class="th0 company-td">Companies</td>
                        <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                        <td class="th2" title="Details about the products">Product Evaluation</td>
                        <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                        <td class="th4" title="Internal & External analysis">Industry</td>
						<td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                        <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                        <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                        <td class="th8" title="Overall growth of company">Petri Growth</td>

                </tr>
                <tr>
                    <?php
                    if($companyDetails)
                    {
                        for($cnt = 0;$cnt < count($companyDetails) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <!-- <td></td> -->

                            <td>
                            <?php $companyDetails[$cnt]->getCompanyName();?>
                            <div class="left-logo-div1">
                                <img src="public_img/<?php echo $companyDetails[$cnt]->getImage();?>" class="company-logo" alt="<?php echo $companyDetails[$cnt]->getCompanyName();?>" title="<?php echo $companyDetails[$cnt]->getBackground();?>">
                                
                            </div>                            
                            	
                            	<div class="right-name-div">

                                    <b class="font-weight900" title="<?php echo $companyDetails[$cnt]->getBackground();?>"><?php echo $companyDetails[$cnt]->getCompanyName();?></b>
                                    <?php $esValue = $companyDetails[$cnt]->getEstimatedValue();?>
                                    <p class="ev-p"  title="<?php echo $companyDetails[$cnt]->getBackground();?>">EV: RM<?php echo $eV = number_format($esValue, 0); ?></p>
                                </div>                                    
                               

                                <form method="POST" action="utilities/addFavoriteFunction.php" enctype="multipart/form-data">
                                    <div class="right-star-div">
                                        <!-- <input type="text" value="<?php //echo $userDetails->getFavoriteProject();?>" name="favorite"> -->
                                        <input type="hidden" value="<?php echo $userDetails->getFavoriteProject();?>" name="favorite_old">
                                        <?php $selected = $userDetails->getFavoriteProject();
                                        // if ($selected) {
                                        $selectedExp = explode(",",$selected);
                                        for ($i=0; $i < count($selectedExp) ; $i++)
                                        {
                                            $selectedArray[] = $selectedExp[$i];
                                        }
                                        if (in_array($companyDetails[$cnt]->getId(),$selectedArray )) 
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="favorite_selected" type="checkbox" value="<?php echo $companyDetails[$cnt]->getId();?>"><img src="img/favourite2.png" class="favourite-png" title="Favourite" alt="Favourite"></button><?php
                                        }
                                        else
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="favorite" type="checkbox" value="<?php echo $companyDetails[$cnt]->getId();?>"><img src="img/favourite.png" class="favourite-png" title="Favourite" alt="Favourite"></button>
                                        <?php
                                        }
                                        // }
                                        // }
                                        ?>
                                    </div>
                                </form>

                            </td>

                            <td>

                                <div class="right-star-div"></div>
                            </td>

                            <td>
                            	<div class="right-name-div">
                                    <b>
                                        <?php $companyDetails[$cnt]->getCompanyName();?>
                                    </b>
                                </div>
                                <div class="right-star-div">
                                    <img src="img/star.png" class="star-png">
                                </div>
                            </td>

                        </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </tr>

            </table>
        </div>
    </div>

	<div class="clear"></div>


</div>
</div>


<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background:#e8eef7;
}

/* Handle */
::-webkit-scrollbar-thumb {
  background: #bed6ff;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #bed6ff;
}
</style>

<?php include 'js.php'; ?>

</body>
</html>
