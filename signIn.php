<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/registerWithInvitation.php" />
<meta property="og:title" content="Register With Invitation | Aidex" />
<title>Sign In | Aidex</title>

<link rel="canonical" href="https://aidex.sg/registerWithInvitation.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'header.php'; ?> -->
<?php include 'header-after-login.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="width100 overflow same-padding min-height-with-menu-distance menu-distance">




    <div class="second-four-div width100 same-padding form-div">
    	
    	<img src="img/sign-in-icon.png" class="line-icon" alt="Sign In" title="Sign In">
        <p class="bold-subtitle-p" >
        	Login
        </p>
        <div class="two-left-visual-div two-right float-right margin-control">
        	<img src="img/sign-in.png" class="width100" alt="Sign In" title="Sign In">
        </div>
        <div class="two-right-content-div two-left float-left margin-control text-left">
        <form action="utilities/loginFunction.php" method="POST">
             	<div class="input-div">
                    <p class="input-top-text">Username</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Username" id="username" name="username" required>
                </div>
             	<div class="input-div">
                    <p class="input-top-text">Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Type Your Password" id="password" name="password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
                </div>

                <div class="clear"></div>

                <!-- <input type="checkbox" class="aidex-checkbox"><label class="aidex-label">Remember Me</label> -->

                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top dual-a-btm" name="loginButton">Sign In</button>
				<div class="width100 text-center some-margin-top">
                	<a  class="open-signup blue-link forget-link">Not yet have an account? Sign up here!</a>
                	<br><br>
                    <a  class="open-forgot blue-link some-margin-top forget-link">Forgot Password</a>
                    <!-- <br><br>
                    <a class="open-forgotusername blue-link some-margin-top forget-link">Forgot Username</a> -->
                </div>
       	</form>
        </div>
	</div>

    <div class="clear"></div>

    <div class="spacing-div"></div>

</div>

<!-- CSS -->

<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
</style>

<?php include 'js.php'; ?>

</body>
</html>