<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/registerWithInvitation.php" />
<meta property="og:title" content="Register With Invitation | Aidex" />
<title>Register With Invitation | Aidex</title>

<link rel="canonical" href="https://aidex.sg/registerWithInvitation.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'header.php'; ?> -->
<?php include 'header-after-login.php'; ?>

<div class="width100 overflow same-padding min-height menu-distance">

    <?php 
        // Program to display URL of current page. 
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
        $link = "https"; 
        else
        $link = "http"; 

        // Here append the common URL characters. 
        $link .= "://"; 

        // Append the host(domain name, ip) to the URL. 
        $link .= $_SERVER['HTTP_HOST']; 

        // Append the requested resource location to the URL 
        $link .= $_SERVER['REQUEST_URI']; 

        // Print the link 
        // echo $link; 
        ?> 

        <?php
        $str1 = $link;
        // $referrerUidLink = str_replace( 'http://localhost/aidex19/registerWithInvitation.php?referrerUID=', '', $str1);
        $referrerUidLink = str_replace( 'https://aidex.sg/registerWithInvitation.php?referrerUID=', '', $str1);
        // $referrerUidLink = str_replace( 'https://aidex.sg/testing/registerWithInvitation.php?referrerUID=', '', $str1);
        // echo $referrerUidLink;
    ?>

    <div class="second-four-div width100 same-padding form-div" id="signup-div">
    	<!-- <img src="img/sign-up-icon.png" class="line-icon" alt="Sign Up" title="Sign Up" > -->
        <img src="img/sign-up-icon.png" class="line-icon" alt="Sign Up" title="Sign Up" >
        <p class="bold-subtitle-p">Sign Up Now</p>

        <div class="two-left-visual-div two-left float-left margin-control">
        	<img src="img/sign-up.png" class="width100" alt="Sign Up" title="Sign Up">
        </div>

        <div class="two-right-content-div two-right float-right text-left margin-control">
            <!-- <form action="utilities/newUserRegisterFunction.php" method="POST"> -->
            <form action="utilities/newRegisterUserWithInvitationFunction.php" method="POST">
             	<div class="input-div">
                    <p class="input-top-text">Username</p>
                    <input class="aidex-input clean" type="text" placeholder="Username" id="register_username" name="register_username" required>
                </div>  
             	<div class="input-div">
                    <p class="input-top-text">Email</p>
                    <input class="aidex-input clean" type="email" placeholder="Email" id="register_email_user" name="register_email_user" required>
                </div>   
             	<div class="input-div">
                    <p class="input-top-text">Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Password" id="register_password" name="register_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>                 
             	<div class="input-div">
                    <p class="input-top-text">Retype Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                </div>  
             	<div class="input-div">
                    <!-- <p class="input-top-text" id="register_nationality" name="register_nationality" required>Country</p>
                    <select class="aidex-input clean">
                    	<option value="Singapore">Singapore</option>
                        <option value="Malaysia">Malaysia</option>
                    </select> -->
                    <p class="input-top-text">Country</p>
                    <select class="aidex-input clean" id="register_nationality" name="register_nationality" required>
                        <option>Please Select A Country</option>
                        <?php
                        for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                        {
                        ?>
                        <option value="<?php echo $countryList[$cntPro]->getEnName(); 
                        ?>"> 
                        <?php 
                        echo $countryList[$cntPro]->getEnName(); //take in display the options
                        ?>
                        </option>
                        <?php
                        }
                        ?>
                    </select>

                </div> 
                <div class="input-div">
                	<p class="input-top-text">Contact Number</p>
                    <input class="aidex-input clean" type="text" placeholder="Contact Number" id="register_phone" name="register_phone" required>
                </div>  

                <div class="clear"></div>

                <input class="aidex-input clean" type="hidden" value="<?php echo $referrerUidLink ?>" id="upline_uid" name="upline_uid">

                <div class="clear"></div>
				<div class="input-div">
                	<input type="checkbox" class="aidex-checkbox some-margin-top" required><label class="aidex-label">By ticking I agree to the <a class="blue-link open-terms">Terms of Services</a></label>
                </div>
                
                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="refereeButton">Create Account</button>
                <div class="width100 text-center some-margin-top">
                	<a class="open-login blue-link forget-link">Already have an account? Login now!</a>
                </div>
            </form>
        </div>  
        
        <div class="clear"></div>     
	</div>

    <div class="clear"></div>

    <div class="spacing-div"></div>

</div>

<!-- CSS -->

<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
</style>

<?php include 'js.php'; ?>

</body>
</html>