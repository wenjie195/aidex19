<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156204184-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-156204184-1');
</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://aidex.sg/img/fb-meta.jpg" />
<meta name="author" content="Aidex">
<meta property="og:description" content="The power of AIDEX artificial intelligence that revolutionizes ASEAN investments Digital Exchange. Our system allows clients to easily access money and generate returns with user-friendly platform." />
<meta name="description" content="The power of AIDEX artificial intelligence that revolutionizes ASEAN investments Digital Exchange. Our system allows clients to easily access money and generate returns with user-friendly platform." />
<meta name="keywords" content="AIDEX, ASEAN Investments Digital Exchange, AI, Digital Currency, Bitcoin, digital, cyptocurrency, digital assets, security, service, huobi, 	Ethereum, Ether, Litecoin, Namecoin, Peercoin, Singapore, etc">
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>