<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = null;
$userRows = null;
$conn = connDB();

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/resetPassword.php" />
<meta property="og:title" content=">Reset Password | Aidex" />
<title>Reset Password | Aidex</title>

<link rel="canonical" href="https://aidex.sg/resetPassword.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
    <div class="width100 overflow same-padding menu-distance min-height-with-menu-distance mobile-change-one-column">
        <div class="two-right-content-div two-left float-left">
            <form action="utilities/resetPasswordFunction.php" method="POST">
                <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">
                <h2 class="tab-h2">Reset Password</h2>

                <div class="input-div">
                    <p class="input-top-text">Verify Code</p>
                    <input class="aidex-input clean" type="text" placeholder="Verify Code" id="verify_code" name="verify_code" required>
                </div>  

                <div class="input-div">
                    <p class="input-top-text">New Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="New Password" id="new_password" name="new_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>  
                <div class="input-div">
                    <p class="input-top-text">Retype New Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                </div>  
                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="submit">Submit</button>
            </form>
        </div>    
        <div class="two-left-visual-div two-right float-right">
            <img src="img/edit-password.png" class="width100" alt="Edit Password" title="Edit Password">
        </div>
    </div>

<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
</style>

<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>