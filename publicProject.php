<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'header.php'; ?> -->
<?php include 'header-after-login.php'; ?>
<div class="width100 overflow menu-distance same-padding min-height-with-menu-distance">
	<div class="width100 text-center">
    	<img src="img/public-project.png" class="line-icon" alt="Current Projects in Public" title="Current Projects in Public">
	</div>
    <h1 class="title-h1 text-center">Current Projects</h1> 
    <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Public</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Petri Rating</button>
    </div>
    
    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>
                                                                                      
                    </thead>
                </tr>
               <tr>
                    	<td class="th0-color color-td company-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
						<td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>

						<td class="th7-color color-td"></td>                         
                        
                        <td class="th8-color color-td"></td>

                </tr>           
                <tr>
                    	<td class="th0 company-td">Companies</td>
                        <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                        <td class="th2" title="Details about the products">Product Evaluation</td>
                        <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                        <td class="th4" title="Internal & External analysis">Industry</td>
						<td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                        <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                        <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                        <td class="th8" title="Overall growth of company">Petri Growth</td>                
                
                </tr>
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/havan-clothing.png" class="company-logo" alt="Havan Clothing is a social enterprise that sells minimalistic pocket tees touting designs by underprivileged children. Their goal is to make our customers look good, do good and feel good." title="Havan Clothing is a social enterprise that sells minimalistic pocket tees touting designs by underprivileged children. Their goal is to make our customers look good, do good and feel good.">
                            
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900"
                            title="Havan Clothing is a social enterprise that sells minimalistic pocket tees touting designs by underprivileged children. Their goal is to make our customers look good, do good and feel good.">Havan Clothing</b><p class="ev-p">EV: RM4,000,000</p>
                        </div>

                        </td>
                        <td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/rtist.png" class="company-logo" alt="Rtist is a platform that focuses on highlighting creative talents by making them into listings. They connect companies with the right local creative talent via permanent, contract, freelance or internship." title="Rtist is a platform that focuses on highlighting creative talents by making them into listings. They connect companies with the right local creative talent via permanent, contract, freelance or internship.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="*Rtist is a platform that focuses on highlighting creative talents by making them into listings. They connect companies with the right local creative talent via permanent, contract, freelance or internship.">Rtist</b><p class="ev-p">EV: RM5,668,000</p>
						</div>	
                        </td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="five-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/venture-grab.png" class="company-logo" alt="VentureGrab.com is a business buy-sell portal which provides automation matching of investment amount, business category, types and geographical location." title="VentureGrab.com is a business buy-sell portal which provides automation matching of investment amount, business category, types and geographical location.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="VentureGrab.com is a business buy-sell portal which provides automation matching of investment amount, business category, types and geographical location.">VentureGrab</b><p class="ev-p">EV: RM4,000,000</p>
						</div>	
                        </td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="five-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr>  
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/nu-vending.png" class="company-logo" alt="NuVending is a Malaysia leading vending machine supplier by offering best quality of vending machine equipped with latest technology." title="NuVending is a Malaysia leading vending machine supplier by offering best quality of vending machine equipped with latest technology.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="NuVending is a Malaysia leading vending machine supplier by offering best quality of vending machine equipped with latest technology.">Nu Vending</b>
                        	<p class="ev-p">EV: RM15,000,000</p>
						</div>	
                        </td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr>                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/love18c.png" class="company-logo" alt="A handmake chocolate workshop that is much more affordable and tastier than the Japan brand. Currently have stores at Kuala Lumpur and Penang." title="A handmake chocolate workshop that is much more affordable and tastier than the Japan brand. Currently have stores at Kuala Lumpur and Penang.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="A handmake chocolate workshop that is much more affordable and tastier than the Japan brand. Currently have stores at Kuala Lumpur and Penang.">Love18C Sdn Bhd</b><p class="ev-p">EV: RM11,700,000</p>
						</div>	
                        </td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="text-center"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr>
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/jocom.png" class="company-logo" alt="#1 Malaysia online grocer app, we are specialized in providing grocery shopping online service. It provides a total integrated solution that protects both the consumers and the vendors." title="#1 Malaysia online grocer app, we are specialized in providing grocery shopping online service. It provides a total integrated solution that protects both the consumers and the vendors.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="#1 Malaysia online grocer app, we are specialized in providing grocery shopping online service. It provides a total integrated solution that protects both the consumers and the vendors.">JOCOM MSHOPPING SDN BHD</b>
                        	<p class="ev-p">EV: RM 57,000,000</p>
						</div>	
                        </td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="text-center"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr>  
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/aspire-asia.png" class="company-logo" alt="Alfred connect homeowners with the property management in all stages starting from handing over of vacant possession to day-to-day management tasks. (i.e. submitting defects, facilities booking and any other applications)." title="Alfred connect homeowners with the property management in all stages starting from handing over of vacant possession to day-to-day management tasks. (i.e. submitting defects, facilities booking and any other applications).">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="Alfred connect homeowners with the property management in all stages starting from handing over of vacant possession to day-to-day management tasks. (i.e. submitting defects, facilities booking and any other applications).">Aspire Asia</b>
                        	<p class="ev-p">EV: RM 7,500,000</p>
						</div>	
                        </td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="text-center"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/goody-technologies.png" class="company-logo" alt="Goody Technologies Sdn Bhd is the leading New Generation Digital Media Network in Asia that contains No.1 Chinese Media Site, Crowd-sourcing Content Creators Platform，Data Technologies Platform and Digital Advertising Platform that provides online advertising solutions to advertisers." title="Goody Technologies Sdn Bhd is the leading New Generation Digital Media Network in Asia that contains No.1 Chinese Media Site, Crowd-sourcing Content Creators Platform，Data Technologies Platform and Digital Advertising Platform that provides online advertising solutions to advertisers.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="Goody Technologies Sdn Bhd is the leading New Generation Digital Media Network in Asia that contains No.1 Chinese Media Site, Crowd-sourcing Content Creators Platform，Data Technologies Platform and Digital Advertising Platform that provides online advertising solutions to advertisers.">Goody Technologies</b>
                        	<p class="ev-p">EV: RM 18,000,000</p>
						</div>	
                        </td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="four-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/supabear.png" class="company-logo" alt="Supabear solves the problems faced by both owner & agent in the property industry. Owner can submit their property details within few clicks and reach out to thousand of agents. Agents will be notified once there is property submitted within their registered area." title="Supabear solves the problems faced by both owner & agent in the property industry. Owner can submit their property details within few clicks and reach out to thousand of agents. Agents will be notified once there is property submitted within their registered area.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900"
                        title="Supabear solves the problems faced by both owner & agent in the property industry. Owner can submit their property details within few clicks and reach out to thousand of agents. Agents will be notified once there is property submitted within their registered area.">SupaBear</b><p class="ev-p">EV: RM 4,760,000</p>
						</div>	
                        </td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="one-stars-color star-css"><img src="img/star.png" class="star-png"></td>
                        <td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="three-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
						<td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                        <td class="text-center">N/A</td>
						<td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>  
                </tr>                                                                                                            
            </table>
        </div>        
    </div>
    
    <div id="Unlisted" class="tabcontent">
          	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>
                                                                                      
                    </thead>
                </tr>
               <tr>
                    	<td class="th0-color color-td company-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
						<td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>

						<td class="th7-color color-td"></td>                         
                        
                        <td class="th8-color color-td"></td>

                </tr>           
                <tr>
                    	<td class="th0 company-td">Companies</td>
                        <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                        <td class="th2" title="Details about the products">Product Evaluation</td>
                        <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                        <td class="th4" title="Internal & External analysis">Industry</td>
						<td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                        <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                        <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                        <td class="th8" title="Overall growth of company">Petri Growth</td>                
                
                </tr>
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/mypetslibrary.png" class="company-logo" alt="Mypetlibrary" title="Mypetslibrary is Asia’s 1st established professional platform featuring pets that are able to connect professional sellers and buyers across nationwide.">
                        </div>
                        <div class="right-name-div">
                        	<b class="font-weight900" title="Mypetslibrary is Asia’s 1st established professional platform featuring pets that are able to connect professional sellers and buyers across nationwide.">Mypetlibrary</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/samofa.png" class="company-logo" alt="Samofa" title="Samofa">
                        </div>
                        <div class="right-name-div">    
                            <b class="font-weight900">Samofa</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/chapter28.png" class="company-logo" alt="Chapter 28" title="Chapter28 started as a natural cosmetic brand with the intention to promote a vegan campaign. Alice’s strong passion for the concept of natural beauty makes her step into the cosmetic industry and to bring more awareness toward using cosmetics that are fully plants based. 
">
						</div>
                        <div class="right-name-div">
							<b class="font-weight900" title="Chapter28 started as a natural cosmetic brand with the intention to promote a vegan campaign. Alice’s strong passion for the concept of natural beauty makes her step into the cosmetic industry and to bring more awareness toward using cosmetics that are fully plants based. 
">Chapter 28</b>
						</div>
						</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/revenews.png" class="company-logo" alt="Revenews" title="Revenews">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Revenews has always been focused on the broader views of online revenue-sharing approaches. Revenews has become a leading voice for its dedication to objectivity and its fresh perspective.">Revenews</b>
                        </div>    
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr> 
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/thousand-media.png" class="company-logo" alt="Thousand Media" title="Thousand Media is an agency that provides unlimited Design/ Content creation and marketing solutions service.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Thousand Media is an agency that provides unlimited Design/ Content creation and marketing solutions service.">Thousand Media</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/urban-hygienist.png" class="company-logo" alt="Urban Hygienist" title="Urban Hygienist provides an effective cleaning solution to control eczema, allergic rhinitis, and asthma by removing unhygiene stuff in your home with eco-friendly water filter technology.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Urban Hygienist provides an effective cleaning solution to control eczema, allergic rhinitis, and asthma by removing unhygiene stuff in your home with eco-friendly water filter technology.">Urban Hygienist</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/zuzudo.png" class="company-logo" alt="Zuzudo Kitchen Bar" title="Zuzudo Kitchen Bar is a restaurant that serve Pork Free Japanese Food like Ramen, Various Don, Rice, CHicken Gyoza, Sushi and Snacks.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Zuzudo Kitchen Bar is a restaurant that serve Pork Free Japanese Food like Ramen, Various Don, Rice, CHicken Gyoza, Sushi and Snacks.">Zuzudo Kitchen Bar</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/ChiNou-Smart-Solutions.png" class="company-logo" alt="ChiNou Smart Solutions Sdn Bhd" title="ChiNou Smart Solutions Sdn Bhd">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900">ChiNou Smart Solutions Sdn Bhd</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/80+.png" class="company-logo" alt="80+" title="80+ aims to make available specialty coffee capsules so that everyone with in the Asian region can enjoy a cup of inspiring indulgence, anytime, anywhere.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="80+ aims to make available specialty coffee capsules so that everyone with in the Asian region can enjoy a cup of inspiring indulgence, anytime, anywhere.">80+</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/brandworks.png" class="company-logo" alt="Brandworks" title="Brandworks helps various brands tell meaningful stories across all of their channels. Brandworks provides digital strategy, channel strategy and campaign delivery.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Brandworks helps various brands tell meaningful stories across all of their channels. Brandworks provides digital strategy, channel strategy and campaign delivery.">Brandworks</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                      
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/chillitbuddy.png" class="company-logo" alt="Chillit Buddy" title="Chill'it Buddy can be considered as a social news and content website that gathers and share the latest, trendiest and spiciest news from all around the world.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Chill'it Buddy can be considered as a social news and content website that gathers and share the latest, trendiest and spiciest news from all around the world.">Chillit Buddy</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/dck.png" class="company-logo" alt="DCK" title="DCK products are designed to satisfy all of their customers automotive needs, anytime, anywhere.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="DCK products are designed to satisfy all of their customers automotive needs, anytime, anywhere.">DCK</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/dexinguoji.png" class="company-logo" alt="De Xin Guo Ji" title="De Xin Guo Ji helps their clients trade forex with convenient and earn money with confidence.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="De Xin Guo Ji helps their clients trade forex with convenient and earn money with confidence.">De Xin Guo Ji</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/design-village.png" class="company-logo" alt="Design Village" title="Design Village is Penang's outlet mall that hosts a broad variety of exciting international and local brands.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Design Village is Penang's outlet mall that hosts a broad variety of exciting international and local brands.">Design Village</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/foodee.png" class="company-logo" alt="FOODee" title="FOODee is an app to help you solve your daily problems on what to eat.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="FOODee is an app to help you solve your daily problems on what to eat.">FOODee</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/forever-travel-service.png" class="company-logo" alt="Forever Travel Service" title="Forever Travel Service believes that travel is an enriching experience that not only frees and relax a person, but also brodens your horizons. Forever Travel Service shares their adventures and travels with you, along with the latest news & offers of Travel & Tour services.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Forever Travel Service believes that travel is an enriching experience that not only frees and relax a person, but also brodens your horizons. Forever Travel Service shares their adventures and travels with you, along with the latest news & offers of Travel & Tour services.">Forever Travel Service</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>                                                                                                                                         
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/furkids.png" class="company-logo" alt="Bworth Fur Kids Home" title="Bworth Fur kids Home is a no-kill, self support shelter, to promote kindness and compassion towards all animals, to encourage a more humane world for the furkids.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Bworth Fur kids Home is a no-kill, self support shelter, to promote kindness and compassion towards all animals, to encourage a more humane world for the furkids.">Bworth Fur Kids Home</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>

                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/gic.png" class="company-logo" alt="GIC Group" title="They provides local & overseas properties investment, new projects marketing, investor club, real estate agency and hospitality management services.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="They provides local & overseas properties investment, new projects marketing, investor club, real estate agency and hospitality management services.">GIC Group</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>

                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/igl.png" class="company-logo" alt="IGL Coatings" title="IGL Coatings is a coating company that formulates eco-friendly car coatings which is safe and outperform existing products in the market in terms of durability, quality and ease of application.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="IGL Coatings is a coating company that formulates eco-friendly car coatings which is safe and outperform existing products in the market in terms of durability, quality and ease of application.">IGL Coatings</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/ist.png" class="company-logo" alt="iSpendtribute" title="iSpendtribute is a Social Enterprise that unites the community to cultivate a sharing culture by sustainably contributing to the society through their proprietary Spendtribute cycle.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="iSpendtribute is a Social Enterprise that unites our community to cultivate a sharing culture by sustainably contributing to the society through our proprietary Spendtribute cycle.">iSpendtribute</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/loadee.png" class="company-logo" alt="Loadee" title="Loadee">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Loadee">Loadee</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/mrringgit.png" class="company-logo" alt="Mr Ringgit" title="Mr Ringgit provides loan services through app.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Mr Ringgit provides loan services through app.">Mr Ringgit</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/sin-pen.png" class="company-logo" alt="Sin-Pen Travel" title="Sin-Pen established since 1975. They specialized in domestic & international tour, daily departure coach to Genting, SUPER VIP coach which available for rent.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Sin-Pen established since 1975. They specialized in domestic & international tour, daily departure coach to Genting, SUPER VIP coach which available for rent.">Sin-Pen Travel</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/vb.png" class="company-logo" alt="VogueBeaute" title="VogueBeaute believe that beauty does not discriminate. Their products are formulated to suit every shade of skin tone and every unique personality. ">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="VogueBeaute believe that beauty does not discriminate. Their products are formulated to suit every shade of skin tone and every unique personality. ">VogueBeaute</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>
                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/world-class-digital-marketing.png" class="company-logo" alt="World Class Digital Marketing" title="World Class Digital Marketing">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="World Class Digital Marketing">World Class Digital Marketing</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>                
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/yupa.png" class="company-logo" alt="Yupa" title="Yupa is a platform to let you discover and explore travel experiences, activities and interest where you can begin adventures of your style with the locals.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Yupa">Yupa</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>                                                                                                                                                                                                                        
            </table>
        </div>        
    </div>
	<div class="clear"></div>


</div>
</div>


<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background:#e8eef7; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #bed6ff; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #bed6ff; 
}	
</style>
<?php include 'js.php'; ?>
<script>
function openList(evt, listName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(listName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>

