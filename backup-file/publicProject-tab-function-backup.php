<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects in Public | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 overflow menu-distance same-padding">
	<div class="width100 text-center">
    	<img src="img/public-project.png" class="line-icon" alt="Current Projects in Public" title="Current Projects in Public">
	</div>
    <h1 class="title-h1 text-center">Current Projects in Public</h1> 
    <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Listed</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Unlisted</button>
    </div>
    
    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div tab2">
            <table class="company-name-table">
                <tr>
                    <td><button class="tablinks2 active" onclick="openCompany(event, 'Havan')">Havan Clothing</button></td>
                    <td><button class="tablinks2 active" onclick="openCompany(event, 'Rtist')">Rtist</button></td>
                    <td>Nu Vending</td>
                    <td>Love18C Sdn Bhd</td>
                    <td>JOCOM MSHOPPING SDN BHD</td>
                </tr>
            </table>
        </div>
        <div id="Havan" class="tabcontent2">
        	<p>Havan</p>
        </div>
        <div id="Rtist" class="tabcontent2">
        	<p>Rtist</p>
        </div>        
    </div>
    
    <div id="Unlisted" class="tabcontent">
      <h3>Paris</h3>
      <p>Paris is the capital of France.</p> 
    </div>


</div>
<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}

</style>
<?php include 'js.php'; ?>

<script>
function openList(evt, listName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(listName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
<script>
function openCompany(evt, companyName) {
  var i, tabcontent2, tablinks2;
  tabcontent2 = document.getElementsByClassName("tabcontent2");
  for (i = 0; i < tabcontent2.length; i++) {
    tabcontent2[i].style.display = "none";
  }
  tablinks2 = document.getElementsByClassName("tablinks2");
  for (i = 0; i < tablinks2.length; i++) {
    tablinks2[i].className = tablinks2[i].className.replace(" active", "");
  }
  document.getElementById(companyName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>

