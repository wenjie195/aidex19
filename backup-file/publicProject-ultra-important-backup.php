<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 overflow menu-distance same-padding">
	<div class="width100 text-center">
    	<img src="img/public-project.png" class="line-icon" alt="Current Projects in Public" title="Current Projects in Public">
	</div>
    <h1 class="title-h1 text-center">Current Projects</h1> 
    <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Public</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Petri Rating</button>
    </div>
    
    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Founder</th>
						<th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>

                        <th class="th2"></th>
                        <th class="th2"></th>
                        <th class="th2"></th>
                        <th class="th2">Product</th>
						<th class="th2"></th>
                        <th class="th2"></th>
                        <th class="th2"></th>  
                        
                        <th class="th3"></th>
                        <th class="th3"></th>
                        <th class="th3">Business Model</th>
                        <th class="th3"></th>
						<th class="th3"></th>
                        
                        <th class="th4"></th>
                        <th class="th4"></th>
                        <th class="th4">Industry</th>
                        <th class="th4"></th>
						<th class="th4"></th>      
                        
                        <th class="th5"></th>
                        <th class="th5"></th>
                        <th class="th5">Team Evaluation</th>
                        <th class="th5"></th>
						<th class="th5"></th>     
                        
                        <th class="th6"></th>
                        <th class="th6">Market Readiness</th>
                        <th class="th6"></th>
						<th class="th6"></th>                         

						<th class="th7"></th>                         
                        
                        <th class="th8"></th>
                        <th class="th8">Performance Matrix</th>
                        <th class="th8"></th>  
                        
                        <th class="th9">Websites</th>                                                                               
                    </thead>
                </tr>
               <tr>
                    	<td class="th0-color color-td company-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
						<td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>

                        <td class="th2-color color-td"></td>
                        <td class="th2-color color-td"></td>
                        <td class="th2-color color-td"></td>
                        <td class="th2-color color-td"></td>
						<td class="th2-color color-td"></td>
                        <td class="th2-color color-td"></td>
                        <td class="th2-color color-td"></td>
                        
                        <td class="th3-color color-td"></td>
                        <td class="th3-color color-td"></td>
                        <td class="th3-color color-td"></td>
                        <td class="th3-color color-td"></td>
						<td class="th3-color color-td"></td>
                        
                        <td class="th4-color color-td"></td>
                        <td class="th4-color color-td"></td>
                        <td class="th4-color color-td"></td>
                        <td class="th4-color color-td"></td>
						<td class="th4-color color-td"></td>  
                        
                        <td class="th5-color color-td"></td>
                        <td class="th5-color color-td"></td>
                        <td class="th5-color color-td"></td>
                        <td class="th5-color color-td"></td>
						<td class="th5-color color-td"></td>     
                        
                        <td class="th6-color color-td"></td>
                        <td class="th6-color color-td"></td>
                        <td class="th6-color color-td"></td>
						<td class="th6-color color-td"></td>                         

						<td class="th7-color color-td"></td>                         
                        
                        <td class="th8-color color-td"></td>
                        <td class="th8-color color-td"></td>
                        <td class="th8-color color-td"></td>
                        
                        <td class="th9-color color-td"></td> 
                </tr>           
                <tr>
                    	<td class="th0 company-td">Companies</td>
                        <td class="th1">Working experienced (10%)</td>
                        <td class="th1">Business experienced (30%)</td>
                        <td class="th1">Experienced in the industry (30%)</td>
                        <td class="th1">Awards (5%)</td>
						<td class="th1">Achievement (15%)</td>
                        <td class="th1">Qualification (10%)</td>
                        <td class="th1">Total score</td>

                        <td class="th2">Unique selling point of product (15%)</td>
                        <td class="th2">Choices & varieties (10%)</td>
                        <td class="th2">Presentation of product (15%)</td>
                        <td class="th2">Price (30%)</td>
						<td class="th2">Payment (20%)</td>
                        <td class="th2">Problem-solving (10%)</td>
                        <td class="th2">Total Score</td>  
                        
                        <td class="th3">Scalability (30%)</td>
                        <td class="th3">Switchable (10%)</td>
                        <td class="th3">Recurring revenues (30%)</td>
                        <td class="th3">Protection from competition (30%)</td>
						<td class="th3">Total score</td>
                        
                        <td class="th4">Competitor analysis (30%)</td>
                        <td class="th4">Entry barrier (30%)</td>
                        <td class="th4">Power of suppliers (25%)</td>
                        <td class="th4">Power of buyers (15%)</td>
						<td class="th4">Total score</td>      
                        
                        <td class="th5">Working experience (35%)</td>
                        <td class="th5">Sales& marketing team (35%)</td>
                        <td class="th5">Team qualification (10%)</td>
                        <td class="th5">Team achievement (20%)</td>
						<td class="th5">Total score</td>     
                        
                        <td class="th6">Ask questions from consumer point of view (45%)</td>
                        <td class="th6">Google Adword look whether people search for it or not (20%)</td>
                        <td class="th6">Numbers of competitor (35%)</td>
						<td class="th6">Total score</td>                         

						<td class="th7">Petri Growth</td>                         
                        
                        <td class="th8">Revenue</td>
                        <td class="th8">Cash flow</td>
                        <td class="th8">Total Score</td>  
                        
                        <td class="th9"></td> 
                </tr>
                <tr>
                    	<td class="company-td"><b class="font-weight900">Havan Clothing</b><br>
							*Listed<br>
							*Funded<br>
							*Rated<br>
							*Researched
                        </td>
                        <td><b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            10 years working experience in Finance and Business<br> 
                            *Hany Cheng (⅔ X 10% = 6.67%)<br>
                            10 years working experience in education & counselling, but not related to their business 
						</td>
                        <td><b class="font-weight900 bad-color">Bad (10%)</b><br>
							*This is the 1st business for both founders
                        </td>
                        <td><b class="font-weight900 ave-color">Average (15%)</b><br>
                            *Ivan Eng (⅔ X 30% = 20%)<br>
                            A bit touch to business field with 10 years<br>
                            *Hany Cheng (⅓ X 30% = 10%)<br>
                            No touching to business field
                        </td>
                        <td><b class="font-weight900 bad-color">Bad (1.67%)</b><br>
							*No business award for both founders.
						</td>
						<td><b class="font-weight900 bad-color">Bad (5%)</b><br>
							*No achievement for both founders.
                        </td>
                        <td><b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            (2012 - 2014) Master of Business Administration (MBA) of Finance, General in Multimedia University<br>
                            (2006 - 2009) Bachelor’s degree of International Business in Multimedia University<br>
                            *Hany Cheng (⅓ X 10% = 3.33%)<br>
                            Not stated, but to become a counsellor should have a diploma or degree certificate.
                        </td>
                        <td class="ave-color">45.01% (Average)</td>

                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *All designs are created by the Shelter-home children<br>
                            *Simple & recognizing child’s creativity<br>
                            *However, it is too many competitor sell simple design T-shirt
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (3.33%)</b><br>
							*Only have T-shirts and socks
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *The website and product packaging is satisfied
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (10%)</b><br>
							*Price charged is moderate with nice packaging
                        </td>
						<td>
                       		<b class="font-weight900 ave-color">Average (20%)</b><br>
							*Using iPay88 and provided various payment methods.
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (3.33%)</b><br>
    						*Yes,as everyone wear clothes
                        </td>
                        <td class="ave-color">56.66% (Average)</td>  
                        
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						*Yes, fashion is scalable
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (3.33%)</b><br>
                            *Customers can choose to switch to another brands
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (10%)</b><br>
							*Not really as they only sell Tee and socks right now
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (10%)</b><br>
    						*There are a lot of competitiors in this industry
                        </td>
						<td class="ave-color">Average (43.33%)</td>
                        
                        <td><b class="font-weight900 ave-color">Average (20%)</b><br>  
                            New industry competitor has been increase steadily				
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (10%)</b><br> 
                            Clothing business is pretty easy to emulate and with modern service such as on demand clothing printing, anyone can start on this inudstry
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (25%)</b><br>
                            The amount of cloth manufacturing industry is a lot. Shirt can be design easily and resell.
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (5%)</b><br>
							Buyers has a lot of alternative rather than T-shirt and tons of competitors. This make buyer power high.
                        </td>
						<td class="ave-color">Average (60%)</td>      
                        
                        <td>
                        	<b class="font-weight900 good-color">Good (35%)</b><br>
							*Their team are educated and full of working experience
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (35%)</b><br>
                            *Their revenue growth in a huge %, there can see their effort on sales & marketing side.
                        </td>
                        <td>
							<b class="font-weight900 good-color">Good (10%)</b><br>
                            *Overall are educated and with working experience                        
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (6.67%)</b><br>
							*Not data on them
                        </td>
						<td class="good-color">Good (86.67%)</td>     
                        
                        <td><b class="font-weight900 ave-color">Average (30%)</b><br>
							*They have good feedbacks and reviews but not much buying intention on them, most of the people buying their product is just because of sympathy.
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (6.67%)</b><br>
							*Bad Traffic<br>
							*Searching "Clothing" in google, it is not shown on the 1st page.
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (35%)</b><br>
							*They have a lot of competitor selling similar products in this market.
                        </td>
						<td class="good-color">Good (78.33%)</td>                         

						<td><b class="font-weight900 ave-color">61.67% (Average)</b></td>                         
                        
                        <td><b class="font-weight900 good-color">Good</b><br>
							*Yearly revenue from RM20K in 2017 to RM240K in 2019.
                        </td>
                        <td><b class="font-weight900 bad-color">Bad</b><br>
							*No cash flow in their yearly financial report.
						</td>
                        <td>-</td>  
                        
                        <td><a href="https://www.havanclothing.com/" target="_blank" class="blue-link">havanclothing.com</a></td> 
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900">Rtist</b><br>
							*Listed<br>
							*Funded<br>
							*Rated<br>
							*Researched
                        </td>
                        <td><b class="font-weight900 ave-color">Average (3.33%)</b><br>
                            *worked in industrial design for 4 years
						</td>
                        <td><b class="font-weight900 ave-color">Average (20%) </b><br>
							*Managing Director at Zeroo Sdn Bhd, CEO & Founder of Rtist Creative Platform, selling insurance for a couple of years )
                        </td>
                        <td><b class="font-weight900 ave-color">Average (20%)</b><br>
                            *Graduate from Malaysian Institute of Art, product designer for 4 years
                        </td>
                        <td><b class="font-weight900 good-color">Good (5%)</b><br>
							*Cradle CIP300 Grand Recipient awarded RM300,000 (2018)<br>
                            Top 5 Start-ups of the Selangor Accelerator Programme<br>
                            Top 10 Winners in the MyStartr Dream Factory Startup Contest (2017)<br>
						</td>
						<td><b class="font-weight900 bad-color">Bad (5%)</b><br>
							*no achievement
                        </td>
                        <td><b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            *diploma in Malaysian Institute of Art
                        </td>
                        <td class="ave-color">60% (Average)</td>

                        <td>
                            <b class="font-weight900 good-color">Good (15%)</b><br>
                            *Make employer life easier by recruiting online
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Average (6.67%)</b><br>
							*Provide freelancer like graphic design and business writing and also act as a platform for job posting
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *Moderate display<br>
                            Website display<br>
                            Clean & professional looking 
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							Price charged based on freelancer. It is based on market rate.
                        </td>
						<td>
                       		<b class="font-weight900 ave-color">Average (13.33 %)</b><br>
							*Payment looks secure and easy to use. Have few payment option to choose of.
                        </td>
                        <td>
                            <b class="font-weight900 good-color">Good (10%)</b><br>
    						*Solved all the employer/hiring problem in recruiting.
                        </td>
                        <td class="good-color">Good (75%)</td>  
                        
                        <td>
                            <b class="font-weight900 good-color">Good (30%)</b><br>
    						*They are planning to expand their business in the next 3 years
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            *There are still other platform to choose
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (10%)</b><br>
							*There is without any add on in this platform/product
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						*There is still competitive in the market
                        </td>
						<td class="ave-color">Average (66.67%)</td>
                        
                        <td><b class="font-weight900 good-color">Good (30%)</b><br>  
                            *There is competition in the market but most of the competitors are not localize and some are those indirect competitors				
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br> 
                            *It can be enter into this industry with a website and money
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (16.67%)</b><br>
                            *They can chooseto place themselves in another platform. However, they are locked with annual payment which were made by them in the Rtist platform, so they will more likely need to stick back to Rtist.
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (15%)</b><br>
							*There is another platform but count in USD. So engage with Rtist is much more cheaper.
                        </td>
						<td class="good-color">Good (81.67%)</td>      
                        
                        <td>
                        	<b class="font-weight900 good-color">Good (35%)</b><br>
							*Their team are educated and full of working experience
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (23.33%)</b><br>
                            *There are 100% growth on their revenue every year
                        </td>
                        <td>
							<b class="font-weight900 bad-color">Bad (3.33%)</b><br>
                            *Not any data stated on their education                        
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (6.67%)</b><br>
							*Not data on them
                        </td>
						<td class="good-color">Good (68.32%)</td>     
                        
                        <td><b class="font-weight900 ave-color">Average (30%)</b><br>
							*4.9 out of 5 on their Facebook reviews.<br>
							*People using Rtist to finding designers is still low.
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (13.33%)</b><br>
							*Middle level traffic<br>
							*Searching designer platform will shown Rtist in 1st page of Google
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (11.67%)</b><br>
							*This is the only one formal website to helping other hire or search on freelancer.
                        </td>
						<td class="ave-color">Average (55%)</td>                         

						<td><b class="font-weight900 good-color">67.67%% Good</b></td>                         
                        
                        <td><b class="font-weight900 ave-color">Average</b><br>
							*increase around 500k in year 2018
                        </td>
                        <td><b class="font-weight900 ave-color">Average</b><br>
							*increase around 65k in year 2018
						</td>
                        <td>-</td>  
                        
                        <td><a href="https://www.rtist.com.my/" target="_blank" class="blue-link">rtist.com.my</a></td> 
                </tr>                
                <tr>
                    	<td class="company-td"><b class="font-weight900">VentureGrab</b><br>
							*Listed<br>
							*Funded<br>
							*Rated<br>
							*Researched
                        </td>
                        <td><b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            *He was a Chief Operation Officer of a China Company and the Managing Director of Skyboard Media SDN BHD.
						</td>
                        <td><b class="font-weight900 good-color">Good  (30%)</b><br>
							*Operated a Japanese BBQ Restaurant and an Advertising Company.
                        </td>
                        <td><b class="font-weight900 ave-color">Average (20%)</b><br>
                            *So far don't have experienced in this industry but managed 2 companies before
                        </td>
                        <td><b class="font-weight900 ave-color">Average (3.33%)</b><br>
							*Awarded by Sinchew, Golden Eagle and SME100
						</td>
						<td><b class="font-weight900 ave-color">Average (10%)</b><br>
							*Venture Grab has been released in several press and platforms
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (10%)</b><br>
                            *Master holder
                        </td>
                        <td class="good-color">76.66% (Good)</td>

                        <td>
                            <b class="font-weight900 good-color">Good (15%)</b><br>
                            *They are pioneer to this business
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (10%)</b><br>
							*They provide business investment and business listing which is the first in Malaysia 
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *The website itself is okay, user-friendly and easy to find the listings.
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							*Price is reasonable
                        </td>
						<td>
                       		<b class="font-weight900 ave-color">Average (13.33 %)</b><br>
							*N/A
                        </td>
                        <td>
                            <b class="font-weight900 good-color">Good (10%)</b><br>
    						*Solved all the businesses problems by the owners do not have to worry about how to sell their business or finding for partners.
                        </td>
                        <td class="good-color">78.33% (Good)</td>  
                        
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						*The founders has a goal to expand over Asean
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            *Customer can choose to make listing themselves or through VentureGrab
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (10%)</b><br>
							*Do not involve recurring revenues
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						*Competitors are only those funding platforms
                        </td>
						<td class="ave-color">Average (56.67%)</td>
                        
                        <td><b class="font-weight900 ave-color">Average (20%)</b><br>  
                            *There is temporary no direct competitors but indirect which is the funding platform.				
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br> 
                            *Can be entered by others since it was under service industry
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (16.67%)</b><br>
                            *The business listing can be or not to be list their business on that platform
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (10%)</b><br>
							*Buyer can choose to buy or not to on that platform
                        </td>
						<td class="ave-color">Average (66.67%)</td>      
                        
                        <td>
                        	<b class="font-weight900 good-color">Good (35%)</b><br>
							*Their team are educated and full of working experience
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (23.33%)</b><br>
                            *Do not have data
                        </td>
                        <td>
							<b class="font-weight900 good-color">Good (10%)</b><br>
                            *Overall are educated and with working experience                  
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (6.67%)</b><br>
							*Not data on them
                        </td>
						<td class="good-color">Good (75%)</td>     
                        
                        <td><b class="font-weight900 ave-color">Average (30%)</b><br>
							*Consumer have to buy or invest at that price that they agreed at
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (20%)</b><br>
							*Good traffic, People are looking for investment service.
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (35%)</b><br>
							*Less competitive, indirect competition from the funding platform
                        </td>
						<td class="good-color">Good (85%) </td>                         

						<td><b class="font-weight900 good-color">73% Good</b></td>                         
                        
                        <td><b class="font-weight900 bad-color">Bad</b><br>
							*Having huge loss in year 2018 compare with 2017.
                        </td>
                        <td><b class="font-weight900 ave-color">Average</b><br>
							*Cash flow increase from year 2017 to 2018
						</td>
                        <td>-</td>  
                        
                        <td><a href="https://www.venturegrab.com/" target="_blank" class="blue-link">venturegrab.com</a></td> 
                </tr>                 
                <tr>
                    	<td class="company-td"><b class="font-weight900">Nu Vending</b><br>
							*Live<br>
							*UR<br>
							*UF
                        </td>
                        <td><b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            * worked as a manufacturing manager, technical product manager, field service engineer
						</td>
                        <td><b class="font-weight900 good-color">Good  (30%)</b><br>
							*Manufacturing Manager for 1 year 2 months.<br>
                            *Technical product manager for 1 year 3 months.<br>
                            *FIeld Service Engineer for 9 months<br>
                            *BMET for 11 months<br>
                            *Co- Founder, CEO of NuVending
                        </td>
                        <td><b class="font-weight900 good-color">Good (30%)</b><br>
                            *Founder of Sunny Tech Electronics (DBA Nu Vending)<br>
							*Not much record in the industry, but has worked at many different industry
                        </td>
                        <td><b class="font-weight900 bad-color">Bad (1.67%)</b><br>
							*did not show
						</td>
						<td><b class="font-weight900 bad-color">Bad (5%)</b><br>
							*did not show
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (10%)</b><br>
                            *Master of Engineering in Chinese Academy of Sciences, Shanghai Institute of Ceramics
                        </td>
                        <td class="good-color">83.34% (Good)</td>

                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *integration of multiple payment acquiring devices on unattended POS that accepts Paywave, MyDebit Cards , Maybank QR pay, Grab Pay ,Boost, and other e-Money licensed platform.
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (10%)</b><br>
							*provides types of vending machines eg, coffee, mix n match (drinks), claw machines, mix n match (foods, chill to hot), mix n match (foods, frozen food & ice cream) 
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *The website is user friendly, simple but detailed. As for the vending machines, it has different design for each machine
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							*Their products has rating, which means there will people buy from them
                        </td>
						<td>
                       		<b class="font-weight900 good-color">Good (20%)</b><br>
							*The vending machines accepts various payment methods eg, cash, coins, credit/debit card, e-wallet, token
                        </td>
                        <td>
                            <b class="font-weight900 good-color">Good (10%)</b><br>
    						*Solve business problems such as physical workforce, high OPEX, CAPEX, human factors and working hours
                        </td>
                        <td class="good-color">80% (Good)</td>  
                        
                        <td>
                            <b class="font-weight900 good-color">Good (30%)</b><br>
    						*To be the Market Leader within Malaysia for Automation Technology through Vending Machines and the Digital Ecosystem, and expanding into the ASEAN market.
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (3.33%)</b><br>
                            *Customer can choose to buy from retail stores such as supermarkets
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							*After buying the machines, they will have to restock when they ran out of stock
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						* The business model is easy to copy, but just the costs are high
                        </td>
						<td class="good-color">Good (73.33)</td>
                        
                        <td><b class="font-weight900 ave-color">Average (20%)</b><br>  
                            *high competition as there are also some vending machines suppliers				
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br> 
                            **Since there are quite some numbers of competitors, the entry barrier might not that high
                        </td>
                        <td>
                        	<b class="font-weight900 good-color">Good (25%)</b><br>
                            *Suppllier power is low as they can be replaced by any products
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (10%)</b><br>
							*has moderate buying power, usually for people that prefer convenient
                        </td>
						<td class="good-color">Good (75%)</td>      
                        
                        <td>
                        	<b class="font-weight900 good-color">Good (35%)</b><br>
							*Their team are educated and full of working experience
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (23.33%)</b><br>
                            *There are growth on their revenue every year
                        </td>
                        <td>
							<b class="font-weight900 ave-color">Average (6.67)</b><br>
                            *Team long working exprience                  
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Average (13.33%)</b><br>
							*Specialize in business management and marketing, won top agency award 3 years consecutively
                        </td>
						<td class="good-color">Good (78.33%)</td>     
                        
                        <td><b class="font-weight900">N/A</b><br>
							*did not list out the reviews
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (13.33%)</b><br>
							*Moderate traffic, will appear on the first page if users search for 'vending machine'
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average  (23.33%)</b><br>
							*Moderate competitors in their industry (Vending Machine Suppliers)
                        </td>
						<td class="bad-color">Bad (12.22%)</td>                         

						<td><b class="font-weight900 ave-color">67.04% (Average)</b></td>                         
                        
                        <td><b class="font-weight900 ave-color">Average</b>
                        </td>
                        <td><b class="font-weight900 bad-color">Bad</b><br>
							*Decrease around 220k from year 2018 to 2019.
						</td>
                        <td>-</td>  
                        
                        <td><a href="https://www.nuvending.my/" target="_blank" class="blue-link">nuvending.my</a></td> 
                </tr>
                <tr>
                    	<td class="company-td"><b class="font-weight900">Love18C Sdn Bhd</b><br>
							*UR<br>
							*Funded<br>
							*Listed
                        </td>
                        <td><b class="font-weight900 good-color">Good (10%)</b><br>
                            *work in IT & Internet Industry for 20 years then enter into a whole new chocolatier industry
						</td>
                        <td><b class="font-weight900 good-color">Good  (30%)</b><br>
							*Operation manager (Intradeco 9 years)<br>
                            *Service Delivery Director (Privasia1 year 9 months)<br>
                            *Chief Information Officer (Platform2u.com 2 years 3 months)<br>
                            *President & Chief Strategy Officer (Buyanihan 1 year 5 months)<br>
                            *President (Ipay88 1 Year 11 months)<br>
                            *Country Manager (Innity 3 years 10 months)<br>
                            *Chief Chocolatier (Love18C 2012- present)
                        </td>
                        <td><b class="font-weight900 bad-color">Bad (10%)</b><br>
                            *Fresh man for chocolatier industry, previously worked in IT and Internet industries for 20 years
                        </td>
                        <td><b class="font-weight900 bad-color">N/A</b><br>
							*data not shown
						</td>

                        <td>
                        	<b class="font-weight900 good-color">Good (15%)</b><br>
                            *sold over 50,000 boxed of chocolates since Y2012, with over 15,000 boxed sold during christmas and CNY season.<br>
                            *First Love Ganache chocolate has been featured in over 10 prime newspapers and magazines, interviewed by Astro AEC and sharing in social media.<br>
                            *Top 3 Malaysia handmade chocolates brand in the market
                        </td>
						<td><b class="font-weight900 good-color">Good (10%)</b><br>
							*Master in e-Business in University of SOuthern Queensland
                        </td>                        
                        <td class="good-color">75% (Good)</td>

                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *Premium real chocolate at a affordable price with well packaging.
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (6.67%)</b><br>
							*Have different kind of chocolates (Flavors & target market)
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *The UI/UX of the apps & web are good, easy to use and the product packaging is so nice.
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							*Price is reasonable and is cheaper than the other competitors
                        </td>
						<td>
                       		<b class="font-weight900 ave-color">Average (13.33%)</b><br>
							*Payment is secured and offer different kind of payment type for customers.
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (3.33%)</b><br>
    						*Only solved when peoples need it.
                        </td>
                        <td class="ave-color">63.33% (Average)</td>  
                        
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						*Founders expect to expand to China Market in 2020
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (3.33%)</b><br>
                            *Yes, consumer might switch to another since the competition is there but Love18 offers a better price with better quality.
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							*If dine in the Cafe then yes. Customers might also order drinks or food.
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						*There are already many popular chocolate brand in Malaysia, if they wanted to handcraft their own chocolate it is possible
                        </td>
						<td class="ave-color">Average (63.33%)</td>
                        
                        <td><b class="font-weight900 bad-color">Bad (10%)</b><br>  
                            *There are strong competitors such as ROYCE, Harriston Signature				
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (10%)</b><br> 
                            *Since there are many competitors selling chocolates therefore the entry barrier is low
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (16.67%)</b><br>
                            *Chocolate's recipe are almost similar, so a supplier can choose to supply to other brand as well
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (5%)</b><br>
							*consumers have other choice to choose from since there are many competitors
                        </td>
						<td class="ave-color">Average (41.67%)</td>      
                        
                        <td>
                        	N/A
                        </td>
                        <td>
                        	N/A
                        </td>
                        <td>
							N/A                  
                        </td>
                        <td>
                        	N/A
                        </td>
						<td>N/A</td>     
                        
                        <td><b class="font-weight900 ave-color">Average (30%)</b><br>
							*They have a very good ratings and feedback from their facebook page
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (13.33%)</b><br>
							*There are quite a lot of the user who search for "Love18", "Love18 chocolate" on Google Search Engine
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average  (23.33%)</b><br>
							*There are a number of competitors selling the similar products (chocolate but not all of them are having own handcraft chocolate)
                        </td>
						<td class="ave-color">Average (66.66%)</td>                         

						<td><b class="font-weight900 ave-color">62% Average</b></td>                         
                        
                        <td><b class="font-weight900 ave-color">Average</b>
                        </td>
                        <td>N/A 
						</td>
                        <td>-</td>  
                        
                        <td><a href="https://love18.cafe/" target="_blank" class="blue-link">love18.cafe</a></td> 
                </tr>
                <tr>
                    	<td class="company-td"><b class="font-weight900">JOCOM MSHOPPING SDN BHD</b><br>
							*Live<br>
							*UR<br>
							*UF
                        </td>
                        <td><b class="font-weight900 ave-color">Average (6.67%)</b><br>
                            *He worked as a developer (multimedia), sales and also director (Consulting group)
						</td>
                        <td><b class="font-weight900 ave-color">Average  (20%)</b><br>
							*Operated an IT consulting group named JOCOM as well.
                        </td>
                        <td><b class="font-weight900 ave-color">Average (20%)</b><br>
                            *Experienced he had was IT based, since JOCOM is a mobile apps and e-commerce site. It is related to what he previously is doing.
                        </td>
                        <td><b class="font-weight900 ave-color">Average (3.33%)</b><br>
							*Being awarded as "Online Mcommerce Entreprenuership" and "E-halal.com partner "
						</td>

                        <td>
                        	<b class="font-weight900 ave-color">Average (10%)</b><br>
                            *He is the chairman of Malaysia Digital Chamber of Commerce (MDCC) and is the first that founded M-Commerce Online shopping that can deliver within 24 hours.
                        </td>
						<td><b class="font-weight900 ave-color">Average (6.67%)</b><br>
							*He is a degree holder under computer science at Monash University
                        </td>                        
                        <td class="ave-color">66.67% (Average)</td>

                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *Previously is the first in Malaysia, their USP is will deliver to customer's doorstep within 24 hours
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (6.67%)</b><br>
							*Provides plenty of groceries choices.
                        </td>
                        <td>
                            <b class="font-weight900 ave-color">Average (10%)</b><br>
                            *The UI/UX of the apps & web are good, easy to use. 
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							*The price are just similar with others, not cheaper than other platform
                        </td>
						<td>
                       		<b class="font-weight900 ave-color">Average (13.33%)</b><br>
							*Variety of payment type, secured and easy to use
                        </td>
                        <td>
                            <b class="font-weight900 good-color">Good (10%)</b><br>
    						*Definitely because it brings convenience for the people
                        </td>
                        <td class="ave-color">70% (Average)</td>  
                        
                        <td>
                            <b class="font-weight900 ave-color">Average (20%)</b><br>
    						*The founder wants to expand to the countries nearby ASEAN
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (3.33%)</b><br>
                            *Yes, as there are many competitors out there with cheaper price like "Happy Fresh" and customers can choose to walk in to buy their groceries
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br>
							*Yes, it may happened because of minimum spend.
                        </td>
                        <td>
                            <b class="font-weight900 bad-color">Bad (10%)</b><br>
    						*There are a lot of competitiors in this industry now and people can choose to walk in.
                        </td>
						<td class="ave-color">Average (53.33%)</td>
                        
                        <td><b class="font-weight900 ave-color">Average (20%)</b><br>  
                            *There are already a few competitors in the market which offer better than JOCOM				
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (20%)</b><br> 
                            *Since there are plenty of competitors are coming in, the entry barrier might not that high
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (16.67%)</b><br>
                            *Since there are plenty of comptitors outside, the suppliers have high bargaining power.
                        </td>
                        <td>
                        	<b class="font-weight900 bad-color">Bad (5%)</b><br>
							*They can choose any other providers or walk in
                        </td>
						<td class="ave-color">Average (61.67%)</td>      
                        
                        <td>
                        	N/A
                        </td>
                        <td>
                        	N/A
                        </td>
                        <td>
							N/A                  
                        </td>
                        <td>
                        	N/A
                        </td>
						<td>N/A</td>     
                        
                        <td><b class="font-weight900 ave-color">Average (30%)</b><br>
							Overall review from Shopee is good
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average (13.33%)</b><br>
							*There are quite a lot of the user who search for "JOCOM" on Google Search Engine
                        </td>
                        <td>
                        	<b class="font-weight900 ave-color">Average  (23.33%)</b><br>
							*There are plenty of new comers are in this industry like happyfresh or the retail store have their own e-commerce platform
                        </td>
						<td class="ave-color">Average (66.66%)</td>                         

						<td><b class="font-weight900 ave-color">53.05% Average</b></td>                         
                        
                        <td><b class="font-weight900 ave-color">Average</b><br>
                        *Profit from RM1 Million increased to RM2 Million in 2016 to 2017.
                        </td>
                        <td><b class="font-weight900 ave-color">Average</b><br>
                        *Cash flow from RM743,142 increased to RM 1 million
						</td>
                        <td>-</td>  
                        
                        <td><a href="https://www.jocom.my/" target="_blank" class="blue-link">jocom.my</a></td> 
                </tr>
                               
            </table>
        </div>        
    </div>
    
    <div id="Unlisted" class="tabcontent" style="height: calc(100vh - 320px);">
      <p class="text-center" style="margin:0; padding:30px;">Coming Soon</p>
    </div>
	<div class="clear"></div>
    <div class="width100 overflow big-container-div">
    	<div class="container-div">
        	<div class="bad-div color-div"></div><p class="bad-color color-p">Bad: 0 ~ 0.3</p> 
        </div>
    	<div class="container-div">
        	<div class="ave-div  color-div"></div><p class="ave-color color-p">Average: 0.31 ~ 0.7</p> 
        </div>
    	<div class="container-div last-container-div">
        	<div class="good-div  color-div"></div><p class="good-color color-p">Good: 0.71 ~ 1</p> 
        </div>            
    </div>

</div>
</div>


<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background:#e8eef7; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #bed6ff; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #bed6ff; 
}	
</style>
<?php include 'js.php'; ?>
<script>
function openList(evt, listName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(listName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>

