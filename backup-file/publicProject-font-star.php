<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 overflow menu-distance same-padding min-height-with-menu-distance">
	<div class="width100 text-center">
    	<img src="img/public-project.png" class="line-icon" alt="Current Projects in Public" title="Current Projects in Public">
	</div>
    <h1 class="title-h1 text-center">Current Projects</h1> 
    <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Public</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Petri Rating</button>
    </div>
    
    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>
                                                                                      
                    </thead>
                </tr>
               <tr>
                    	<td class="th0-color color-td company-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
						<td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>

						<td class="th7-color color-td"></td>                         
                        
                        <td class="th8-color color-td"></td>

                </tr>           
                <tr>
                    	<td class="th0 company-td">Companies</td>
                        <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                        <td class="th2" title="Details about the products">Product Evaluation</td>
                        <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                        <td class="th4" title="Internal & External analysis">Industry</td>
						<td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                        <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                        <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                        <td class="th8" title="Overall growth of company">Petri Growth</td>                
                
                </tr>
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="Havan Clothing is a social enterprise that sells minimalistic pocket tees touting designs by underprivileged children. Their goal is to make our customers look good, do good and feel good.">Havan Clothing</b>
							
                        </td>
                        <td class="two-stars-color star-css">★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
						<td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="text-center">N/A</td>
						<td class="three-stars-color star-css">★★★</td>
                        
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="*Rtist is a platform that focuses on highlighting creative talents by making them into listings. They connect companies with the right local creative talent via permanent, contract, freelance or internship.">Rtist</b>
							
                        </td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="five-stars-color star-css">★★★★★</td>
						<td class="four-stars-color star-css">★★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css">★★★★</td>  
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="VentureGrab.com is a business buy-sell portal which provides automation matching of investment amount, business category, types and geographical location.">VentureGrab</b>
							
                        </td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
						<td class="four-stars-color star-css">★★★★</td>
                        <td class="five-stars-color star-css">★★★★★</td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css">★★★★</td>  
                </tr>  
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="NuVending is a Malaysia leading vending machine supplier by offering best quality of vending machine equipped with latest technology.">Nu Vending</b>
							
                        </td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
						<td class="four-stars-color star-css">★★★★</td>
                        <td class="two-stars-color star-css">★★</td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css">★★★★</td>  
                </tr>                
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="A handmake chocolate workshop that is much more affordable and tastier than the Japan brand. Currently have stores at Kuala Lumpur and Penang.">Love18C Sdn Bhd</b>
							
                        </td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
						<td class="text-center">N/A</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css">★★★★</td>  
                </tr>
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="#1 Malaysia online grocer app, we are specialized in providing grocery shopping online service. It provides a total integrated solution that protects both the consumers and the vendors.">JOCOM MSHOPPING SDN BHD</b>
							
                        </td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
						<td class="text-center">N/A</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="text-center">N/A</td>
						<td class="four-stars-color star-css">★★★★</td>  
                </tr>  
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="Alfred connect homeowners with the property management in all stages starting from handing over of vacant possession to day-to-day management tasks. (i.e. submitting defects, facilities booking and any other applications).">Aspire Asia</b>
							
                        </td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="two-stars-color star-css">★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
						<td class="text-center">N/A</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="text-center">N/A</td>
						<td class="three-stars-color star-css">★★★</td>  
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="Goody Technologies Sdn Bhd is the leading New Generation Digital Media Network in Asia that contains No.1 Chinese Media Site, Crowd-sourcing Content Creators Platform，Data Technologies Platform and Digital Advertising Platform that provides online advertising solutions to advertisers.">Goody Technologies</b>
							
                        </td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="four-stars-color star-css">★★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
						<td class="three-stars-color star-css">★★★</td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="text-center">N/A</td>
						<td class="three-stars-color star-css">★★★</td>  
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900"
                        title="Supabear solves the problems faced by both owner & agent in the property industry. Owner can submit their property details within few clicks and reach out to thousand of agents. Agents will be notified once there is property submitted within their registered area.">SupaBear</b>
							
                        </td>
                        <td class="three-stars-color star-css">★★★</td>
                        <td class="one-stars-color star-css">★</td>
                        <td class="two-stars-color star-css">★★</td>
                        <td class="three-stars-color star-css">★★★</td>
						<td class="two-stars-color star-css">★★</td>
                        <td class="two-stars-color star-css">★★</td>
                        <td class="text-center">N/A</td>
						<td class="two-stars-color star-css">★★</td>  
                </tr>                                                                                                            
            </table>
        </div>        
    </div>
    
    <div id="Unlisted" class="tabcontent">
          	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>
                                                                                      
                    </thead>
                </tr>
               <tr>
                    	<td class="th0-color color-td company-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>
						<td class="th1-color color-td"></td>
                        <td class="th1-color color-td"></td>

						<td class="th7-color color-td"></td>                         
                        
                        <td class="th8-color color-td"></td>

                </tr>           
                <tr>
                    	<td class="th0 company-td">Companies</td>
                        <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                        <td class="th2" title="Details about the products">Product Evaluation</td>
                        <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                        <td class="th4" title="Internal & External analysis">Industry</td>
						<td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                        <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                        <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                        <td class="th8" title="Overall growth of company">Petri Growth</td>                
                
                </tr>
                <tr>
                    	<td class="company-td"><b class="font-weight900">Mypetlibrary</b></td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900">Samofa</b></td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900">Chapter28</b></td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900">Revenews</b></td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr> 
                <tr>
                    	<td class="company-td"><b class="font-weight900">Thousand Media</b></td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr>                                                                                                                                                 
            </table>
        </div>        
    </div>
	<div class="clear"></div>


</div>
</div>


<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background:#e8eef7; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #bed6ff; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #bed6ff; 
}	
</style>
<?php include 'js.php'; ?>
<script>
function openList(evt, listName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(listName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>

