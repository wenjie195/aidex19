<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects in Public | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 overflow menu-distance same-padding">
	<div class="width100 text-center">
    	<img src="img/public-project.png" class="line-icon" alt="Current Projects in Public" title="Current Projects in Public">
	</div>
    <h1 class="title-h1 text-center">Current Projects in Public</h1> 
    <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Listed</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Unlisted</button>
    </div>
    
    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="right-fix-company">
        
        </div>
        <div class="table-scroll-div">
        	<div class="table-header">
            	<div class="th1-big-div big-div">
            		<div class="th1-div">Founder</div>
                    <div class="clear"></div>
                    <div class="th1-a">Working experienced (10%)</div>
                    <div class="th1-a">Business experienced (30%)</div>
                    <div class="th1-a">Experienced in the industry (30%)</div>
                    <div class="th1-a">Awards (5%)</div>
                    <div class="th1-a">Achievement (15%)</div>
                    <div class="th1-a">Qualification (10%)</div>
                    <div class="th1-a">Total score</div>
                </div>
                <div class="th2-big-div big-div">
                	<div class="th1-div">Product</div>
                    <div class="clear"></div>
                    <div class="th1-a">Unique selling point of product (15%)</div>
                    <div class="th1-a">Choices & varieties (10%)</div>
                    <div class="th1-a">Presentation of product (15%)</div>
                    <div class="th1-a">Price (30%)</div>
                    <div class="th1-a">Payment (20%)</div>
                    <div class="th1-a">Problem-solving (10%)</div>
                    <div class="th1-a">Total score</div>
                </div>
                <div class="th3-big-div big-div">
                	<div class="th1-div">Business Model</div>
                    <div class="clear"></div>
                    <div class="th1-a">Scalability (30%)</div>
                    <div class="th1-a">Switchable (10%)</div>
                    <div class="th1-a">Recurring revenues (30%)</div>
                    <div class="th1-a">Protection from competition (30%)</div>
                    <div class="th1-a">Total score</div>
                </div>
                <div class="th4-big-div big-div">
                	<div class="th1-div">Industry</div>
                    <div class="clear"></div>
                    <div class="th1-a">Competitor analysis (30%)</div>
                    <div class="th1-a">Entry barrier (30%)</div> 
                    <div class="th1-a">Power of suppliers (25%)</div>
                    <div class="th1-a">Power of buyers (15%)</div>
                    <div class="th1-a">Total score</div> 
                </div>
                <div class="th5-big-div big-div">
                	<div class="th1-div">Team Evaluation</div>
                    <div class="clear"></div>
                    <div class="th1-a">Working experience (35%)</div>
                    <div class="th1-a">Sales& marketing team (35%)</div>
                    <div class="th1-a">Team qualification (10%)</div>
                    <div class="th1-a">Team achievement (20%)</div>
                    <div class="th1-a">Total score</div>                    
                </div>
                <div class="th6-big-div big-div">
                	<div class="th1-div">Market Readiness</div>
                    <div class="clear"></div>
                    <div class="th1-a">Ask questions from consumer point of view (45%)</div>
                    <div class="th1-a">Google Adword look whether people search for it or not (20%)</div>
                    <div class="th1-a">Numbers of competitor (35%)</div>
                    <div class="th1-a">Total score</div>                    
                </div>
                <div class="th7-big-div big-div">
                <div class="th1-div">&nbsp;</div>
                	<div class="clear"></div>
                    <div class="th1-a">Petri Growth</div>
                </div>
                <div class="th8-big-div big-div">                
                	<div class="th1-div">Performance Matrix</div>
                	<div class="clear"></div>
                    <div class="th1-a">Revenue</div>
                    <div class="th1-a">Cash flow</div>
                    <div class="th1-a">Total Score</div>
                </div>
                <div class="th9-big-div big-div"> 
                	<div class="th1-div">Websites</div>
                    <div class="clear"></div>
                    <div class="th1-a">&nbsp;</div>
                 </div>
            </div>
            <div class="clear"></div>
<!--            <div class="company-name-table">
            	<div class="td1">
                			Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            10 years working experience in Finance and Business<br> 
                            *Hany Cheng (⅔ X 10% = 6.67%)<br>
                            10 years working experience in education & counselling, but not related to their business 
				</div>
                <div class="td1">
                        	Bad (10%)</b><br>
							*This is the 1st business for both founders
                </div>
                <div class="td1">
                        	<b class="font-weight900">Average (15%)</b><br>
                            *Ivan Eng (⅔ X 30% = 20%)<br>
                            A bit touch to business field with 10 years<br>
                            *Hany Cheng (⅓ X 30% = 10%)<br>
                            No touching to business field
                </div>
                <div class="td1">
                        	<b class="font-weight900">Bad (1.67%)</b><br>
							*No business award for both founders.
				</div>
                <div class="td1">
						    <b class="font-weight900">Bad (5%)</b><br>
							*No achievement for both founders.
                </div>
                <div class="td1">
                			<b class="font-weight900">Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            (2012 - 2014) Master of Business Administration (MBA) of Finance, General in Multimedia University<br>
                            (2006 - 2009) Bachelor’s degree of International Business in Multimedia University<br>
                            *Hany Cheng (⅓ X 10% = 3.33%)<br>
                            Not stated, but to become a counsellor should have a diploma or degree certificate.
                </div>
                <div class="td1">
                            45.01% (Average)
				</div>
                <div class="td1">
                            Unique selling point of product (15%)
                </div>
                <div class="td1">
                        	Choices & varieties (10%)
                </div>
                <div class="td1">
                        	Presentation of product (15%)
               	</div>
                <div class="td1">
                        	Price (30%)
                </div>
                <div class="td1">
							Payment (20%)
                </div>
                <div class="td1">
                			Problem-solving (10%)
                </div>
                <div class="td1">
                        	Total Score
                </div>
                <div class="td1">
							Scalability (30%)
                </div>
                <div class="td1">
                        	Switchable (10%)
                </div>
                <div class="td1">
                        	Recurring revenues (30%)
              	</div>
                <div class="td1">
                        	Protection from competition (30%)
                </div>
                <div class="td1">
							Total score
                </div>
                <div class="td1">        
                        	Competitor analysis (30%)
                </div>
                <div class="td1">
                        	Entry barrier (30%)
                </div>
                <div class="td1">
                        	Power of suppliers (25%)
                </div>
                <div class="td1">
                        	Power of buyers (15%)
                </div>
                <div class="td1">
							Total score
                </div>
                <div class="td1">  
                        	Working experience (35%)
             	</div>
                <div class="td1">
                        	Sales& marketing team (35%)
                </div>
                <div class="td1">
                        	Team qualification (10%)
                </div>
                <div class="td1">
                        	Team achievement (20%)
                </div>
                <div class="td1">
							Total score
                </div>    
                <div class="td1">
							Ask questions from consumer point of view (45%)
                </div>                        
                <div class="td1">
							Google Adword look whether people search for it or not (20%)
                </div>  
                <div class="td1">
							Numbers of competitor (35%)
                </div>
                <div class="td1">
							Total score
                </div>
                <div class="td1">
							
                </div>                
                <div class="td1">
						    Petri Growth	
                </div>                        
                <div class="td1">
						    Revenue	
                </div>  
                <div class="td1">
							Cash flow
                </div>                
                <div class="td1">
						    Total Score	
                </div>                        
                <div class="td1">
						    
                </div>                 

            </div>-->
            <table class="company-name-table">
			<tbody>
                <tr>

                        <td width="200px"><b class="font-weight900">Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            10 years working experience in Finance and Business<br> 
                            *Hany Cheng (⅔ X 10% = 6.67%)<br>
                            10 years working experience in education & counselling, but not related to their business 
						</td>
                        <td width="200px"><b class="font-weight900">Bad (10%)</b><br>
							*This is the 1st business for both founders
                        </td>
                        <td><b class="font-weight900">Average (15%)</b><br>
                            *Ivan Eng (⅔ X 30% = 20%)<br>
                            A bit touch to business field with 10 years<br>
                            *Hany Cheng (⅓ X 30% = 10%)<br>
                            No touching to business field
                        </td>
                        <td><b class="font-weight900">Bad (1.67%)</b><br>
							*No business award for both founders.
						</td>
						<td><b class="font-weight900">Bad (5%)</b><br>
							*No achievement for both founders.
                        </td>
                        <td><b class="font-weight900">Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            (2012 - 2014) Master of Business Administration (MBA) of Finance, General in Multimedia University<br>
                            (2006 - 2009) Bachelor’s degree of International Business in Multimedia University<br>
                            *Hany Cheng (⅓ X 10% = 3.33%)<br>
                            Not stated, but to become a counsellor should have a diploma or degree certificate.
                        </td>
                        <td>45.01% (Average)</td>

                        <td>Unique selling point of product (15%)</td>
                        <td>Choices & varieties (10%)</td>
                        <td>Presentation of product (15%)</td>
                        <td>Price (30%)</td>
						<td>Payment (20%)</td>
                        <td>Problem-solving (10%)</td>
                        <td>Total Score</td>  
                        
                        <td>Scalability (30%)</td>
                        <td>Switchable (10%)</td>
                        <td>Recurring revenues (30%)</td>
                        <td>Protection from competition (30%)</td>
						<td>Total score</td>
                        
                        <td>Competitor analysis (30%)</td>
                        <td>Entry barrier (30%)</td>
                        <td>Power of suppliers (25%)</td>
                        <td>Power of buyers (15%)</td>
						<td>Total score</td>      
                        
                        <td>Working experience (35%)</td>
                        <td>Sales& marketing team (35%)</td>
                        <td>Team qualification (10%)</td>
                        <td>Team achievement (20%)</td>
						<td>Total score</td>     
                        
                        <td>Ask questions from consumer point of view (45%)</td>
                        <td>Google Adword look whether people search for it or not (20%)</td>
                        <td>Numbers of competitor (35%)</td>
						<td>Total score</td>                         

						<td>Petri Growth</td>                         
                        
                        <td>Revenue</td>
                        <td>Cash flow</td>
                        <td>Total Score</td>  
                        
                        <td></td> 
                </tr>
                                <tr>

                        <td width="200px"><b class="font-weight900">Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            10 years working experience in Finance and Business<br> 
                            *Hany Cheng (⅔ X 10% = 6.67%)<br>
                            10 years working experience in education & counselling, but not related to their business 
						</td>
                        <td width="200px"><b class="font-weight900">Bad (10%)</b><br>
							*This is the 1st business for both founders
                        </td>
                        <td><b class="font-weight900">Average (15%)</b><br>
                            *Ivan Eng (⅔ X 30% = 20%)<br>
                            A bit touch to business field with 10 years<br>
                            *Hany Cheng (⅓ X 30% = 10%)<br>
                            No touching to business field
                        </td>
                        <td><b class="font-weight900">Bad (1.67%)</b><br>
							*No business award for both founders.
						</td>
						<td><b class="font-weight900">Bad (5%)</b><br>
							*No achievement for both founders.
                        </td>
                        <td><b class="font-weight900">Average (6.67%)</b><br>
                            *Ivan Eng (3/3 X 10% = 10%)<br>
                            (2012 - 2014) Master of Business Administration (MBA) of Finance, General in Multimedia University<br>
                            (2006 - 2009) Bachelor’s degree of International Business in Multimedia University<br>
                            *Hany Cheng (⅓ X 10% = 3.33%)<br>
                            Not stated, but to become a counsellor should have a diploma or degree certificate.
                        </td>
                        <td>45.01% (Average)</td>

                        <td>Unique selling point of product (15%)</td>
                        <td>Choices & varieties (10%)</td>
                        <td>Presentation of product (15%)</td>
                        <td>Price (30%)</td>
						<td>Payment (20%)</td>
                        <td>Problem-solving (10%)</td>
                        <td>Total Score</td>  
                        
                        <td>Scalability (30%)</td>
                        <td>Switchable (10%)</td>
                        <td>Recurring revenues (30%)</td>
                        <td>Protection from competition (30%)</td>
						<td>Total score</td>
                        
                        <td>Competitor analysis (30%)</td>
                        <td>Entry barrier (30%)</td>
                        <td>Power of suppliers (25%)</td>
                        <td>Power of buyers (15%)</td>
						<td>Total score</td>      
                        
                        <td>Working experience (35%)</td>
                        <td>Sales& marketing team (35%)</td>
                        <td>Team qualification (10%)</td>
                        <td>Team achievement (20%)</td>
						<td>Total score</td>     
                        
                        <td>Ask questions from consumer point of view (45%)</td>
                        <td>Google Adword look whether people search for it or not (20%)</td>
                        <td>Numbers of competitor (35%)</td>
						<td>Total score</td>                         

						<td>Petri Growth</td>                         
                        
                        <td>Revenue</td>
                        <td>Cash flow</td>
                        <td>Total Score</td>  
                        
                        <td></td> 
                </tr>
                </tbody>                
            </table>
        </div>        
    </div>
    
    <div id="Unlisted" class="tabcontent">
      <h3>Paris</h3>
      <p>Paris is the capital of France.</p> 
    </div>


</div>
</div>


<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}

</style>
<?php include 'js.php'; ?>
<script>
function openList(evt, listName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(listName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>

