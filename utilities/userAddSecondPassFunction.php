<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid  = $_SESSION['uid'];

    $secondaryPassword  = $_POST['new_second_password'];
    $secondaryPassword_reenter  = $_POST['retype_new_second_password'];

    $second_password_validation = strlen($secondaryPassword);
    // $second_password_reenter_validation = strlen($secondaryPassword_reenter);

    $secPassword = hash('sha256',$second_password_validation);
    $salt1 = substr(sha1(mt_rand()), 0, 100);
    $finalSecPassword = hash('sha256', $salt1.$secPassword);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $secondaryPassword."<br>";
    // echo $secondaryPassword_reenter."<br>";
    // echo $secPassword."<br>";
    // echo $finalSecPassword."<br>";
    // echo $salt1."<br>";
    
    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");   

    if(strlen($secondaryPassword) >= 6 && strlen($secondaryPassword_reenter) >= 6 )
    {
        // echo " GOOD secondary password length is more than six !";

        if($secondaryPassword == $secondaryPassword_reenter)
        {

            if(!$user)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($finalSecPassword)
                {
                    array_push($tableName,"password_two");
                    array_push($tableValue,$finalSecPassword);
                    $stringType .=  "s";
                }
                if($salt1)
                {
                    array_push($tableName,"salt_two");
                    array_push($tableValue,$salt1);
                    $stringType .=  "s";
                }
            
                array_push($tableValue,$uid);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    // echo "success";
                    // $_SESSION['messageType'] = 3;
                    // header('Location: ../profile.php?type=1');
                    // header( "Location: ../member.php" );
                    $_SESSION['messageType'] = 1;
                    header('Location: ../userAddSecondaryPass.php?type=1');
                    // echo "<script>alert('update secondary password successfully');window.location='../member.php'</script>";  
                }
                else
                {
                    // echo "fail";
                    // header('Location: ../userAddSecondaryPass.php?type=2');
                    $_SESSION['messageType'] = 1;
                    header('Location: ../userAddSecondaryPass.php?type=2');
                    // echo "<script>alert('fail to update password');window.location='../userAddSecondaryPass.php'</script>";  
                }
            }
            else
            {
                // echo "user not found";
                // header('Location: ../userAddSecondaryPass.php?type=3');
                $_SESSION['messageType'] = 1;
                header('Location: ../userAddSecondaryPass.php?type=3');
                // echo "<script>alert('user not found');window.location='../userAddSecondaryPass.php'</script>";  
            }

        }
        else 
        {
            // header( "Location: ../userAddSecondaryPass.php?type=4" );
            // echo "password must be same with reenter";
            $_SESSION['messageType'] = 1;
            header('Location: ../userAddSecondaryPass.php?type=4');
            // echo "<script>alert('password must be same with reenter');window.location='../userAddSecondaryPass.php'</script>";  
        }

    }
    else
    {
        // echo " secondary password length must more than six !";
        // header('Location: ../userAddSecondaryPass.php?type=5');
        $_SESSION['messageType'] = 1;
        header('Location: ../userAddSecondaryPass.php?type=5');
        // echo "<script>alert('secondary password length must more than 6');window.location='../userAddSecondaryPass.php'</script>";  
    }
}
else 
{
     header( "Location: ../index.php" );
}
?>