<?php
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

// $uid = $_SESSION['uid'];

// function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phone,$fullname)
// {
//      if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","full_name"),
//           array($uid,$username,$email,$finalPassword,$salt,$phone,$fullname),"sssssss") === null)
//      {
//           header('Location: ../addReferee.php?promptError=1');
//           //     promptError("error registering new account.The account already exist");
//           //     return false;
//      }
//      else{    }
//      return true;
// }

function registerNewUser($conn,$uid,$username,$email,$nationality,$finalPassword,$salt,$phone,$fullname)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","nationality","password","salt","phone_no","full_name"),
          array($uid,$username,$email,$nationality,$finalPassword,$salt,$phone,$fullname),"ssssssss") === null)
     {
          header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

// function sendEmailForVerification($uid) 
// {
//      $conn = connDB();
//      $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

//      $verifyUser_debugMode = 2;
//      $verifyUser_host = "mail.qlianmeng.asia";
//      $verifyUser_usernameThatSendEmail = "noreply@qlianmeng.asia";                   // Sender Acc Username
//      $verifyUser_password = "P&=?^l=j-g5o";                                                      // Sender Acc Password
//      $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
//      $verifyUser_port = 465;                                                                   // SMTP port no
//      $verifyUser_sentFromThisEmailName = "noreply@qlianmeng.asia";                                       // Sender Username
//      $verifyUser_sentFromThisEmail = "noreply@qlianmeng.asia";                       // Sender Email
//      $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                                      // Recipient Username
//      $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
//      $verifyUser_isHtml = true;                                                                // Set To Html
//      $verifyUser_subject = "Welcome to Q LianMeng";                                      // Title

//      $verifyUser_body = "<p>Dear user,</p>"; // Body
//      $verifyUser_body .="<p>Congratulations, your account has been successfully created ! </p>";
//      $verifyUser_body .="<p>Below is the details to login to the website.</p>";
//      $verifyUser_body .="<p>http://www.qlianmeng.asia</p>";
//      $verifyUser_body .="<p>Username : ".$userRows[0]->getUsername()."</p>";
//      $verifyUser_body .="<p>Password : 123321</p>";
//      $verifyUser_body .="<p>For safety purpose, we encourage you to change your password after first time login.</p>";
//      $verifyUser_body .="<p>Best Regards,</p>";
//      $verifyUser_body .="<p>qlianmeng</p>";
     
//           sendMailTo(
//                null,
//                $verifyUser_host,
//                $verifyUser_usernameThatSendEmail,
//                $verifyUser_password,
//                $verifyUser_smtpSecure,
//                $verifyUser_port, 
//                $verifyUser_sentFromThisEmailName,
//                $verifyUser_sentFromThisEmail,
//                $verifyUser_sendToThisEmailName,
//                $verifyUser_sendToThisEmail,
//                $verifyUser_isHtml,
//                $verifyUser_subject,
//                $verifyUser_body,
//                null
//           );
// }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());

     $register_username = rewrite($_POST['register_username']);
     $register_fullname = rewrite($_POST['register_fullname']);

     $register_phone = rewrite($_POST['register_phone']);
     $register_email_user = rewrite($_POST['register_email_user']);

     $register_nationality = rewrite($_POST['register_nationality']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);

     // $register_phone = $_POST['register_phone'];
     // $register_email_user = $_POST['register_email_user'];
     // $register_password = $_POST['register_password'];
     // $register_password_validation = strlen($register_password);
     // $register_retype_password = $_POST['register_retype_password'];

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_username."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";
     // echo $register_password."<br>";
     // echo $register_retype_password."<br>";

     // echo $password."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    
                    $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                    $usernameDetails = $usernameRows[0];

                    $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                    $fullnameDetails = $fullnameRows[0];

                    $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                    $userEmailDetails = $userEmailRows[0];

                    $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
                    $userPhoneDetails = $userPhoneRows[0];

                    if (!$usernameDetails && !$fullnameDetails && !$userEmailDetails && !$userPhoneDetails)
                    {
                         // if(registerNewUser($conn,$register_uid,$register_username,$register_fullname,$register_phone,$register_email_user,$finalPassword,$salt))
                         // if(registerNewUser($conn,$register_uid,$register_username,$register_email_user,$finalPassword,$salt,$register_phone,$register_fullname))

                         if(registerNewUser($conn,$register_uid,$register_username,$register_email_user,$register_nationality,$finalPassword,$salt,$register_phone,$register_fullname))
                         {
                              // sendEmailForVerification($register_uid);
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../profile.php?type=1');
                              echo "register success";
                         }
                    }
                    else
                    {
                         header('Location: ../addReferee.php?promptError=1');
                    }
               }
               else 
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addReferee.php?type=5');
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addReferee.php?type=5');
          }      
}
else 
{
     header('Location: ../addReferee.php');
}

?>