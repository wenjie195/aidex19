<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid  = $_POST['checkThat'];
     // $verify_Code  = $_POST['verify_Code'];
     // $verify_Pass  = $_POST['verify_Pass'];
     // $verify_Reenter  = $_POST['verify_Reenter'];
     $verify_Code  = $_POST['verify_code'];
     $verify_Pass  = $_POST['new_password'];
     $verify_Reenter  = $_POST['retype_new_password'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $dbPass =  $user[0]->getPassword();
     $dbSalt =  $user[0]->getSalt();

     $finalPassword1 = hash('sha256', $dbSalt.$verify_Code);

     if($finalPassword1 == $dbPass)
     {
          if(strlen($verify_Pass) >= 6 || strlen($verify_Reenter) >= 6 )
          {
               if($verify_Pass == $verify_Reenter)
               {
                    $password = hash('sha256',$verify_Pass);
                    $salt = substr(sha1(mt_rand()), 0, 100);
                    $finalPassword = hash('sha256', $salt.$password);

                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$user[0]->getUid()),"sss");
                    if($passwordUpdated)
                    {
                         // echo "Successfullly changed password";
                         // echo "<script>alert('successfullly changed password');window.location='../index.php'</script>"; 
                         $_SESSION['messageType'] = 4;
                         header('Location: ../index.php?type=1');
                    }
                    else 
                    {
                         // echo "Server problem";
                         // echo "unable to change password";
                         // echo "<script>alert('unable to change password !');window.location='../resetPassword.php'</script>"; 
                         $_SESSION['messageType'] = 4;
                         header('Location: ../index.php?type=2');
                    }
               }
               else 
               {
                    // echo "password does not match";
                    // echo "<script>alert('unable to change password !');window.location='../resetPassword.php'</script>"; 
                    $_SESSION['messageType'] = 4;
                    header('Location: ../index.php?type=3');
               }
          }
          else 
          {
               // echo "password must be more than 5";
               // echo "<script>alert('password must be more than 5 !');window.location='../resetPassword.php'</script>"; 
               $_SESSION['messageType'] = 4;
               header('Location: ../index.php?type=4');
          }
     }
     else 
     {
          // echo "wrong code verification";
          // echo "<script>alert('wrong verification code !');window.location='../resetPassword.php'</script>"; 
          $_SESSION['messageType'] = 4;
          header('Location: ../index.php?type=5');
     }
}
else 
{
     header('Location: ../index.php');
}
?>