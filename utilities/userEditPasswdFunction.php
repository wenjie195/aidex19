<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid  = $_SESSION['uid'];
     
     $editPassword_new  = $_POST['new_password'];
     $editPassword_reenter  = $_POST['retype_new_password'];

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_new."<br>";
     // echo $editPassword_reenter."<br>";

     if(strlen($editPassword_new) >= 6 && strlen($editPassword_reenter) >= 6 )
     {
          if($editPassword_new == $editPassword_reenter)
          {
               $password = hash('sha256',$editPassword_new);
               $salt = substr(sha1(mt_rand()), 0, 100);
               $finalPassword = hash('sha256', $salt.$password);

               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$uid),"sss");
               if($passwordUpdated)
               {
                    // header( "Location: ../member.php" );
                    // echo "update password success ";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../userEditCurrentPass.php?type=1');
                    // echo "<script>alert('update password successfully');window.location='../member.php'</script>";  
               }
               else 
               {
                    // header( "Location: ../userEditCurrentPass.php?type=2" );
                    // echo "server problem ";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../userEditCurrentPass.php?type=2');
                    // echo "<script>alert('fail to update password');window.location='../userEditCurrentPass.php'</script>";  
               }
          }
          else 
          {
               // header( "Location: ../userEditCurrentPass.php?type=3" );
               // echo "password must be same with reenter ";
               $_SESSION['messageType'] = 1;
               header('Location: ../userEditCurrentPass.php?type=3');
               // echo "<script>alert('password must be same with reenter password');window.location='../userEditCurrentPass.php'</script>";  
          }
     }
     else 
     {
          // header( "Location: ../userEditCurrentPass.php?type=4" );
          // echo "password length must be more than 6 ";
          $_SESSION['messageType'] = 1;
          header('Location: ../userEditCurrentPass.php?type=4');
          // echo "<script>alert('password length must be more than 6');window.location='../userEditCurrentPass.php'</script>";  
     }
   
}
else 
{
     header( "Location: ../index.php" );
}
?>