<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';


function generateRandomString($length = 10) 
{
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
     $forgotPassword_email = rewrite($_POST['forgot_password']);
     $forgotPassword_email = filter_var($forgotPassword_email, FILTER_SANITIZE_EMAIL);
     
     if(filter_var($forgotPassword_email, FILTER_VALIDATE_EMAIL))
     {
          $tempPass = generateRandomString();

          $tempPass = hash('sha256',$tempPass);
          $salt = substr(sha1(mt_rand()), 0, 100);
          $finalPassword = hash('sha256', $salt.$tempPass);

          $passwordUpdated = updateDynamicData($conn,"user"," WHERE email = ? ",array("password","salt"),array($finalPassword,$salt,$forgotPassword_email),"sss");
          // var_dump($passwordUpdated);

          // echo $forgotPassword_email;

          if($passwordUpdated)
          {
               $user = getUser($conn," WHERE email = ? ",array("email"),array($forgotPassword_email),"s");
               // var_dump($user);
               $verifyUser_debugMode = 2;
               $verifyUser_host = "mail.aidex.sg";
               $verifyUser_usernameThatSendEmail = "noreply@aidex.sg";                   // Sender Acc Username
               $verifyUser_password = "pnBm4b3y5NjL";                                                      // Sender Acc Password
               $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
               $verifyUser_port = 465;                                                                   // SMTP port no
               $verifyUser_sentFromThisEmailName = "noreply@aidex.sg";                                       // Sender Username
               $verifyUser_sentFromThisEmail = "noreply@aidex.sg";                          // Sender Email
               $verifyUser_sendToThisEmailName = $user[0]->getUsername();                                // Recipient Username
               $verifyUser_sendToThisEmail = $forgotPassword_email;                                      // Recipient Email
               $verifyUser_isHtml = true;                                                                // Set To Html
               $verifyUser_subject = "Reset Password";  
     
               // $verifyUser_body = "<p>Please reset your password in this ";
               // $verifyUser_body .="<a href='http://aidex.sg/testing/resetPassword.php?uid=".$user[0]->getUid()."'>link</a>  ";
               // $verifyUser_body .="using this key code below</p>";
               // $verifyUser_body .="<p>Link to Reset Password  =  <a href='http://aidex.sg/testing/resetPassword.php?uid=".$user[0]->getUid()."'>http://aidex.sg/testing/resetPassword.php?uid=".$user[0]->getUid()."</a></p>";
               // $verifyUser_body .="<p>Key Code = ".$tempPass."</p>";

               $verifyUser_body = "<p>Please reset your password in this ";
               $verifyUser_body .="<a href='http://aidex.sg/resetPassword.php?uid=".$user[0]->getUid()."'>link</a>  ";
               $verifyUser_body .="using this key code below</p>";
               $verifyUser_body .="<p>Link to Reset Password  =  <a href='http://aidex.sg/resetPassword.php?uid=".$user[0]->getUid()."'>http://aidex.sg/resetPassword.php?uid=".$user[0]->getUid()."</a></p>";
               $verifyUser_body .="<p>Key Code = ".$tempPass."</p>";

               // $verifyUser_body = "<p>Hi ".$userRows[0]->getUsername().",</p>";          // Body
               // $verifyUser_body .="<p>Welcome to AIDEX</p>";
               // $verifyUser_body .="<p>Please click the link below to activate your account.</p>";
               // // $verifyUser_body .="<a href='https://aidex.sg/testing/emailVerified.php?getVerified=".$uid."'>link</a>";
               // $verifyUser_body .="<p>https://aidex.sg/testing/emailVerified.php?getVerified=".$uid."</p>";
               // // $verifyUser_body .="<p>https://aidex.sg</p>";
               // $verifyUser_body .="<p>If you run into any problems during activation, contact us at support@aidex.com and we’ll be happy to help.</p>";
               // $verifyUser_body .="<p></p>";
               // $verifyUser_body .="<p>Best,</p>";
               // $verifyUser_body .="<p>The AIDEX team</p>";


               sendMailTo(
                    null,
                    $verifyUser_host,
                    $verifyUser_usernameThatSendEmail,
                    $verifyUser_password,
                    $verifyUser_smtpSecure,
                    $verifyUser_port, 
                    $verifyUser_sentFromThisEmailName,
                    $verifyUser_sentFromThisEmail,
                    $verifyUser_sendToThisEmailName,
                    $verifyUser_sendToThisEmail,
                    $verifyUser_isHtml,
                    $verifyUser_subject,
                    $verifyUser_body,
                    null
               );
               // $_SESSION['messageType'] = 1;
               // header('Location: ../index.php?type=8');
               // //echo "// forgot email success send email ";
               $_SESSION['messageType'] = 3;
               header('Location: ../index.php?type=1');
               // echo "<script>alert('reset password link has been sent to your email');window.location='../index.php'</script>"; 
          }
          else 
          {
               // echo "no user with ths email ";
               $_SESSION['messageType'] = 3;
               header('Location: ../index.php?type=2');
               // echo "<script>alert('no user with ths email !');window.location='../index.php'</script>"; 
          }

     }
     else 
     {
          // echo "wrong email format ";
          $_SESSION['messageType'] = 3;
          header('Location: ../index.php?type=3');
          // echo "<script>alert('wrong email format  !');window.location='../index.php'</script>"; 
     }
}
else 
{
     header('Location: ../index.php');
}
?>