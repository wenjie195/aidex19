<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$username,$email,$nationality,$finalPassword,$salt,$phone,$fullname)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","nationality","password","salt","phone_no","full_name"),
          array($uid,$username,$email,$nationality,$finalPassword,$salt,$phone,$fullname),"ssssssss") === null)
     {
          header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function sendEmailForVerification($uid) 
{
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.aidex.sg";
     $verifyUser_usernameThatSendEmail = "noreply@aidex.sg";                   // Sender Acc Username
     $verifyUser_password = "pnBm4b3y5NjL";         

     $verifyUser_smtpSecure = "ssl";                                           // SMTP type
     $verifyUser_port = 465;                                                   // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@aidex.sg";                   // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@aidex.sg";                       // Sender Email

     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();            // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                   // Recipient Email
     $verifyUser_isHtml = true;                                                // Set To Html
     $verifyUser_subject = "Welcome To AIDEX";       

     $verifyUser_body = "<p>Hi ".$userRows[0]->getUsername().",</p>";          // Body
     $verifyUser_body .="<p>Welcome to AIDEX</p>";
     $verifyUser_body .="<p>Please click the link below to activate your account.</p>";

     $verifyUser_body .="<p>https://aidex.sg/emailVerified.php?getVerified=".$uid."</p>";
     // $verifyUser_body .="<p>https://aidex.sg/testing/emailVerified.php?getVerified=".$uid."</p>";
     // $verifyUser_body .="<p>https://aidex.sg</p>";

     $verifyUser_body .="<p>If you run into any problems during activation, contact us at support@aidex.com and we’ll be happy to help.</p>";
     $verifyUser_body .="<p></p>";
     $verifyUser_body .="<p>Best,</p>";
     $verifyUser_body .="<p>The AIDEX team</p>";

     
    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port, 
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());

     $register_username = rewrite($_POST['register_username']);
     // $register_fullname = rewrite($_POST['register_fullname']);
     $register_fullname = "";

     $register_phone = rewrite($_POST['register_phone']);
     $register_email_user = rewrite($_POST['register_email_user']);

     $register_nationality = rewrite($_POST['register_nationality']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);

     $sponsorID = rewrite($_POST['upline_uid']);

     // $register_phone = $_POST['register_phone'];
     // $register_email_user = $_POST['register_email_user'];
     // $register_password = $_POST['register_password'];
     // $register_password_validation = strlen($register_password);
     // $register_retype_password = $_POST['register_retype_password'];

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_username."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";
     // echo $register_password."<br>";
     // echo $register_retype_password."<br>";

     // echo $password."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

     if($sponsorID)
     {
          $referrerUserRows = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorID),"s");
          if($referrerUserRows)
          {
               $referrerUid = $referrerUserRows[0]->getUid();
               $referrerName = $referrerUserRows[0]->getUsername();
               // $referralUid = $referrerUserRows[0]->getUid();
               // $referralName = $username;
               $referralName = $register_username;
               $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
               $currentLevel = 1;
               $getUplineCurrentLevel = 1;

               $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
               if($referralHistoryRows)
               {
                    $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                    $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
               }
               $referralNewestRows = getReferralHistory($conn,"WHERE referral_name = ?", array("referral_name"),array($referrerName), "s");
               if($referralNewestRows)
               {
                    $getUplineCurrentLevel = $referralNewestRows[0]->getCurrentLevel() + 1;

                    if($register_password == $register_retype_password)
                    {
                         if($register_password_validation >= 6)
                         {
                              
                              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                              $usernameDetails = $usernameRows[0];

                              $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                              $userEmailDetails = $userEmailRows[0];

                              $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
                              $userPhoneDetails = $userPhoneRows[0];

                              if (!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
                              {
                                   if(registerNewUser($conn,$register_uid,$register_username,$register_email_user,$register_nationality,$finalPassword,$salt,$register_phone,$register_fullname))
                                   {
                                        // $_SESSION['messageType'] = 1;
                                        // header('Location: ../profile.php?type=1');
                                        // sendEmailForVerification($register_uid);
                                        // echo "<script>alert('register success');window.location='../index.php'</script>";  

                                        $uid = $register_uid;
                                        // $referralName = $register_username;

                                        if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                                        {          
                                             // sendEmailForVerification($uid);
                                             // echo "<script>alert('register successfully');window.location='../index.php'</script>";  
                                             sendEmailForVerification($uid);
                                             // $_SESSION['messageType'] = 2;
                                             // header('Location: ../index.php?type=1');
                                             $_SESSION['messageType'] = 5;
                                             header('Location: ../index.php?type=1');
                                        }
                                        else
                                        { 
                                             // // echo "fail to register via upline";
                                             // echo "<script>alert('register success 1 level of data store');window.location='../index.php'</script>";  
                                             $_SESSION['messageType'] = 2;
                                             header('Location: ../index.php?type=2');
                                        }

                                   }
                                   else
                                   { 
                                        $_SESSION['messageType'] = 2;
                                        header('Location: ../index.php?type=3');
                                        // echo "fail to register via upline";
                                        // echo "<script>alert('fail to register');window.location='../index.php'</script>";  
                                   }
                              }
                              else
                              {
                                   // echo "input data for register already taken by other user";
                                   $_SESSION['messageType'] = 2;
                                   header('Location: ../index.php?type=4');
                                   // echo "<script>alert('input data for register already taken by other user');window.location='../index.php'</script>";    
                              }
                         }
                         else 
                         {
                              // echo "password length must be more than 6";
                              $_SESSION['messageType'] = 2;
                              header('Location: ../index.php?type=5');
                              // echo "<script>alert('password length must be more than 6');window.location='../index.php'</script>";    
                         }
                    }
                    else 
                    {
                         // echo "password and retype password not the same";
                         $_SESSION['messageType'] = 2;
                         header('Location: ../index.php?type=6');
                         // echo "<script>alert('password and retype password not the same');window.location='../index.php'</script>";    
                    }   
                    
               }
               else
               {
                    // echo "register error with referral ";
                    $_SESSION['messageType'] = 2;
                    header('Location: ../index.php?type=7');
                    // echo "<script>alert('register error with referral ');window.location='../index.php'</script>"; 
               }

          }
          else
          {
               // echo "unable to find related data sponsor ID!!";
               $_SESSION['messageType'] = 2;
               header('Location: ../index.php?type=8');
               // echo "<script>alert('unable to find related data sponsor ID!!');window.location='../index.php'</script>"; 
          } 
     }
     else
     {
          // echo "sponsor ID is unavailible !!";
          $_SESSION['messageType'] = 2;
          header('Location: ../index.php?type=9');
          // echo "<script>alert('sponsor ID is unavailible !!');window.location='../index.php'</script>"; 
     }
}
else 
{
     header('Location: ../index.php');
}
?>