<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
   $conn = connDB();

   $id = $uid;

   $oldFavorite = $_POST["favorite_old"];
   $oldFavoriteExp = explode(",",$oldFavorite);
   $newFavorite = $_POST["favorite"];
   if ($oldFavorite)
   {
      $ArrImplode = $oldFavorite.",".$newFavorite;
   }
   else 
   {
      $ArrImplode = $newFavorite;
   }

   // //   FOR DEBUGGING
   // echo "<br>";
   // echo $id."<br>";
   // echo $oldFavorite."<br>";
   // echo $newFavorite."<br>";
   // echo $ArrImplode."<br>";

   if(isset($_POST['favorite']))
   {
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($ArrImplode)
      {
         array_push($tableName,"favorite_project");
         array_push($tableValue,$ArrImplode);
         $stringType .=  "s";
      }

      array_push($tableValue,$id);
      $stringType .=  "s";
      $addFavorite = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($addFavorite)
      {
         // echo "favorite added";
         header('Location: ../currentProjectPublic.php');
      }
      else
      {
         // echo "fail";
         header('Location: ../currentProjectPublic.php');
      }
   }
   else
   {
      // echo "error";
      header('Location: ../currentProjectPublic.php');
   }

   if(isset($_POST['favorite_selected']))
   {

      $oldFavorite = $_POST["favorite_old"];
      $oldFavoriteExp = explode(",",$oldFavorite);
      $newFavorite = $_POST["favorite_selected"];
      if ($oldFavorite) 
      {
         $ArrImplode = $oldFavorite;
      }
      for ($i=0; $i <count($oldFavoriteExp) ; $i++) 
      {
         $oldFavoriteArray[] = $oldFavoriteExp[$i];
      }

      $ArrImplode = array_diff( $oldFavoriteArray, array($newFavorite) );
      $ArrImplode = implode(",",$ArrImplode);
      
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($ArrImplode)
      {
         array_push($tableName,"favorite_project");
         array_push($tableValue,$ArrImplode);
         $stringType .=  "s";
      }

      array_push($tableValue,$id);
      $stringType .=  "s";
      $addFavorite = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($addFavorite)
      {
         // echo "favorite added";
         header('Location: ../currentProjectPublic.php');
      }
      else
      {
         // echo "fail";
         header('Location: ../currentProjectPublic.php');
      }
   }
   else
   {
      // echo "error";
      header('Location: ../currentProjectPublic.php');
   }

}
else
{
   //echo "dunno";
   header('Location: ../index.php');
}
?>
