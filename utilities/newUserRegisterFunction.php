<?php
if (session_id() == "")
{
     session_start();
}

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$username,$email,$nationality,$finalPassword,$salt,$phone,$fullname)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","nationality","password","salt","phone_no","full_name"),
          array($uid,$username,$email,$nationality,$finalPassword,$salt,$phone,$fullname),"ssssssss") === null)
     {
          header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid)
{
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
     array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function sendEmailForVerification($uid) 
{
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.aidex.sg";
     $verifyUser_usernameThatSendEmail = "noreply@aidex.sg";                   // Sender Acc Username
     $verifyUser_password = "pnBm4b3y5NjL";         

     $verifyUser_smtpSecure = "ssl";                                           // SMTP type
     $verifyUser_port = 465;                                                   // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@aidex.sg";                   // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@aidex.sg";                       // Sender Email

     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();            // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                   // Recipient Email
     $verifyUser_isHtml = true;                                                // Set To Html
     $verifyUser_subject = "Welcome To AIDEX";       

     $verifyUser_body = "<p>Hi ".$userRows[0]->getUsername().",</p>";          // Body
     $verifyUser_body .="<p>Welcome to AIDEX</p>";
     $verifyUser_body .="<p>Please click the link below to activate your account.</p>";

     $verifyUser_body .="<p>https://aidex.sg/emailVerified.php?getVerified=".$uid."</p>";
     // $verifyUser_body .="<p>https://aidex.sg/testing/emailVerified.php?getVerified=".$uid."</p>";
     // $verifyUser_body .="<p>https://aidex.sg</p>";

     $verifyUser_body .="<p>If you run into any problems during activation, contact us at support@aidex.com and we’ll be happy to help.</p>";
     $verifyUser_body .="<p></p>";
     $verifyUser_body .="<p>Best,</p>";
     $verifyUser_body .="<p>The AIDEX team</p>";

     
    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port, 
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());

     $register_username = rewrite($_POST['register_username']);
     // $register_fullname = rewrite($_POST['register_fullname']);
     $register_fullname = "";

     $register_phone = rewrite($_POST['register_phone']);
     $register_email_user = rewrite($_POST['register_email_user']);

     $register_nationality = rewrite($_POST['register_nationality']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);

     // $register_phone = $_POST['register_phone'];
     // $register_email_user = $_POST['register_email_user'];
     // $register_password = $_POST['register_password'];
     // $register_password_validation = strlen($register_password);
     // $register_retype_password = $_POST['register_retype_password'];

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_username."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";
     // echo $register_password."<br>";
     // echo $register_retype_password."<br>";

     // echo $password."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    
                    $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                    $usernameDetails = $usernameRows[0];

                    // $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                    // $fullnameDetails = $fullnameRows[0];

                    $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                    $userEmailDetails = $userEmailRows[0];

                    $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
                    $userPhoneDetails = $userPhoneRows[0];

                    // if (!$usernameDetails && !$fullnameDetails && !$userEmailDetails && !$userPhoneDetails)
                    if (!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
                    {
                         // if(registerNewUser($conn,$register_uid,$register_username,$register_fullname,$register_phone,$register_email_user,$finalPassword,$salt))
                         // if(registerNewUser($conn,$register_uid,$register_username,$register_email_user,$finalPassword,$salt,$register_phone,$register_fullname))

                         if(registerNewUser($conn,$register_uid,$register_username,$register_email_user,$register_nationality,$finalPassword,$salt,$register_phone,$register_fullname))
                         {
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../profile.php?type=1');
                              // sendEmailForVerification($register_uid);
                              // echo "<script>alert('register success');window.location='../index.php'</script>";  
                              

                              $referrerUid = $register_uid;
                              $uid = $register_uid;
                              $referralName = $register_username;
                              $currentLevel = "0";
                              $topReferrerUid = $register_uid;

                              if(referralData($conn,$referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid))
                              {          
                                   sendEmailForVerification($uid);
                                   // $_SESSION['messageType'] = 2;
                                   // header('Location: ../index.php?type=1');
                                   $_SESSION['messageType'] = 5;
                                   header('Location: ../index.php?type=1');
                                   // echo "<script>alert('register successfully');window.location='../index.php'</script>";  
                                   // echo "register successfully";
                                   // echo "<script>alert('register successfully');window.location='../index.php'</script>";  
                              }
                              else
                              {
                                   $_SESSION['messageType'] = 2;
                                   header('Location: ../index.php?type=2');
                                   // // echo "fail to register via upline";
                                   //echo "<script>alert('register success 1 level of data store');window.location='../index.php'</script>";  
                              }
                         }
                         else
                         { 
                              $_SESSION['messageType'] = 2;
                              header('Location: ../index.php?type=3');
                              // echo "fail to register via upline";
                              // echo "<script>alert('fail to register');window.location='../index.php'</script>";  
                         }
                    }
                    else
                    {
                         $_SESSION['messageType'] = 2;
                         header('Location: ../index.php?type=4');
                         // echo "input data for register already taken by other user";
                         //echo "<script>alert('input data for register already taken by other user');window.location='../index.php'</script>";    
                    }
               }
               else 
               {
                    $_SESSION['messageType'] = 2;
                    header('Location: ../index.php?type=5');
                    // echo "password length must be more than 6";
                    // echo "<script>alert('password length must be more than 6');window.location='../index.php'</script>";    
               }
          }
          else 
          {
               $_SESSION['messageType'] = 2;
               header('Location: ../index.php?type=6');
               // echo "password and retype password not the same";
               //echo "<script>alert('password and retype password not the same');window.location='../index.php'</script>";    
          }      
}
else 
{
     header('Location: ../index.php');
}
?>