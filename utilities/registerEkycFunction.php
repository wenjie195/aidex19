<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Ekyc.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

function userRegisterEkyc($conn,$uid,$country,$firstname,$lastname,$prooftype,$icFront,$icBack)
{
     if(insertDynamicData($conn,"ekyc",array("uid","country","firstname","lastname","proof_id_type","ic_front","ic_back"),
          array($uid,$country,$firstname,$lastname,$prooftype,$icFront,$icBack),"sssssss") === null)
     {
          // header('Location: ../addReferee.php?promptError=1');
          promptError("error registering new account.The account already exist");
          return false;
          // echo "ggggg";
     }
     else{    }
     return true;
}

function userRegisterEkycPassport($conn,$uid,$country,$firstname,$lastname,$prooftype,$passport)
{
     if(insertDynamicData($conn,"ekyc",array("uid","country","firstname","lastname","proof_id_type","passport"),
          array($uid,$country,$firstname,$lastname,$prooftype,$passport),"ssssss") === null)
     {
          // header('Location: ../addReferee.php?promptError=1');
          promptError("error registering new account.The account already exist");
          return false;
          // echo "ggggg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $userUid;

     // $username = "-";

     $country = rewrite($_POST['ekyc_country']);
     $firstname = rewrite($_POST['ekyc_firstname']);
     $lastname = rewrite($_POST['ekyc_lastname']);
     $prooftype = rewrite($_POST['ekyc_prooftype']);

     $ekycUpdate = "YES";
     
     // $country = "1";
     // $firstname = "2";
     // $lastname = "3";
     // $prooftype = "4";

     $icFront = $_FILES['file_icfront']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file_icfront"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file_icfront']['tmp_name'],$target_dir.$icFront);
     }

     $icBack = $_FILES['file_icback']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file_icback"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file_icback']['tmp_name'],$target_dir.$icBack);
     }

     $passport = $_FILES['file_passport']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file_passport"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file_passport']['tmp_name'],$target_dir.$passport);
     }

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $country."<br>";
     // echo $firstname."<br>";
     // echo $lastname."<br>";
     // echo $prooftype."<br>";

     if($prooftype == "IC")
     {

          if(userRegisterEkyc($conn,$uid,$country,$firstname,$lastname,$prooftype,$icFront,$icBack))
          {

               if(isset($_POST['submit']))
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    if($ekycUpdate)
                    {
                         array_push($tableName,"ekyc_update");
                         array_push($tableValue,$ekycUpdate);
                         $stringType .=  "s";
                    } 
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $ekycIcUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($ekycIcUpdated)
                    {
                         // header('Location: ../uploadLicense.php');
                         // echo "successfully update ekyc data 1 !";
                         // header('Location: ../kycVerification.php?type=11');
                         header('Location: ../thankYou.php');

                         // header('Location: ../index.php');
                         // echo "Thank you for registering.";
                         // echo "Due to current high number of registration,";
                         // echo "we will notify you when you have passed our e-kyc process";

                    }
                    else
                    {
                         // header('Location: ../kycVerification.php?type=2');
                         $_SESSION['messageType'] = 1;
                         header('Location: ../kycVerification.php?type=2');
                         // echo "<script>alert('fail to update ekyc in user table');window.location='../kycVerification.php'</script>";  
                    }
               }
               else
               {
                    // header('Location: ../kycVerification.php?type=3');
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kycVerification.php?type=3');
                    // echo "<script>alert('error');window.location='../kycVerification.php'</script>";  
               }

          }
          else
          {
               // header('Location: ../kycVerification.php?type=3');
               $_SESSION['messageType'] = 1;
               header('Location: ../kycVerification.php?type=4');
               // echo "<script>alert('unable to update ekyc details');window.location='../kycVerification.php'</script>";  
          }

     }
     elseif($prooftype == "Passport")
     {

          if(userRegisterEkycPassport($conn,$uid,$country,$firstname,$lastname,$prooftype,$passport))
          {

               if(isset($_POST['submit']))
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    if($ekycUpdate)
                    {
                         array_push($tableName,"ekyc_update");
                         array_push($tableValue,$ekycUpdate);
                         $stringType .=  "s";
                    } 
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $ekycPassUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($ekycPassUpdated)
                    {
                         // header('Location: ../uploadLicense.php');
                         // echo "successfully update ekyc data 2 !";
                         // header('Location: ../kycVerification.php?type=12');
                         header('Location: ../thankYou.php');

                         // header('Location: ../index.php');
                         // echo "Thank you for registering.";
                         // echo "Due to current high number of registration,";
                         // echo "we will notify you when you have passed our e-kyc process";

                    }
                    else
                    {
                         // header('Location: ../kycVerification.php?type=2');
                         $_SESSION['messageType'] = 1;
                         header('Location: ../kycVerification.php?type=2');
                         // echo "<script>alert('fail to update ekyc in user table');window.location='../kycVerification.php'</script>";  
                    }
               }
               else
               {
                    // header('Location: ../kycVerification.php?type=3');
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kycVerification.php?type=3');
                    // echo "<script>alert('error');window.location='../kycVerification.php'</script>";  
               }

          }
          else
          {
               // header('Location: ../kycVerification.php?type=3');
               $_SESSION['messageType'] = 1;
               header('Location: ../kycVerification.php?type=4');
               // echo "<script>alert('unable to update ekyc details');window.location='../kycVerification.php'</script>";  
          }

     }
     elseif($prooftype == " ")
     {
          // echo "no image found";
          // header('Location: ../kycVerification.php?type=4');
          $_SESSION['messageType'] = 1;
          header('Location: ../kycVerification.php?type=5');
          // echo "<script>alert('no image uploaded');window.location='../kycVerification.php'</script>";  
     }
     else
     {
          // echo "no image found";
          // header('Location: ../kycVerification.php?type=4');
          $_SESSION['messageType'] = 1;
          header('Location: ../kycVerification.php?type=6');
          // echo "<script>alert('no image found');window.location='../kycVerification.php'</script>";  
     }

     // if(userRegisterEkyc($conn,$uid,$country,$firstname,$lastname,$prooftype,$icFront,$icBack))
     // {
     //      echo "success";
     // }

     // if(userRegisterEkycPassport($conn,$uid,$country,$firstname,$lastname,$prooftype,$passport))
     // {
     //      echo "success";
     // }
  
}
else 
{
     header('Location: ../index.php');
}
?>