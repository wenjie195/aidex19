<?php
if (session_id() == "")
{
    session_start();
}

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        $username = rewrite($_POST['username']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {
                    // if(isset($_POST['remember-me'])) 
                    // {
                        
                    //     // setcookie('email-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                    //     // echo 'remember me';
                    // }
                    // else 
                    // {
                    //     // setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                    //     // echo 'null';
                    // }

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['usertype_level'] = $user->getUserType();
                    
                    // if($user->getEmailVerified() == 'YES')
                    if($user->getEmailVerified() == 2)
                    {
                        if($user->getUserType() == 0)
                        {
                            $_SESSION['messageType'] = 1;
                            header('Location: ../index.php?type=1');
                            // header('Location: ../adminDashboard.php');
                            // echo "admin page";
                        }
                        else
                        {
                            header('Location: ../member.php');
                        }
                    }
                    elseif($user->getEmailVerified() == 1)
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=2');
                        // echo "email not verify yet";
                        // echo "<script>alert('email not verify yet');window.location='../index.php'</script>";  
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=3');
                        // echo "unknown error";
                        //echo "<script>alert('unknown error');window.location='../index.php'</script>";  
                    }
                }
                else 
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=4');
                    // promptError("Incorrect email or password");
                    // echo "incorrect username or password";
                    //echo "<script>alert('incorrect password');window.location='../index.php'</script>";  
                }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../index.php?type=5');
            // echo "no user with this email";
            // promptError("This account does not exist");
            //echo "<script>alert('no user with this username');window.location='../index.php'</script>";  
        }
    }

    $conn->close();
}
?>