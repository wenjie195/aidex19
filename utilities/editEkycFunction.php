<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Ekyc.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $userUid;

     $country = rewrite($_POST['edit_ekyc_country']);
     $firstname = rewrite($_POST['edit_ekyc_firstname']);
     $lastname = rewrite($_POST['edit_ekyc_lastname']);
     $prooftype = rewrite($_POST['edit_ekyc_prooftype']);

     $icFront = $_FILES['file_icfront']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file_icfront"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file_icfront']['tmp_name'],$target_dir.$icFront);
     }

     $icBack = $_FILES['file_icback']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file_icback"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file_icback']['tmp_name'],$target_dir.$icBack);
     }

     $passport = $_FILES['file_passport']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file_passport"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file_passport']['tmp_name'],$target_dir.$passport);
     }
     
     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $country."<br>";
     // echo $firstname."<br>";
     // echo $lastname."<br>";
     // echo $prooftype."<br>";

     // if($prooftype == )
     if($prooftype == "IC")
     {

          $removePassport = "no";

          if(isset($_POST['editSubmit']))
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               // //echo "save to database";
               if($country)
               {
                    array_push($tableName,"country");
                    array_push($tableValue,$country);
                    $stringType .=  "s";
               }
               if($firstname)
               {
                    array_push($tableName,"firstname");
                    array_push($tableValue,$firstname);
                    $stringType .=  "s";
               }
               if($lastname)
               {
                    array_push($tableName,"lastname");
                    array_push($tableValue,$lastname);
                    $stringType .=  "s";
               }
               if($prooftype)
               {
                    array_push($tableName,"proof_id_type");
                    array_push($tableValue,$prooftype);
                    $stringType .=  "s";
               }
               if($icFront)
               {
                    array_push($tableName,"ic_front");
                    array_push($tableValue,$icFront);
                    $stringType .=  "s";
               }
               if($icBack)
               {
                    array_push($tableName,"ic_back");
                    array_push($tableValue,$icBack);
                    $stringType .=  "s";
               }
               if($removePassport)
               {
                    array_push($tableName,"passport");
                    array_push($tableValue,$removePassport);
                    $stringType .=  "s";
               }
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $updateEkycDetails = updateDynamicData($conn,"ekyc"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateEkycDetails)
               {
                    // echo "<script>alert('Data Updated !');window.location='../member.php'</script>"; 
                    // echo "data updated !!";
                    // header('Location: ../thankYou.php');
                    header('Location: ../thankYouUpdate.php');
               }
               else
               {    
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kycVerification.php?type=2');
                    // echo "<script>alert('unable to update data !');window.location='../userEditEkycDetails.php'</script>";   
                    // echo "fail to update data !!";   
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../kycVerification.php?type=3');
               // echo "<script>alert('error');window.location='../userEditEkycDetails.php'</script>";  
               // echo "server error";   
          }

     }
     // elseif()
     elseif($prooftype == "Passport")
     {
          $removeIcFront = "no";
          $removeIcBack = "no";

          if(isset($_POST['editSubmit']))
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               // //echo "save to database";
               if($country)
               {
                    array_push($tableName,"country");
                    array_push($tableValue,$country);
                    $stringType .=  "s";
               }
               if($firstname)
               {
                    array_push($tableName,"firstname");
                    array_push($tableValue,$firstname);
                    $stringType .=  "s";
               }
               if($lastname)
               {
                    array_push($tableName,"lastname");
                    array_push($tableValue,$lastname);
                    $stringType .=  "s";
               }
               if($prooftype)
               {
                    array_push($tableName,"proof_id_type");
                    array_push($tableValue,$prooftype);
                    $stringType .=  "s";
               }
               if($passport)
               {
                    array_push($tableName,"passport");
                    array_push($tableValue,$passport);
                    $stringType .=  "s";
               }
               if($removeIcFront)
               {
                    array_push($tableName,"ic_front");
                    array_push($tableValue,$removeIcFront);
                    $stringType .=  "s";
               }
               if($removeIcBack)
               {
                    array_push($tableName,"ic_back");
                    array_push($tableValue,$removeIcBack);
                    $stringType .=  "s";
               }
               array_push($tableValue,$uid);
               $stringType .=  "s";
               $updateEkycDetails = updateDynamicData($conn,"ekyc"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateEkycDetails)
               {
                    // echo "<script>alert('Data Updated !');window.location='../member.php'</script>"; 
                    // echo "data updated !!";
                    // header('Location: ../thankYou.php');
                    header('Location: ../thankYouUpdate.php');
               }
               else
               {    
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kycVerification.php?type=2');
                    // echo "<script>alert('unable to update data !');window.location='../userEditEkycDetails.php'</script>";   
                    // echo "fail to update data !!";   
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../kycVerification.php?type=3');
               // echo "<script>alert('error');window.location='../userEditEkycDetails.php'</script>";  
               // echo "server error";   
          }

     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../kycVerification.php?type=6');
          // echo "<script>alert('no selection of proof type');window.location='../userEditEkycDetails.php'</script>";  
          // echo "no selection of proof type";   
     }
  
}
else 
{
     header('Location: ../index.php');
}
?>