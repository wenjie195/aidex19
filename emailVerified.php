<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = null;
$userRows = null;

$conn = connDB();

if(isset($_GET['getVerified']))
{
    $uid = rewrite($_GET['getVerified']);
    echo $uid;
    if(updateDynamicData($conn,"user"," WHERE uid = ? ",array("email_verified"),array(2,$uid),"is"))
    {
        // header( "refresh:3;url=index.php" );
        header( "refresh:2;url=index.php" );
    }
    else 
    {
        header('Location:index.php');
    }

    // echo $uid;
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/emailVerified.php" />
<meta property="og:title" content=">Email Verified | Aidex" />
<title>Email Verified | Aidex</title>

<link rel="canonical" href="https://aidex.sg/emailVerified.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
 	<div class="width100 overflow same-padding menu-distance min-height-with-menu-distance text-center">
		<h1 class="aidex-h1">Congratulations!</h1>
        <p class="thankyou-p">Your email has been successfully verified!</b></p>
		<img src="img/email-verified.png" alt="Verification" title="Verification" class="verification-img2 text-center">
   </div>
   
   
   

<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
</style>

<?php include 'js.php'; ?>


</body>
</html>