<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/Ekyc.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $userEkycRows = getUserEkyc($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $userEkycDetails = $userEkycRows[0];

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/kycVerification.php" />
<meta property="og:title" content="eKYC Verification | Aidex" />
<title>eKYC Verification | Aidex</title>

<link rel="canonical" href="https://aidex.sg/kycVerification.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
 	<div class="width100 overflow same-padding menu-distance min-height-with-menu-distance mobile-change-one-column">

        <div class="two-right-content-div two-left float-left">
        <?php 
        $ekycProof = $userDetails->getEkycUpdated();

        if($ekycProof != "")
        {
        ?>

            <!-- <div class="two-right-content-div two-left float-left">
                <h4 class="tab-h2">Edit EKYC Data</h4>
            </div>  -->

                <h2 class="tab-h2">
                    <a href="userEditEkycDetails.php" class="blue-link hover1">Edit eKYC Data
                     <img src="img/edit1.png" class="hover1a edit-png" alt="Edit eKYC Data" title="Edit eKYC Data">
                     <img src="img/edit2.png" class="hover1b edit-png" alt="Edit eKYC Data" title="Edit eKYC Datas">
                    </a>    
                </h2>
          
            
        <?php
        }
        else
        {
        ?>        
        
			<form action="utilities/registerEkycFunction.php" method="POST" enctype="multipart/form-data">
                
                    <h2 class="tab-h2">eKYC Verification</h2>

                    <div class="input-div">
                        <p class="input-top-text">Country</p>
                        <select class="aidex-input clean" id="ekyc_country" name="ekyc_country" required>

                            <option>Please Select A Country</option>
                            <?php
                            for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                            {
                            ?>
                            <option value="<?php echo $countryList[$cntPro]->getEnName(); 
                            ?>"> 
                            <?php 
                            echo $countryList[$cntPro]->getEnName(); //take in display the options
                            ?>
                            </option>
                            <?php
                            }
                            ?>

                        </select>
                    </div>  

                    <div class="input-div">
                        <p class="input-top-text">Last Name</p>
                        <input class="aidex-input clean" type="text" placeholder="Last Name" id="ekyc_lastname" name="ekyc_lastname" required>
                    </div>  

                    <div class="input-div">
                        <p class="input-top-text">First Name</p>
                        <input class="aidex-input clean" type="text" placeholder="First Name" id="ekyc_firstname" name="ekyc_firstname" required>
                    </div>  

                    <div class="input-div">
                        <p class="input-top-text">Proof of ID</p>
                        <select class="aidex-input clean" onchange="random_function()" id="ekyc_prooftype" name="ekyc_prooftype" required>
                            <option>Please select one type of proof ID</option>
                            <option value="IC">IC</option>
                            <option value="Passport">Passport</option>
                        </select>
                    </div>  

                    <div id="IC" style="display: none">
                        <div class="input-div">
                            <p class="input-top-text">IC Front</p>
                            <input id="file-upload" type="file" name="file_icfront" id="file_icfront" accept="image/*" />      
                        </div>  
                        <div class="input-div">
                            <p class="input-top-text">IC Back</p>
                            <input id="file-upload" type="file" name="file_icback" id="file_icback" accept="image/*" />                    
                        </div> 
                    </div>

                    <div id="Passport" style="display: none">
                        <div class="input-div">
                            <p class="input-top-text">Passport</p>
                            <input id="file-upload" type="file" name="file_passport" id="file_passport" accept="image/*" />                    
                        </div>  
                    </div>
        
                    <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="submit">Submit</button>

                </form>
     	<?php
        }
        ?>

        </div>    
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/eKYC.png" class="width100" alt="eKYC" title="eKYC">
        </div>

    </div>


<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "eKYC updated successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update eKYC details !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Opps, somethings goes wrong !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Unable to update eKYC details !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "no image uploaded !";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Please upload your proof ID !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
    function random_function()
    {
        var input = document.getElementById("ekyc_prooftype");

        if(ekyc_prooftype="IC")
        {
            var arr = document.getElementById("IC");
            arr.style.display = input.value == "IC" ? "block" : "none";
        }

        else(ekyc_prooftype="Passport")
        {
            var arr = document.getElementById("Passport");
            arr.style.display = input.value == "Passport" ? "block" : "none";
        }
    }
</script>

</body>
</html>                 