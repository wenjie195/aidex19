<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

?>

<div class="width100 same-padding overflow blue-table-div">
        <?php
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://financialmodelingprep.com/api/v3/quotes/crypto",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "x-rapidapi-host: currency-exchange.p.rapidapi.com",
                    "x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $exchangeRates = json_decode($response, true);
            }
        ?>

		<div class="scroll-div">
            <table class="currency-table">
                    <thead>
                        <tr>
                            <th class="first-width">Pairs</th>
                            <!-- <th>Current</th> -->
                            <th>Current</th>
                            <th>Change Rate (%)</th>
                            <th>High</th>
                            <th>Low</th>
                            <th>24H Volume</th>
                            <th>Graph</th>
                            <th style="text-align:left;">Trade</th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if ($exchangeRates)
                        {

                            $currencyArray = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD");

                            $imageArray = ('<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
                                            <img src="img/us.png" alt="US" title="US" class="country-flag">,
                                            <img src="img/british.png" alt="British" title="British" class="country-flag">,
                                            <img src="img/australia.png" alt="Australia" title="Australia" class="country-flag">,
                                            <img src="img/us.png" alt="US" title="US" class="country-flag">,
                                            <img src="img/british.png" alt="British" title="British" class="country-flag">,
                                            <img src="img/us.png" alt="US" title="US" class="country-flag">,
                                            <img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,');

                            $imageArrayExplode = explode(",",$imageArray);

                            $image2Array = ('<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
                                            <img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
                                            <img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
                                            <img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
                                            <img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
                                            <img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
                                            <img src="img/swiss-franc.png" alt="Switzerland" title="Switzerland" class="country-flag country-flag2">,
                                            <img src="img/australia.png" alt="Australia" title="Australia" class="country-flag country-flag2">,');

                            $image2ArrayExplode = explode(",",$image2Array);
                            $currencyArrayWithSlash = ("BTC/USD,ETH/USD,ETC/USD,LTC/USD,BTG/USD,XRP/USD,XLM/USD,BCH/USD,XMR/USD,TRX/USD");
                            $currencyArrayWithoutSlash = ("BTCUSD,ETHUSD,ETCUSD,LTCUSD,BTGUSD,XRPUSD,XLMUSD,BCHUSD,XMRUSD,TRXUSD");

                            $currencyArrayExplode = explode(",",$currencyArray);
                            $currencyArrayWithoutSlashExplode = explode(",",$currencyArrayWithoutSlash);
                            $currencyArrayWithSlashExplode = explode(",",$currencyArrayWithSlash);
                            ?><input type="hidden" id="total" value="<?php echo count($currencyArrayExplode); ?>"> <?php
                            for ($cntAA=0; $cntAA <count($currencyArrayWithSlashExplode) ; $cntAA++)
                            {
                                for ($cnt=0; $cnt <count($exchangeRates) ; $cnt++)
                                {
                                    $selectCountry = str_replace("C:","",$exchangeRates[$cnt]['symbol']);
                                    if ($selectCountry == $currencyArrayWithoutSlashExplode[$cntAA])
                                    {
                                        ?>
                                        <tr class="open-buy">
                                            <td  class="first-width"><?php //echo $imageArrayExplode[$cntAA]; ?> <?php  echo $currencyArrayWithSlashExplode[$cntAA] ; ?><?php //echo $image2ArrayExplode[$cntAA]; ?></td>

                                            <!-- <td ></td> -->

                                            <?php if ($exchangeRates[$cnt]['price'] < 0)
                                            {
                                            ?>
                                                <td class="red-text"><?php   echo number_format($exchangeRates[$cnt]['price'], 2)."<br>"; ?>
                                            <?php
                                            }
                                            elseif ($exchangeRates[$cnt]['price'] >= 0)
                                            {
                                            ?>
                                                <td class="green-text"><?php   echo number_format($exchangeRates[$cnt]['price'], 2)."<br>"; ?>
                                            <?php
                                            }
                                            ?>

                                              <td>
                                                      <?php echo number_format($exchangeRates[$cnt]['changesPercentage'], 2); ?>
                                              </td>
                                              <td>
                                                      <?php   echo number_format($exchangeRates[$cnt]['dayHigh'], 2); ?>
                                              </td>
                                              <td>
                                                      <?php   echo number_format($exchangeRates[$cnt]['dayLow'], 2); ?>
                                              </td>
                                              <td>
                                                      <?php   echo number_format($exchangeRates[$cnt]['volume'], 2); ?>
                                              </td>
                                              <!-- <td>
                                                graph
                                              </td> -->
                                              <td>
                                                <a class="white-text opacity-hover" href="https://www.tradingview.com/chart/?symbol=COINBASE:<?php  echo $currencyArrayWithoutSlashExplode[$cntAA] ; ?>" target="_blank">View Graph</a>
                                              </td>

                                            <?php
                                            if(isset($_SESSION['uid']))
                                            {
                                            ?>

                                            <td>
                                                <div class="width100 text-center">
                                                    <a class="clean green-btn fix-100" href="waitingApproval.php" style="display:block; text-align:center;">Trade</a>
                                                </div>
                                            </td>

                                            <?php
                                            }
                                            else
                                            {
                                            ?>

                                            <td>
                                                <div class="width100 text-center">
                                                    <a class="clean green-btn fix-100" href="signIn.php" style="display:block; text-align:center;">Trade</a>
                                                </div>
                                            </td>

                                            <?php
                                            }
                                            ?>

                                              <!-- <td>
                                                  <div class="width100 text-center">
                                                    <a class="clean green-btn fix-100" href="signIn.php" style="display:block; text-align:center;">Trade</a>
                                                  </div>
                                              </td> -->



                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
                        ?>
                    </tbody>
            </table>
        </div>
    </div>
