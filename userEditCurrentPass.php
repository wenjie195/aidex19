<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/userEditCurrentPass.php" />
<meta property="og:title" content="Edit Current Password | Aidex" />
<title>Edit Current Password | Aidex</title>

<link rel="canonical" href="https://aidex.sg/userEditCurrentPass.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
 	<div class="width100 overflow same-padding menu-distance min-height-with-menu-distance mobile-change-one-column">


        <div class="two-right-content-div two-left float-left">
            <form action="utilities/userEditPasswdFunction.php" method="POST">
                <h2 class="tab-h2">Edit Current Password</h2>

                <div class="input-div">
                    <p class="input-top-text">New Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="New Password" id="new_password" name="new_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>  

                <div class="input-div">
                    <p class="input-top-text">Retype New Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                </div>  

                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="submit">Submit</button>

            </form>
        </div>    
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/edit-password.png" class="width100" alt="Edit Password" title="Edit Password">
        </div>

    </div>


<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password updated !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update your password !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Password must be same with reenter password !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Password length must be more than 5";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunctionA()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>