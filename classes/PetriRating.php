<?php
class PetriRating {
    /* Member variables */
    // var $id, $companyName, $estimatedValue, $image, $background, $dateCreated, $dateUpdated;
    var $id,$companyName,$estimatedValue,$image,$background,
            $founderEvaluation,$productEvaluation,$businessModel,$industry,$teamEvaluation,$marketReadiness,
                $revenuesPO,$petriGrowth,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getEstimatedValue()
    {
        return $this->estimatedValue;
    }

    /**
     * @param mixed $estimatedValue
     */
    public function setEstimatedValue($estimatedValue)
    {
        $this->estimatedValue = $estimatedValue;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param mixed $background
     */
    public function setBackground($background)
    {
        $this->background = $background;
    }
            
    /**
     * @return mixed
     */
    public function getFounderEvaluation()
    {
        return $this->founderEvaluation;
    }

    /**
     * @param mixed $founderEvaluation
     */
    public function setFounderEvaluation($founderEvaluation)
    {
        $this->founderEvaluation = $founderEvaluation;
    }

    /**
     * @return mixed
     */
    public function getProductEvaluation()
    {
        return $this->productEvaluation;
    }

    /**
     * @param mixed $productEvaluation
     */
    public function setProductEvaluation($productEvaluation)
    {
        $this->productEvaluation = $productEvaluation;
    }

    /**
     * @return mixed
     */
    public function getBusinessModel()
    {
        return $this->businessModel;
    }

    /**
     * @param mixed $businessModel
     */
    public function setBusinessModel($businessModel)
    {
        $this->businessModel = $businessModel;
    }

    /**
     * @return mixed
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param mixed $industry
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    }

    /**
     * @return mixed
     */
    public function getTeamEvaluation()
    {
        return $this->teamEvaluation;
    }

    /**
     * @param mixed $teamEvaluation
     */
    public function setTeamEvaluation($teamEvaluation)
    {
        $this->teamEvaluation = $teamEvaluation;
    }

    /**
     * @return mixed
     */
    public function getMarketReadiness()
    {
        return $this->marketReadiness;
    }

    /**
     * @param mixed $marketReadiness
     */
    public function setMarketReadiness($marketReadiness)
    {
        $this->marketReadiness = $marketReadiness;
    }

    /**
     * @return mixed
     */
    public function getRevenuePO()
    {
        return $this->revenuesPO;
    }

    /**
     * @param mixed $revenuesPO
     */
    public function setRevenuePO($revenuesPO)
    {
        $this->revenuesPO = $revenuesPO;
    }

    /**
     * @return mixed
     */
    public function getPetriGrowth()
    {
        return $this->petriGrowth;
    }

    /**
     * @param mixed $petriGrowth
     */
    public function setPetriGrowth($petriGrowth)
    {
        $this->petriGrowth = $petriGrowth;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getPetriRating($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","company_name","estimated_value","image","background",
                            "founder_evaluation","product_evaluation","business_model","industry","team_evaluation",
                                "market_readiness","revenues_po","petri_growth","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"petrirating");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */

        // $stmt->bind_result($id,$companyName,$estimatedValue,$image,$background,$founderEvaluation,$productEvaluation,$dateCreated,$dateUpdated);
        $stmt->bind_result($id,$companyName,$estimatedValue,$image,$background,$founderEvaluation,$productEvaluation,$businessModel,$industry,$teamEvaluation,
                            $marketReadiness,$revenuesPO,$petriGrowth,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new PetriRating;
            $class->setId($id);
            $class->setCompanyName($companyName);
            $class->setEstimatedValue($estimatedValue);
            $class->setImage($image);
            $class->setBackground($background);

            $class->setFounderEvaluation($founderEvaluation);
            $class->setProductEvaluation($productEvaluation);

            $class->setBusinessModel($businessModel);
            $class->setIndustry($industry);
            $class->setTeamEvaluation($teamEvaluation);
            $class->setMarketReadiness($marketReadiness);
            $class->setRevenuePO($revenuesPO);
            $class->setPetriGrowth($petriGrowth);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
