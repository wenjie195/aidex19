<?php
class UserEkyc {
    /* Member variables */
    var $id,$uid,$username,$firstname,$lastname,$email,$phoneNo,$fullName,$addressOne,$addressTwo,$postcode,$state,$country,$proofIDType,$icFront,$icBack,$passport,
            $nationality,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getAddressOne()
    {
        return $this->addressOne;
    }

    /**
     * @param mixed $addressOne
     */
    public function setAddressOne($addressOne)
    {
        $this->addressOne = $addressOne;
    }

    /**
     * @return mixed
     */
    public function getAddressTwo()
    {
        return $this->addressTwo;
    }

    /**
     * @param mixed $addressTwo
     */
    public function setAddressTwo($addressTwo)
    {
        $this->addressTwo = $addressTwo;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getProofIdType()
    {
        return $this->proofIDType;
    }

    /**
     * @param mixed $proofIDType
     */
    public function setProofIdType($proofIDType)
    {
        $this->proofIDType = $proofIDType;
    }

    /**
     * @return mixed
     */
    public function getIcFront()
    {
        return $this->icFront;
    }

    /**
     * @param mixed $icFront
     */
    public function setIcFront($icFront)
    {
        $this->icFront = $icFront;
    }

    /**
     * @return mixed
     */
    public function getIcBack()
    {
        return $this->icBack;
    }

    /**
     * @param mixed $icBack
     */
    public function setIcBack($icBack)
    {
        $this->icBack = $icBack;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $passport
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUserEkyc($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","firstname","lastname","email","phone_no","full_name","address_lineone","address_linetwo","postcode","state","country",
                                "proof_id_type","ic_front","ic_back","passport","nationality","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"ekyc");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $uid, $username, $email, $phoneNo, $fullName, $nationality, $dateCreated, $dateUpdated);
        $stmt->bind_result($id,$uid,$username,$firstname,$lastname,$email,$phoneNo,$fullName,$addressOne,$addressTwo,$postcode,$state,$country,$proofIDType,$icFront,$icBack,$passport,
                                $nationality,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new UserEkyc;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);

            $user->setFirstname($firstname);
            $user->setLastname($lastname);

            $user->setEmail($email);
            $user->setPhoneNo($phoneNo);
            $user->setFullName($fullName);

            $user->setAddressOne($addressOne);
            $user->setAddressTwo($addressTwo);
            $user->setPostcode($postcode);
            $user->setState($state);
            $user->setCountry($country);

            $user->setProofIdType($proofIDType);
            $user->setIcFront($icFront);
            $user->setIcBack($icBack);
            $user->setPassport($passport);

            $user->setNationality($nationality);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
