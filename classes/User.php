<?php
class User {
    /* Member variables */
    // var $uid,$username,$email,$password,$salt,$phoneNo,$icNo,$fullName,$loginType,$userType,$dateCreated,$dateUpdated;
    var $id, $uid, $username, $email, $emailVerified, $password, $salt, $passwordTwo, $saltTwo, $phoneNo ,$fullName, $ekycUpdate, $nationality, $favoriteProject, $petriRating,
            $loginType, $userType, $dateCreated, $dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmailVerified()
    {
        return $this->emailVerified;
    }

    /**
     * @param mixed $emailVerified
     */
    public function setEmailVerified($emailVerified)
    {
        $this->emailVerified = $emailVerified;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getPasswordTwo()
    {
        return $this->passwordTwo;
    }

    /**
     * @param mixed $passwordTwo
     */
    public function setPasswordTwo($passwordTwo)
    {
        $this->passwordTwo = $passwordTwo;
    }

    /**
     * @return mixed
     */
    public function getSaltTwo()
    {
        return $this->saltTwo;
    }

    /**
     * @param mixed $saltTwo
     */
    public function setSaltTwo($saltTwo)
    {
        $this->saltTwo = $saltTwo;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getEkycUpdated()
    {
        return $this->ekycUpdate;
    }

    /**
     * @param mixed $ekycUpdate
     */
    public function setEkycUpdated($ekycUpdate)
    {
        $this->ekycUpdate = $ekycUpdate;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getFavoriteProject()
    {
        return $this->favoriteProject;
    }

    /**
     * @param mixed $favoriteProject
     */
    public function setFavoriteProject($favoriteProject)
    {
        $this->favoriteProject = $favoriteProject;
    }

    /**
     * @return mixed
     */
    public function getPetriRating()
    {
        return $this->petriRating;
    }

    /**
     * @param mixed $petriRating
     */
    public function setPetriRating($petriRating)
    {
        $this->petriRating = $petriRating;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","email","email_verified","password","salt","password_two","salt_two","phone_no","full_name","ekyc_update","nationality",
                            "favorite_project","petri_rating", "login_type","user_type","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $uid, $username, $email, $emailVerified, $password, $salt, $passwordTwo, $saltTwo, $phoneNo, $fullName, $ekycUpdate, $nationality, 
                                $favoriteProject, $petriRating, $loginType, $userType, $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);

            $user->setEmailVerified($emailVerified);

            $user->setPassword($password);
            $user->setSalt($salt);

            $user->setPasswordTwo($passwordTwo);
            $user->setSaltTwo($saltTwo);

            $user->setPhoneNo($phoneNo);
            $user->setFullName($fullName);

            $user->setEkycUpdated($ekycUpdate);

            $user->setNationality($nationality);

            $user->setFavoriteProject($favoriteProject);
            $user->setPetriRating($petriRating);

            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
