<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/CompanySelection.php';
require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/PetriRating.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$companyDetails = getCompanySelection($conn);

$petriRate = getPetriRating($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'header.php'; ?> -->
<?php include 'header-after-login.php'; ?>
<div class="width100 overflow menu-distance same-padding min-height-with-menu-distance">

	<!-- <div class="width100 text-center">
	</div>
    <h1 class="title-h1 text-center">Current Projects<?php //echo $userDetails->getFavoriteProject();?></h1>
    <h1 class="title-h1 text-center" style="display:none;"><?php //echo $userDetails->getFavoriteProject();?></h1>
    <div class="tab">
    </div> -->

    <div class="width100 text-center">
    	<img src="img/public-project.png" class="line-icon" alt="Current Projects in Public" title="Current Projects in Public">
	</div>

    <h1 class="title-h1 text-center">Current Projects</h1> 
    <!-- <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Public</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Petri Rating</button>
    </div> -->

    <div class="tab-div">
      <div class="tab-div-css active-tab">Public</div>
      <a href="currentProjectPetriRating.php"><div class="tab-div-css">Petri Rating</div></a>
    </div>

    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>
                    </thead>
                </tr>
                <tr>
                    <td class="th0-color color-td company-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th7-color color-td"></td>
                    <td class="th8-color color-td"></td>
                </tr>
                <tr>
                    <td class="th0 company-td">Companies</td>
                    <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                    <td class="th2" title="Details about the products">Product Evaluation</td>
                    <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                    <td class="th4" title="Internal & External analysis">Industry</td>
                    <td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                    <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                    <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                    <td class="th8" title="Overall growth of company">Petri Growth</td>
                </tr>
                <tr>
                    <?php
                    if($companyDetails)
                    {
                        for($cnt = 0;$cnt < count($companyDetails) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <!-- <td></td> -->                     
                            <td class="company-td">        
                                <div class="left-logo-div1">
                                    <img src="public_img/<?php echo $companyDetails[$cnt]->getImage();?>" class="company-logo" alt="<?php echo $companyDetails[$cnt]->getCompanyName();?>" title="<?php echo $companyDetails[$cnt]->getCompanyName();?>">
                                </div>

                                <div class="right-name-div">
                                    <?php $esValue = $companyDetails[$cnt]->getEstimatedValue();?>
                                    <b class="font-weight900"
                                    title="<?php echo $companyDetails[$cnt]->getBackground();?>"><?php echo $companyDetails[$cnt]->getCompanyName();?></b><p class="ev-p">EV: RM<?php echo $eV = number_format($esValue, 0); ?></p>
                                </div>

                                <form method="POST" action="utilities/addFavoriteFunction.php" enctype="multipart/form-data">
                                    <div class="right-star-div">
                                        <!-- <input type="text" value="<?php //echo $userDetails->getFavoriteProject();?>" name="favorite"> -->
                                        <input type="hidden" value="<?php echo $userDetails->getFavoriteProject();?>" name="favorite_old">
                                        <?php $selected = $userDetails->getFavoriteProject();
                                        // if ($selected) {
                                        $selectedExp = explode(",",$selected);
                                        for ($i=0; $i < count($selectedExp) ; $i++)
                                        {
                                            $selectedArray[] = $selectedExp[$i];
                                        }
                                        if (in_array($companyDetails[$cnt]->getId(),$selectedArray )) 
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="favorite_selected" type="checkbox" value="<?php echo $companyDetails[$cnt]->getId();?>"><img src="img/favourite2.png" class="favourite-png" title="Favourite" alt="Favourite"></button><?php
                                        }
                                        else
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="favorite" type="checkbox" value="<?php echo $companyDetails[$cnt]->getId();?>"><img src="img/favourite.png" class="favourite-png" title="Favourite" alt="Favourite"></button>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </form>
                            </td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getFounderEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getProductEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getBusinessModel();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getIndustry();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getTeamEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getMarketReadiness();?></td>
                            <td class="text-center"><?php echo $companyDetails[$cnt]->getRevenuePO();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getPetriGrowth();?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </tr>
            </table>
        </div>
    </div>

    <div class="clear"></div>
    
</div>

<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background:#e8eef7;
}

/* Handle */
::-webkit-scrollbar-thumb {
  background: #bed6ff;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #bed6ff;
}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully add the project as favorite!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to add the project as favorite !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "At least keep ONE favorite project";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>