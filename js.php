<div class="clear"></div>
<div class="width100 same-padding footer-div blue-bg"><p class="white-text footer-p text-center">@<?php echo $time;?> AIDEX, All Rights Reserved.</p></div>

<!-- Sign Up Modal -->
<div id="signup-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css signup-modal-content">
    <span class="close-css close-signup">&times;</span>

    <div class="second-four-div width100 same-padding form-div">
    	<img src="img/sign-up-icon.png" class="line-icon" alt="Sign Up" title="Sign Up" >
        <p class="bold-subtitle-p">
        	Sign Up Now
        </p>
        <div class="two-left-visual-div two-left float-left margin-control">
        	<img src="img/sign-up.png" class="width100" alt="Sign Up" title="Sign Up">
        </div>
        <div class="two-right-content-div two-right float-right text-left margin-control">
            <form action="utilities/newUserRegisterFunction.php" method="POST">
            	<!-- <div class="input-div">
                    <p class="input-top-text">Name</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Name Here" id="register_fullname" name="register_fullname" required>
                </div> -->
             	<div class="input-div">
                    <p class="input-top-text">Username</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Username" id="register_username" name="register_username" required>
                </div>
             	<div class="input-div">
                    <p class="input-top-text">Email</p>
                    <input class="aidex-input clean" type="email" placeholder="Type Your Email" id="register_email_user" name="register_email_user" required>
                </div>
             	<div class="input-div">
                    <p class="input-top-text">Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Password" id="register_password" name="register_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>
             	<div class="input-div">
                    <p class="input-top-text">Retype Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                </div>
             	<div class="input-div">
                    <!-- <p class="input-top-text" id="register_nationality" name="register_nationality" required>Country</p> -->
                    <p class="input-top-text">Country</p>
                    <select class="aidex-input clean" id="register_nationality" name="register_nationality" required>
                        <option>Please Select A Country</option>
                        <?php
                        for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                        {
                        ?>
                        <option value="<?php echo $countryList[$cntPro]->getEnName(); 
                        ?>"> 
                        <?php 
                        echo $countryList[$cntPro]->getEnName(); //take in display the options
                        ?>
                        </option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <!-- <div class="input-div">
                    <p class="input-top-text">Country</p>
                    <input class="aidex-input clean" type="text" placeholder="Country" id="register_nationality" name="register_nationality" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover" alt="View Password" title="View Password">
                </div> -->
                <div class="input-div">
                	<p class="input-top-text">Contact Number</p>
                    <input class="aidex-input clean" type="text" placeholder="Contact Number" id="register_phone" name="register_phone" required>
                </div>

                <div class="clear"></div>

                <input type="checkbox" class="aidex-checkbox" required><label class="aidex-label">By ticking I agree to the <a class="blue-link open-terms">Terms of Services</a></label>

                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="refereeButton">Create Account</button>
                <div class="width100 text-center some-margin-top">
                	<a class="open-login blue-link forget-link">Already have an account? Login now!</a>
                </div>
            </form>
        </div>

       
        </div>
	</div>
</div>

<!-- Sign Up Modal -->
<div id="login-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css signup-modal-content">
    <span class="close-css close-login">&times;</span>

    <div class="second-four-div width100 same-padding form-div">
    	
    	<img src="img/sign-in-icon.png" class="line-icon" alt="Sign In" title="Sign In">
        <p class="bold-subtitle-p" >
        	Login
        </p>
        <div class="two-left-visual-div two-right float-right margin-control">
        	<img src="img/sign-in.png" class="width100" alt="Sign In" title="Sign In">
        </div>
        <div class="two-right-content-div two-left float-left margin-control text-left">
        <form action="utilities/loginFunction.php" method="POST">
             	<div class="input-div">
                    <p class="input-top-text">Username</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Username" id="username" name="username" required>
                </div>
             	<div class="input-div">
                    <p class="input-top-text">Password</p>
                    <input class="aidex-input clean password-input" type="password" placeholder="Type Your Password" id="password" name="password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
                </div>

                <div class="clear"></div>

                <!-- <input type="checkbox" class="aidex-checkbox"><label class="aidex-label">Remember Me</label> -->

                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top dual-a-btm" name="loginButton">Sign In</button>
				<div class="width100 text-center some-margin-top">
                	<a  class="open-signup blue-link forget-link">Not yet have an account? Sign up here!</a>
                  <br><br>
                  <a  class="open-forgot blue-link some-margin-top forget-link">Forgot Password</a>
                  <!-- <br><br>
                  <a class="open-forgotusername blue-link some-margin-top forget-link">Forgot Username</a> -->
                </div>
       	</form>
        </div>
	</div>
  </div>
</div>

<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgot">&times;</span>
    <h2 class="blue-h2">Forgot Password</h2>
    <!-- <form class="login-form" method="POST" > -->
    <form class="login-form" method="POST" action="utilities/forgotPasswordFunction.php">
      <div class="input-popout-div">
        <input class="aidex-input clean pop-out-input" type="email" placeholder="Type Your Email Here" name="forgot_password" required>
      </div>
      <div class="clear"></div>
      <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="submit">Submit</button>
    </form>
  </div>

</div>

<!-- Forgot Username Modal -->
<div id="forgotusername-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgotusername">&times;</span>
    <h2 class="blue-h2">Forgot Username</h2>
    <form class="login-form" method="POST" >
    	<div class="input-popout-div">
        	<input class="aidex-input clean pop-out-input" type="email" placeholder="Type Your Email Here"  name="forgotPassword_email">
        </div>
        <div class="clear"></div>
        <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top">Submit</button>
        
    </form>
  </div>

</div>
 <!-- Contact Admin Modal -->
<div id="contactadmin-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-contactadmin">&times;</span>
    <div class="clear"></div>
    <h2 class="blue-h2">Contact Admin</h2>
	<p class="contact-admin-p text-center">support@aidex.sg</p>
  </div>

</div>


<!-- Terms Modal -->
<div id="terms-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css signup-modal-content terms-modal-content">
    <span class="close-css close-terms">&times;</span>
	<div class="width100 text-center">
        <p class="bold-subtitle-p" >
        	Aidex User Agreement
        </p>
        <p class="small-date-p">Last updated: 24th of March</p>
		<p class="risk-p">
        <b>Risk Warning and Compliance Disclosures</b><br><br>

 

This Risk Warning and Compliance Disclosures should be read in conjunction with the Aidex Terms of Service, we would like you to understand the risks and compliance matters while you trade on our platform.<br><br>

Risk is always associates with investment, therefore you shall be cautious in your operations. There are potential losses in investment in financial products. The historical data of any digital currency or financial product cannot guarantee its future performance and returns, especially in the market downturn when there is no means to ensure the users to gain benefits or stop losses. The trading of digital currency or any product on the market is speculative and highly risky. The risk of loss in trading or holding digital assets can be substantial. You should therefore carefully consider whether trading in digital assets or any levered or derivative digital assets is suitable for you in light of your financial condition.<br><br>

In addition, there are specific transaction risks outside the regular trading hours, including the risk of low liquidity, risk of high volatility, risk of price change, unconnected market, news bulletin influencing price and greater price gap.<br><br>

As we know that there are many uncertainties in the digital currency, the risks are huge, and if you do not meet the following requirements, please do not get involved!<br><br>

The ability to sustain >50% asset loss or even goes up to 100% loss in asset value.<br>

2. To have at least 3 years of investment/trading experience in securities, funds, futures, gold, foreign exchange, etc.<br>

3. To comply with anti-money laundering laws and regulations of various countries and can accept digital asset review when necessary.<br>

4. To be able to bear the risks of ceased operation of trading platform caused by policy risks.<br>

5. To be able to bear the risks of asset loss due to the technical vulnerability of digital currency.<br>

6. To have turned 18 years old in legal age.<br><br>

 

The user shall thoroughly understand and know the applicable transaction risks and make an accurate evaluation of his / her own economic capacity, capital source and risk tolerance from the general reasonable person's perspective before opening an account, and the user shall carefully evaluate the investment priorities, investment amount and financial products.<br><br>

Based on the characteristics of Internet trading users and products, Aidex does not completely guarantee the accuracy, appropriateness, timeliness, reliability, validity and effectiveness of the securities transaction information material in any express or implied manner and all details, data and files are for reference only. The user acknowledges and agrees that risks cannot be avoided by making transactions through Aidex. The risks resulting from the transaction shall be borne by the user, and Aidex shall not bear any liability for the user's losses due to investment risk.<br><br>

 

Aidex cannot and does not want clients in jurisdictions that have banned activities involving digital assets, or on international sanction lists. We just don’t want to go against with various countries’ regulations. We may be asked for information from law enforcement officials as good corporate citizens, and will assist if law permits. What this means is that our network only accepts customer-compliant legislation. We welcome the opportunity to earn your business; in return we expect you to act on our platform in a lawful and proper way.<br><br><br>

 

<b>Privacy Policy</b><br><br>

Once you visit Aidex and open an account with us, you will become Aidex's customer. We will collect your information available in Aidex to provide them with our products and services. We recognize that customers are extremely vigilant about the use and confidentiality of their personal data. Furthermore, we will collect your personal details and financial information when you sign up an account with Aidex. The personal details collected will be use to manage your account, provide customer support and improve your customer experience. We will use the aggregated personal and non-personal information to enhance Aidex and track and safeguard our services.<br><br> 

As specified in the private policy, we will not sell, rent or trade your personal details to any third party, unless you authorize or the law requires. If you have any queries, or you believe that Aidex does not comply with this private policy, please send an email to support@aidex.com.<br><br><br>

 

<b>Disclaimer</b><br><br> 

All products, services and contents on the website are provided on an “as is” basis without any express or implied warranties, including but not limited to implied warranties of merchantability, implied warranty of applicability for a particular purpose, ownership, non-infringement, and security, or accuracy. Aidexis not responsible for the accuracy or reliability of any information or content, nor does it make any statement or guarantee of the completeness or accuracy of the content provided by third party providers on the website. You have the responsibility to evaluate the accuracy, reliability, timeliness and completeness of any information provided on the website, and Aidex hereby declares that it does not undertake any obligation to update the information on the website.<br><br>  

You agree to indemnify, defend and protect Aidex from any liability, loss, claim and expenses, including the attorney fees associated with your breach of these terms of use or using the services and information provided on the website.
        
        </p>
	</div>
  </div>
</div>

<!-- Havan Modal -->
<div id="havan-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css company-modal-content">
    <span class="close-css close-havan">&times;</span><br>
    <h2 class="blue-h2">Havan Clothing</h2>
	<div class="clear"></div>
    <div class="table-scroll-div pop-out-table-div">
            <table class="company-name-table company-table2">
            	<tr>
                	<thead>
                    	<th class="th0-color" title="Different criteria has different score, some criteria will have higher priority and scores whereas some lower.">Score Weightage</th>
                        <th class="th1-color" title="The criteria use to measure and evaluate.">Rating Criteria</th>
                        <th class="th2-color">Rating</th>
                        <th class="th3-color">Reasons</th>
                    </thead>
                </tr>
                <tr>
                	<td class="th5-color">Founder</td>
                    <td class="th5-color"></td>
                    <td class="th5-color"></td>
                    <td class="th5-color"></td>
                </tr>
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Working Experienced</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    <b>Ivan Eng (4/5 X 10% = 8%)</b><br>
                    10 years working experience in Finance and Business<br><br>
                    
                    <b>Hany Cheng (2/5 X 10% = 4%)</b><br>
                    10 years working experience in education & counselling, but not related to their business
                    </td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Business Experienced</b></td>
                    <td><img src="img/star.png" class="star-png"></td>
                    <td>
                    This is the 1st business for both founders
                    </td>
                </tr>                 
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Experienced in the Industry</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    <b>Ivan Eng (3/5 X 30% = 18%)</b><br>
					A bit touch to business field with 10 years<br><br> 
                    
                    <b>Hany Cheng (1/5 X 30% = 6%)</b><br>
                    No touching to business field
                    </td>
                </tr>
                <tr>
                	<td><b>5%</b></td>
                    <td><b>Awards</b></td>
                    <td>N/A</td>
                    <td>
                    N/A
                    </td>
                </tr>                
                <tr>
                	<td><b>15%</b></td>
                    <td><b>Achievement</b></td>
                    <td>N/A</td>
                    <td>
                    N/A
                    </td>
                </tr>                
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Qualification</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    <b>*Ivan Eng (5/5 X 10% = 10%)</b><br>
                    (2012 - 2014) Master of Business Administration (MBA) of Finance, General in Multimedia University<br>
                    (2006 - 2009) Bachelor’s degree of International Business in Multimedia University<br><br>
                    
                    <b>*Hany Cheng (3/5 X 10% = 6%)</b><br>
                    Not stated, but to become a counsellor should have a diploma or degree certificate
                    </td>
                </tr> 
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                 
                <tr>
                	<td class="th6-color white-text">Product</td>
                    <td class="th6-color"></td>
                    <td class="th6-color"></td>
                    <td class="th6-color"></td>
                </tr>                
                <tr>
                	<td><b>15%</b></td>
                    <td><b>Unique Selling Point of Product</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    All designs are created by the Shelter-home children<br>

                    Simple & recognizing child’s creativity<br>
                    
                    However, it is too many competitors selling simple design T-shirt
                    </td>
                </tr>                 
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Choices & Varieties</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Only have T-shirts and socks
                    </td>
                </tr>
                <tr>
                	<td><b>15%</b></td>
                    <td><b>Presentation of Product</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    The website and product packaging is satisfied
                    </td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Price</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Price charged is moderate with nice packaging
                    </td>
                </tr>                
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Payment</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Using iPay88 and provided various payment methods
                    </td>
                </tr>                
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Problem-Solving</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Yes,as everyone wear clothes
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                 
                <tr>
                	<td class="th0-color white-text">Business Model</td>
                    <td class="th0-color"></td>
                    <td class="th0-color"></td>
                    <td class="th0-color"></td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Scalability</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Yes, fashion is scalable
                    </td>
                </tr>                 
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Switchable</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Customers can choose to switch to another brands
                    </td>
                </tr>
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Recurring Revenues</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Not really as they only sell Tee and socks right now
                    </td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Protection from Competition</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    There are a lot of competitiors in this industry
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                                
                <tr>
                	<td class="th1-color white-text">Industry</td>
                    <td class="th1-color"></td>
                    <td class="th1-color"></td>
                    <td class="th1-color"></td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Competitor Analysis</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    New industry competitor has been increase steadily
                    </td>
                </tr>                 
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Entry Barrier</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Clothing business is pretty easy to emulate and with modern service such as on demand clothing printing, anyone can start on this inudstry
                    </td>
                </tr>
                <tr>
                	<td><b>25%</b></td>
                    <td><b>Power of Suppliers</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    The amount of cloth manufacturing industry is a lot. Shirt can be design easily and resell
                    </td>
                </tr>                
                <tr>
                	<td><b>5%</b></td>
                    <td><b>Power of Buyers</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Buyers has a lot of alternative rather than T-shirt and tons of competitors. This make buyer power high
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                 
                <tr>
                	<td class="th2-color white-text">Team Evaluation</td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                </tr>                
                <tr>
                	<td><b>35%</b></td>
                    <td><b>Working Experience</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Their team are educated and full of working experience
                    </td>
                </tr>                 
                <tr>
                	<td><b>35%</b></td>
                    <td><b>Sales& Marketing Team</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Their revenue growth in a huge %, there can see their effort on sales & marketing side
                    </td>
                </tr>
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Team Qualification</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Overall are educated and with working experience
                    </td>
                </tr>                
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Team Achievement</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    N/A
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>
                
                <tr>
                	<td class="th2-color white-text">Market Readiness</td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                </tr>                
                <tr>
                	<td><b>45%</b></td>
                    <td><b>Ask Questions from Consumer Point of View</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    They have good feedbacks and reviews but not much buying intention on them, most of the people buying their product is just because of sympathy
                    </td>
                </tr>                 
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Google Adword Look Whether People Search for it or not </b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Bad Traffic<Br>
					Searching "Clothing" in google, it is not shown on the 1st page
                    </td>
                </tr>
                <tr>
                	<td><b>35%</b></td>
                    <td><b>Numbers of Competitor</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    They have a lot of competitor selling similar products in this market
                    </td>
                </tr>                                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                                                                                        
            </table>
  </div>
  <div class="width100 three-data-div">
  	<div class="three-div-width">
    	<p class="three-p">Website</p>
        <a href="https://www.havanclothing.com/" class="blue-link" target="_blank">havanclothing.com</a>
    </div>
  	<div class="three-div-width">
    	<p class="three-p">Listed Platform</p>
        <a href="https://www.mystartr.com/" class="blue-link" target="_blank">mystartr.com</a>
    </div>    
  	<div class="three-div-width">
    	<p class="three-p">Petri Growth</p>
        <img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png">
    </div>    
  </div>
</div>
</div>
<!-- Rtist Modal -->
<div id="rtist-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css company-modal-content">
    <span class="close-css close-rtist">&times;</span><br>
    <h2 class="blue-h2">Rtist</h2>
	<div class="clear"></div>
    <div class="table-scroll-div pop-out-table-div">
            <table class="company-name-table company-table2">
            	<tr>
                	<thead>
                    	<th class="th0-color" title="Different criteria has different score, some criteria will have higher priority and scores whereas some lower.">Score Weightage</th>
                        <th class="th1-color" title="The criteria use to measure and evaluate.">Rating Criteria</th>
                        <th class="th2-color">Rating</th>
                        <th class="th3-color">Reasons</th>
                    </thead>
                </tr>
                <tr>
                	<td class="th5-color">Founder</td>
                    <td class="th5-color"></td>
                    <td class="th5-color"></td>
                    <td class="th5-color"></td>
                </tr>
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Working Experienced</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Worked in industrial design for 4 years
                    </td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Business Experienced</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Managing Director at Zeroo Sdn Bhd, CEO & Founder of Rtist Creative Platform, selling insurance for a couple of years
                    </td>
                </tr>                 
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Experienced in the Industry</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Graduate from Malaysian Institute of Art, product designer for 4 years
                    </td>
                </tr>
                <tr>
                	<td><b>5%</b></td>
                    <td><b>Awards</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Cradle CIP300 Grand Recipient awarded RM300,000 (2018)<br>

                    Top 5 Start-ups of the Selangor Accelerator Programme<br>
                    
                    Top 10 Winners in the MyStartr Dream Factory Startup Contest (2017)
                    </td>
                </tr>                
                <tr>
                	<td><b>15%</b></td>
                    <td><b>Achievement</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    No achievement
                    </td>
                </tr>                
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Qualification</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Diploma in Malaysian Institute of Art
                    </td>
                </tr> 
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                 
                <tr>
                	<td class="th6-color white-text"><b>Product</b></td>
                    <td class="th6-color"></td>
                    <td class="th6-color"></td>
                    <td class="th6-color"></td>
                </tr>                
                <tr>
                	<td><b>15%</b></td>
                    <td><b>Unique Selling Point of Product</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Make employer life easier by recruiting online
                    </td>
                </tr>                 
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Choices & Varieties</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Provide freelancer like graphic design and business writing and also act as a platform for job posting
                    </td>
                </tr>
                <tr>
                	<td><b>15%</b></td>
                    <td><b>Presentation of Product</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Moderate display website display<br>
                    Clean & professional looking
                    </td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Price</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Price charged based on freelancer<br>It is based on market rate
                    </td>
                </tr>                
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Payment</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Payment looks secure and easy to use. Have few payment option to choose of
                    </td>
                </tr>                
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Problem-Solving</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    Solved all the employer/hiring problem in recruiting
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                 
                <tr>
                	<td class="th0-color white-text"><b>Business Model</b></td>
                    <td class="th0-color"></td>
                    <td class="th0-color"></td>
                    <td class="th0-color"></td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Scalability</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    They are planning to expand their business in the next 3 years
                    </td>
                </tr>                 
                <tr>
                	<td><b>10%</b></td>
                    <td><b>Switchable</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    There are still other platform to choose
                    </td>
                </tr>
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Recurring Revenues</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    There is without any add on in this platform/product
                    </td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Protection from Competition</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    There is still competitive in the market
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                                
                <tr>
                	<td class="th1-color white-text">Industry</td>
                    <td class="th1-color"></td>
                    <td class="th1-color"></td>
                    <td class="th1-color"></td>
                </tr>                
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Competitor Analysis</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *New industry competitor has been increase steadily
                    </td>
                </tr>                 
                <tr>
                	<td><b>30%</b></td>
                    <td><b>Entry Barrier</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *Clothing business is pretty easy to emulate and with modern service such as on demand clothing printing, anyone can start on this inudstry
                    </td>
                </tr>
                <tr>
                	<td><b>25%</b></td>
                    <td><b>Power of Suppliers</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *The amount of cloth manufacturing industry is a lot. Shirt can be design easily and resell.
                    </td>
                </tr>                
                <tr>
                	<td><b>5%</b></td>
                    <td><b>Power of Buyers</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *Buyers has a lot of alternative rather than T-shirt and tons of competitors. This make buyer power high.
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                 
                <tr>
                	<td class="th2-color white-text">Team Evaluation</td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                </tr>                
                <tr>
                	<td><b>35%</b></td>
                    <td><b>Working Experience</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *Their team are educated and full of working experience
                    </td>
                </tr>                 
                <tr>
                	<td><b>35%</b></td>
                    <td><b>Sales& Marketing Team</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *Their revenue growth in a huge %, there can see their effort on sales & marketing side.
                    </td>
                </tr>
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Team Qualification</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *Overall are educated and with working experience
                    </td>
                </tr>                
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Team Achievement</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    N/A
                    </td>
                </tr>                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>
                
                <tr>
                	<td class="th2-color white-text">Market Readiness</td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                    <td class="th2-color"></td>
                </tr>                
                <tr>
                	<td><b>45%</b></td>
                    <td><b>Ask Questions from Consumer Point of View</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *They have good feedbacks and reviews but not much buying intention on them, most of the people buying their product is just because of sympathy.
                    </td>
                </tr>                 
                <tr>
                	<td><b>20%</b></td>
                    <td><b>Google Adword Look Whether People Search for it or not </b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *Bad Traffic<Br>
					*Searching "Clothing" in google, it is not shown on the 1st page.
                    </td>
                </tr>
                <tr>
                	<td><b>35%</b></td>
                    <td><b>Numbers of Competitor</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    *They have a lot of competitor selling similar products in this market.
                    </td>
                </tr>                                
                <tr>
                	<td></td>
                    <td><b>Total Score</b></td>
                    <td><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td>
                    <td>
                    </td>
                </tr>                                                                                        
            </table>
  </div>
  <div class="width100 three-data-div">
  	<div class="three-div-width">
    	<p class="three-p">Website</p>
        <a href="https://www.havanclothing.com/" class="blue-link" target="_blank">havanclothing.com</a>
    </div>
  	<div class="three-div-width">
    	<p class="three-p">Listed Platform</p>
        <a href="https://www.mystartr.com/" class="blue-link" target="_blank">mystartr.com</a>
    </div>    
  	<div class="three-div-width">
    	<p class="three-p">Petri Growth</p>
        <img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png">
    </div>    
  </div>
</div>
</div>
<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
        <script>
	$(document).ready(function() {
	var s = $(".menu-white");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=170) {
			s.addClass("menu-bg");
		} else {
			s.removeClass("menu-bg");	
		}
		});
	});
	if (window.matchMedia('screen and (max-width: 1200px)').matches) {
	$(document).ready(function() {
	var s = $(".menu-white");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=100) {
			s.addClass("menu-bg");
		} else {
			s.removeClass("menu-bg");	
		}
		});
	});		
		}
	</script> 
    
<!-- Responsive Menu --->
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
        
        
<!--- Modal Box --->
<script>
var forgotmodal = document.getElementById("forgot-modal");
var forgotusernamemodal = document.getElementById("forgotusername-modal");
var contactadminmodal = document.getElementById("contactadmin-modal");
var signupmodal = document.getElementById("signup-modal");
var loginmodal = document.getElementById("login-modal");
var havanmodal = document.getElementById("havan-modal");
var termsmodal = document.getElementById("terms-modal");
var openforgot = document.getElementsByClassName("open-forgot")[0];
var openforgot1 = document.getElementsByClassName("open-forgot")[1];
var openforgotusername = document.getElementsByClassName("open-forgotusername")[0];
var openforgotusername1 = document.getElementsByClassName("open-forgotusername")[1];
var opencontactadmin = document.getElementsByClassName("open-contactadmin")[0];
var opencontactadmin1 = document.getElementsByClassName("open-contactadmin")[1];
var opencontactadmin2 = document.getElementsByClassName("open-contactadmin")[2];
var opencontactadmin3 = document.getElementsByClassName("open-contactadmin")[3];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var opensignup3 = document.getElementsByClassName("open-signup")[3];
var opensignup4 = document.getElementsByClassName("open-signup")[4];
var opensignup5 = document.getElementsByClassName("open-signup")[5];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openhavan = document.getElementsByClassName("open-havan")[0];
var openterms = document.getElementsByClassName("open-terms")[0];
var openterms1 = document.getElementsByClassName("open-terms")[1];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closeforgotusername = document.getElementsByClassName("close-forgotusername")[0];
var closecontactadmin = document.getElementsByClassName("close-contactadmin")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closehavan = document.getElementsByClassName("close-havan")[0];
var closeterms = document.getElementsByClassName("close-terms")[0];
if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openforgot1){
openforgot1.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openforgotusername){
openforgotusername.onclick = function() {
  forgotusernamemodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openforgotusername1){
openforgotusername1.onclick = function() {
  forgotusernamemodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opencontactadmin){
opencontactadmin.onclick = function() {
  contactadminmodal.style.display = "block";
}
}
if(opencontactadmin1){
opencontactadmin1.onclick = function() {
  contactadminmodal.style.display = "block";
}
}
if(opencontactadmin2){
opencontactadmin2.onclick = function() {
  contactadminmodal.style.display = "block";
}
}
if(opencontactadmin3){
opencontactadmin3.onclick = function() {
  contactadminmodal.style.display = "block";
}
}
if(opensignup){
opensignup.onclick = function() {
  signupmodal.style.display = "block";
}
}
if(opensignup1){
opensignup1.onclick = function() {
  signupmodal.style.display = "block";
}
}
if(opensignup2){
opensignup2.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup3){
opensignup3.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup4){
opensignup4.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup5){
opensignup5.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
}
}
if(openlogin4){
openlogin4.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
}
}
if(openhavan){
openhavan.onclick = function() {
  havanmodal.style.display = "block";
}
}
if(openterms){
openterms.onclick = function() {
  termsmodal.style.display = "block";
}
}
if(openterms1){
openterms1.onclick = function() {
  termsmodal.style.display = "block";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closeforgotusername){
closeforgotusername.onclick = function() {
  forgotusernamemodal.style.display = "none";
}
}
if(closecontactadmin){
closecontactadmin.onclick = function() {
  contactadminmodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closehavan){
closehavan.onclick = function() {
  havanmodal.style.display = "none";
}
}
if(closeterms){
closeterms.onclick = function() {
  termsmodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == forgotusernamemodal) {
    forgotusernamemodal.style.display = "none";
  } 
  if (event.target == contactadminmodal) {
    contactadminmodal.style.display = "none";
  }  
  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }   
  if (event.target == havanmodal) {
    havanmodal.style.display = "none";
  } 
  if (event.target == termsmodal) {
    termsmodal.style.display = "none";
  }       
}
</script>


<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script>
$(".checkbox1").click(function(){
    $(this).toggleClass('checked1')
});
</script>
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>