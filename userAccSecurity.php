<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/member.php" />
<meta property="og:title" content="Member | Aidex" />
<title>Member | Aidex</title>

<link rel="canonical" href="https://aidex.sg/member.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
 	<div class="width100 overflow same-padding">
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/member.png" class="width100" alt="Member" title="Member">
        </div>
        <div class="two-right-content-div two-left float-left">

            <h4 class="tab-h2">Account Security</h4>

            <div class="input-div">
                <!-- <p class="input-top-text">Login Password</p> -->
                <a href="userEditCurrentPass.php" class="red-link">Edit Login Password</a>
            </div>  

            <div class="input-div">
                <!-- <p class="input-top-text">Secondory Password</p> -->
                <a href="userAddSecondaryPass.php" class="red-link">Add Secondory Password</a>
            </div>  

        </div>    


    </div>
    <div class="clear"></div>
    <div class="spacing-div"></div>
    <h2 class="line-h2"><img src="img/cryptocurrency.png" class="line-icon line-icon-spacing" alt="Cryptocurrency" title="Cryptocurrency"></h2>
    <div class="clear"></div>    

    <h4 class="tab-h2"><a href="member.php" class="red-link">Account Information</a> | Account Security | <a href="kycVerification.php" class="red-link">KYC Verification</a> | <a href="invitationLink.php" class="red-link">Referral Link</a> </h4>

    <div class="spacing-div"></div>
<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>
<?php include 'js.php'; ?>
</body>
</html>