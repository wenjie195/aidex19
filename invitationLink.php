<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userReferrerRows = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uid),"s");
// $userReferrerDetails = $userReferrerRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/invitationLink.php" />
<meta property="og:title" content="Refer | Aidex" />
<title>Refer | Aidex</title>

<link rel="canonical" href="https://aidex.sg/invitationLink.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>

    <div class="width100 overflow same-padding min-height-with-menu-distance menu-distance">
    <h2 class="line-h2"><img src="img/link.png" class="line-icon line-icon-spacing" alt="Invitation Link" title="Invitation Link"></h2>
    <div class="clear"></div>  
            <h2 class="tab-h2 text-center">Invitation Link</h2>

            <div class="invite-link-div">
                <input type="hidden" id="linkCopy" value="https://aidex.sg/registerWithInvitation.php?referrerUID=<?php echo $_SESSION['uid']?>">
                <div class="invitation-link-container shadow-white-div">
                    <a id="invest-now-referral-link" href="#" class="invitation-link-a opacity-hover text-overflow">https://aidex.sg/registerWithInvitation.php?referrerUID=<?php echo $_SESSION['uid']?></a>
                </div>
                <button class="clean blue-btn-hover copy-btn" id="copy-referral-link">COPY</button>
            </div>

            <div class="clear"></div>
            
            <div class="shadow-white-div two-div">
                <img src="img/invited.png" class="two-div-img" alt="Friends Invited" title="Friends Invited">
                <p class="four-div-small-p">
                    Friends Invited
                </p>
                <p class="four-div-big-p2">
                <?php
				if($userReferrerRows)
				{   
					$initialReferralCount = count($userReferrerRows);
					$totalReferralCount = $initialReferralCount - 1;
				}
				else
				{   $totalReferralCount = 0;   }
				?>
                    <?php echo $totalReferralCount;?>
                </p>         
           </div> 
           <div class="shadow-white-div two-div right-two-div">
                <img src="img/trade.png" class="two-div-img" alt="Friends Traded" title="Friends Traded">
                <p class="four-div-small-p">
                    Friends Traded
                </p>
                <p class="four-div-big-p2">
                0
                </p>         
           </div>            


</div>  


<?php include 'js.php'; ?>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>

</body>
</html>