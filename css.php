<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="icon" href="img/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/main.css?version=1.0.1">
<link rel="stylesheet" type="text/css" href="css/component.css?version=1.0.3">
<link rel="stylesheet" type="text/css" href="css/style.css?version=1.3.8">