<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/CompanySelection.php';
require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/PetriRating.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$companyDetails = getCompanySelection($conn);

$petriRate = getPetriRating($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/publicProject.php" />
<meta property="og:title" content="Current Project in Public | Aidex" />
<title>Current Projects | Aidex</title>
<link rel="canonical" href="https://aidex.sg/publicProject.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'header.php'; ?> -->
<?php include 'header-after-login.php'; ?>
<div class="width100 overflow menu-distance same-padding min-height-with-menu-distance">

	<!-- <div class="width100 text-center">
	</div>
    <h1 class="title-h1 text-center">Current Projects<?php //echo $userDetails->getFavoriteProject();?></h1>
    <h1 class="title-h1 text-center" style="display:none;"><?php //echo $userDetails->getFavoriteProject();?></h1>
    <div class="tab">
    </div> -->

    <div class="width100 text-center">
    	<img src="img/public-project.png" class="line-icon" alt="Current Projects in Public" title="Current Projects in Public">
	</div>

    <h1 class="title-h1 text-center">Current Projects</h1> 
    <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Public</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Petri Rating</button>
    </div>

    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>
                    </thead>
                </tr>
                <tr>
                    <td class="th0-color color-td company-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th7-color color-td"></td>
                    <td class="th8-color color-td"></td>
                </tr>
                <tr>
                    <td class="th0 company-td">Companies</td>
                    <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                    <td class="th2" title="Details about the products">Product Evaluation</td>
                    <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                    <td class="th4" title="Internal & External analysis">Industry</td>
                    <td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                    <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                    <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                    <td class="th8" title="Overall growth of company">Petri Growth</td>
                </tr>
                <tr>
                    <?php
                    if($companyDetails)
                    {
                        for($cnt = 0;$cnt < count($companyDetails) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <!-- <td></td> -->
                            <td>
                                <?php $companyDetails[$cnt]->getCompanyName();?>
                                <div class="left-logo-div1">
                                    <img src="public_img/<?php echo $companyDetails[$cnt]->getImage();?>" class="company-logo" alt="<?php echo $companyDetails[$cnt]->getCompanyName();?>" title="<?php echo $companyDetails[$cnt]->getBackground();?>">
                                </div>                            
                                <div class="right-name-div">
                                    <b class="font-weight900" title="<?php echo $companyDetails[$cnt]->getBackground();?>"><?php echo $companyDetails[$cnt]->getCompanyName();?></b>
                                    <?php $esValue = $companyDetails[$cnt]->getEstimatedValue();?>
                                    <p class="ev-p"  title="<?php echo $companyDetails[$cnt]->getBackground();?>">EV: RM<?php echo $eV = number_format($esValue, 0); ?></p>
                                </div>                                    
                                <form method="POST" action="utilities/addFavoriteFunction.php" enctype="multipart/form-data">
                                    <div class="right-star-div">
                                        <!-- <input type="text" value="<?php //echo $userDetails->getFavoriteProject();?>" name="favorite"> -->
                                        <input type="hidden" value="<?php echo $userDetails->getFavoriteProject();?>" name="favorite_old">
                                        <?php $selected = $userDetails->getFavoriteProject();
                                        // if ($selected) {
                                        $selectedExp = explode(",",$selected);
                                        for ($i=0; $i < count($selectedExp) ; $i++)
                                        {
                                            $selectedArray[] = $selectedExp[$i];
                                        }
                                        if (in_array($companyDetails[$cnt]->getId(),$selectedArray )) 
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="favorite_selected" type="checkbox" value="<?php echo $companyDetails[$cnt]->getId();?>"><img src="img/favourite2.png" class="favourite-png" title="Favourite" alt="Favourite"></button><?php
                                        }
                                        else
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="favorite" type="checkbox" value="<?php echo $companyDetails[$cnt]->getId();?>"><img src="img/favourite.png" class="favourite-png" title="Favourite" alt="Favourite"></button>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </form>
                            </td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getFounderEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getProductEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getFounderEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getProductEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getFounderEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getProductEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getFounderEvaluation();?></td>
                            <td class="four-stars-color star-css"><?php echo $companyDetails[$cnt]->getProductEvaluation();?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </tr>
            </table>
        </div>
    </div>

    <div id="Unlisted" class="tabcontent">
          	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>                                                         
                    </thead>
                </tr>
               <tr>
                    <td class="th0-color color-td company-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th7-color color-td"></td>                         
                    <td class="th8-color color-td"></td>
                </tr>           
                <tr>
                    <td class="th0 company-td">Companies</td>
                    <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                    <td class="th2" title="Details about the products">Product Evaluation</td>
                    <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                    <td class="th4" title="Internal & External analysis">Industry</td>
                    <td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                    <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                    <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                    <td class="th8" title="Overall growth of company">Petri Growth</td>                
                </tr>
                
                <tr>
                    <?php
                    if($petriRate)
                    {   
                        for($cntAA = 0;$cntAA < count($petriRate) ;$cntAA++)
                        {
                        ?>
                        <tr>
                            <td class="company-td">
                                <div class="left-logo-div1">
                                    <img src="petrirate_img/<?php echo $petriRate[$cntAA]->getImage();?>" class="company-logo" alt="<?php echo $petriRate[$cntAA]->getCompanyName();?>" title="<?php echo $petriRate[$cntAA]->getBackground();?>">
                                </div>   
                                <div class="right-name-div">
                                    <b class="font-weight900" title="<?php echo $petriRate[$cntAA]->getBackground();?>"><?php echo $petriRate[$cntAA]->getCompanyName();?></b>
                                </div>
                                
                                <form method="POST" action="utilities/addPetriFavoriteFunction.php" enctype="multipart/form-data">
                                <!-- <form method="POST" action="#" enctype="multipart/form-data"> -->
                                    <div class="right-star-div">
                                        <input type="hidden" value="<?php echo $userDetails->getPetriRating();?>" name="petri_old">
                                        <?php $selectedPR = $userDetails->getPetriRating();
                                        // if ($selected) {
                                        $selectedPRExp = explode(",",$selectedPR);
                                        for ($i=0; $i < count($selectedPRExp) ; $i++)
                                        {
                                            $selectedPRArray[] = $selectedPRExp[$i];
                                        }
                                        if (in_array($petriRate[$cntAA]->getId(),$selectedPRArray )) 
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="petri_selected" type="checkbox" value="<?php echo $petriRate[$cntAA]->getId();?>"><img src="img/favourite2.png" class="favourite-png" title="Favourite" alt="Favourite"></button><?php
                                        }
                                        else
                                        {
                                        ?>
                                            <button class="transparent-button clean" name="petri" type="checkbox" value="<?php echo $petriRate[$cntAA]->getId();?>"><img src="img/favourite.png" class="favourite-png" title="Favourite" alt="Favourite"></button>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </form>

                            </td>
                            <td>Coming Soon</td>
                            <td>Coming Soon</td>
                            <td>Coming Soon</td>
                            <td>Coming Soon</td>
                            <td>Coming Soon</td>
                            <td>Coming Soon</td>
                            <td>Coming Soon</td>
                            <td>Coming Soon</td>
                        </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                </tr> 

                <!-- <tr>
                    <td class="company-td">
                        <div class="left-logo-div1">
                            <img src="img/mypetslibrary.png" class="company-logo" alt="Mypetlibrary" title="Mypetslibrary is Asia’s 1st established professional platform featuring pets that are able to connect professional sellers and buyers across nationwide.">
                        </div>
                        <div class="right-name-div">
                            <b class="font-weight900" title="Mypetslibrary is Asia’s 1st established professional platform featuring pets that are able to connect professional sellers and buyers across nationwide.">Mypetlibrary</b>
                        </div>
                    </td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                </tr>
                <tr>
                    	<td class="company-td">
                        <div class="left-logo-div1">
                        	<img src="img/samofa.png" class="company-logo" alt="Samofa" title="Samofa">
                        </div>
                        <div class="right-name-div">    
                            <b class="font-weight900">Samofa</b>
                        </div>
                        </td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>
                        <td>Coming Soon</td>
                        <td>Coming Soon</td>
						<td>Coming Soon</td>  
                </tr> -->
                </table>
        </div>        
    </div>

    <div class="clear"></div>
    
</div>

<style>
.header1{
	background: rgba(0,28,130,1);
	background: -moz-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,28,130,1)), color-stop(80%, rgba(0,28,130,1)), color-stop(100%, rgba(9,9,76,1)));
	background: -webkit-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -o-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: -ms-linear-gradient(left, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	background: linear-gradient(to right, rgba(0,28,130,1) 0%, rgba(0,28,130,1) 80%, rgba(9,9,76,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#001c82', endColorstr='#09094c', GradientType=1 );}
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background:#e8eef7;
}

/* Handle */
::-webkit-scrollbar-thumb {
  background: #bed6ff;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #bed6ff;
}
</style>

<?php include 'js.php'; ?>

<script>
function openList(evt, listName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(listName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

</body>
</html>
