<?php
if(isset($_SESSION['uid']))
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-bg" role="banner">
    <div class="big-container-size hidden-padding">
        <div class="left-logo-div float-left hidden-logo-padding">
        <a href="index.php">
            <img src="img/white-logo.png" class="logo-img web-logo" alt="AIDEX" title="AIDEX">
            <img src="img/aidex.png" class="logo-img mobile-logo" alt="AIDEX" title="AIDEX">
        </a>
        </div>

        <div class="float-right logged-in-menu">
            <a href="index.php#div_refresh" class="white-text opacity-hover menu-margin-right logged-a">Exchange</a>
            <!-- <a href="publicProject.php" class="white-text opacity-hover menu-margin-right logged-a">Watchlist</a>   -->
            <div class="dropdown logged-a">
                <a  class="white-text opacity-hover menu-margin-right">
                    Watchlist <img src="img/dropdown.png" class="dropdown-png">
                </a>
                <div class="dropdown-content yellow-dropdown-content">
                    <p class="dropdown-p"><a href="currentProjectPublic.php"  class="menu-padding dropdown-a">Public</a></p>
                    <p class="dropdown-p"><a href="currentProjectPetriRating.php"  class="menu-padding dropdown-a">Petri Rating</a></p>
                </div>
            </div>              
            <a href="member.php" class="white-text opacity-hover menu-margin-right logged-a">Info</a> 
            <div class="dropdown logged-a">
                <a  class="white-text opacity-hover menu-margin-right">
                    Security <img src="img/dropdown.png" class="dropdown-png">
                </a>
                <div class="dropdown-content yellow-dropdown-content">
                    <p class="dropdown-p"><a href="userEditCurrentPass.php"  class="menu-padding dropdown-a">Edit Password</a></p>
                    <p class="dropdown-p"><a href="userAddSecondaryPass.php"  class="menu-padding dropdown-a">Add Sec. Password</a></p>
                </div>
            </div>  
            <a href="kycVerification.php"  class="white-text opacity-hover menu-margin-right logged-a">
                eKYC
            </a>
            <a href="invitationLink.php" class="white-text opacity-hover menu-margin-right logged-a">Refer</a>
            <a href="logout.php" class="white-text opacity-hover logged-a">Logout</a>  
            <div id="dl-menu" class="dl-menuwrapper logged-in-dl">
                <button class="dl-trigger">Open Menu</button>
                <ul class="dl-menu">
                    <li><a href="index.php#div_refresh">Exchange</a></li>
                    <!-- <li><a  href="publicProject.php">Watchlist</a></li>                                 -->
                    <li><a  href="currentProjectPublic.php">Watchlist</a></li>  
                    <li><a  href="member.php">Info</a></li>
                    <li><a href="userEditCurrentPass.php">Edit Password</a></li>
                    <li><a href="userAddSecondaryPass.php" >Add Secondary<br>Password</a></li>
                    <li><a href="kycVerification.php">eKYC Verification</a></li>
                    <li><a href="userEditEkycDetails.php">Edit eKYC Details</a></li>
                    <li><a href="invitationLink.php">Refer</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div><!-- /dl-menuwrapper -->                      
        </div>
    </div>
</header>

<?php
}
else
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
    <div class="big-container-size hidden-padding">
        <div class="left-logo-div float-left hidden-logo-padding">
            <a href="index.php" class="opacity-hover">
                <img src="img/white-logo.png" class="logo-img web-logo" alt="AIDEX" title="AIDEX">
                <img src="img/aidex.png" class="logo-img mobile-logo" alt="AIDEX" title="AIDEX">
            </a>
        </div>
        <div class="middle-menu-div web-menu">
            <a class="white-text menu-padding opacity-hover" href="publicProject.php">Watchlist</a>
            <a class="white-text opacity-hover" href="index.php#div_refresh">Exchange</a>        	
        </div>
        <div class="right-menu-div float-right">
            <a class="white-text menu-padding opacity-hover open-login web-menu">Login</a>
            <a class="white-text opacity-hover open-signup web-menu">Sign Up</a>
            <div id="dl-menu" class="dl-menuwrapper">
                <button class="dl-trigger">Open Menu</button>
                <ul class="dl-menu">
                    <li><a  href="publicProject.php">Watchlist</a></li>
                    <li><a href="index.php">Exchange</a></li>
                    <li><a class="open-login" >Login</a></li>
                    <li><a class="open-signup">Sign Up</a></li>
                </ul>
            </div><!-- /dl-menuwrapper -->           
        </div>
    </div>
</header>

<?php
}
?>