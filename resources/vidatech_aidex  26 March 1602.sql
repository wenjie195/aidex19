-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2020 at 09:02 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_aidex`
--

-- --------------------------------------------------------

--
-- Table structure for table `companyselection`
--

CREATE TABLE `companyselection` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `estimated_value` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `background` varchar(5000) NOT NULL,
  `founder_evaluation` varchar(5000) DEFAULT NULL,
  `product_evaluation` varchar(5000) DEFAULT NULL,
  `business_model` varchar(255) DEFAULT NULL,
  `industry` varchar(255) DEFAULT NULL,
  `team_evaluation` varchar(255) DEFAULT NULL,
  `market_readiness` varchar(255) DEFAULT NULL,
  `revenues_po` varchar(255) DEFAULT NULL,
  `petri_growth` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companyselection`
--

INSERT INTO `companyselection` (`id`, `company_name`, `estimated_value`, `image`, `background`, `founder_evaluation`, `product_evaluation`, `business_model`, `industry`, `team_evaluation`, `market_readiness`, `revenues_po`, `petri_growth`, `date_created`, `date_updated`) VALUES
(1, 'Haven Clothing', '4000000', 'havan-clothing.png', 'Havan Clothing is a social enterprise that sells minimalistic pocket tees touting designs by underprivileged children. Their goal is to make our customers look good, do good and feel good.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:36:24', '2020-03-26 07:40:17'),
(2, 'Rtist', '5668000', 'rtist.png', 'Rtist is a platform that focuses on highlighting creative talents by making them into listings. They connect companies with the right local creative talent via permanent, contract, freelance or internship.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:36:24', '2020-03-26 07:44:55'),
(3, 'VentureGrab', '4000000', 'venture-grab.png', 'VentureGrab.com is a business buy-sell portal which provides automation matching of investment amount, business category, types and geographical location.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:36:50', '2020-03-26 07:40:24'),
(4, 'Nu Vending', '15000000', 'nu-vending.png', 'NuVending is a Malaysia leading vending machine supplier by offering best quality of vending machine equipped with latest technology.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:36:50', '2020-03-26 07:40:27'),
(5, 'Love18C Sdn Bhd', '11700000', 'love18c.png', 'A handmake chocolate workshop that is much more affordable and tastier than the Japan brand. Currently have stores at Kuala Lumpur and Penang.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:37:19', '2020-03-26 07:40:38'),
(6, 'JOCOM MSHOPPING SDN BHD', '57000000', 'jocom.png', '#1 Malaysia online grocer app, we are specialized in providing grocery shopping online service. It provides a total integrated solution that protects both the consumers and the vendors.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:37:19', '2020-03-26 07:40:41'),
(7, 'Aspire Asia', '7500000', 'aspire-asia.png', 'Alfred connect homeowners with the property management in all stages starting from handing over of vacant possession to day-to-day management tasks. (i.e. submitting defects, facilities booking and any other applications)', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:37:57', '2020-03-26 07:40:43'),
(8, 'Goody Technologies', '18000000', 'goody-technologies.png', 'Goody Technologies Sdn Bhd is the leading New Generation Digital Media Network in Asia that contains No.1 Chinese Media Site, Crowd-sourcing Content Creators Platform，Data Technologies Platform and Digital Advertising Platform that provides online advertising solutions to advertisers.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:37:57', '2020-03-26 07:40:46'),
(9, 'SupaBear', '4760000', 'supabear.png', 'Supabear solve the problems faced by both owner & agent in the property Industry. Owner can submit their property details within few clicks and reach out to thousand of agents. Agents will be notified once there is property submitted within their registered area.', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', 'N/A', '<img src=\"img/star.png\" class=\"star-png\"><img src=\"img/star.png\" class=\"star-png\">', '2020-03-24 08:38:49', '2020-03-26 07:46:57');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) NOT NULL,
  `ch_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `ch_name`, `en_name`, `date_created`, `date_updated`) VALUES
(1, '阿布哈茲', 'Abkhazia', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(2, '阿富汗', 'Afghanistan', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(3, '奥兰', 'Åland', '2020-02-28 02:51:32', '2020-02-28 02:53:16'),
(4, '阿尔巴尼亚', 'Albania', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(5, '阿尔及利亚', 'Algeria', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(6, '美属萨摩亚', 'American Samoa', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(7, '安道尔', 'Andorra', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(8, '安哥拉', 'Angola', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(9, '安圭拉', 'Anguilla', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(10, '安地卡及巴布達', 'Antigua and Barbuda', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(11, '阿根廷', 'Argentina', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(12, '亞美尼亞', 'Armenia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(13, '阿尔扎赫', 'Artsakh', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(14, '阿鲁巴', 'Aruba', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(15, '澳大利亚', 'Australia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(16, '奥地利', 'Austria', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(17, '阿塞拜疆', 'Azerbaijan', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(18, '巴哈马', 'Bahamas', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(19, '巴林', 'Bahrain', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(20, '孟加拉国 孟加拉国', 'Bangladesh', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(21, '巴巴多斯', 'Barbados', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(22, '白俄羅斯', 'Belarus', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(23, '比利時', 'Belgium', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(24, '伯利兹', 'Belize', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(25, '贝宁', 'Benin', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(26, '百慕大', 'Bermuda', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(27, '不丹', 'Bhutan', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(28, '玻利维亚', 'Bolivia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(29, '波斯尼亚和黑塞哥维那', 'Bosnia and Herzegovina', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(30, '博茨瓦纳', 'Botswana', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(31, '巴西', 'Brazil', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(32, '文莱', 'Brunei', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(33, '保加利亚', 'Bulgaria', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(34, '布吉納法索', 'Burkina Faso', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(35, '布隆迪', 'Burundi', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(36, '柬埔寨', 'Cambodia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(37, '喀麦隆', 'Cameroon', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(38, '加拿大', 'Canada', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(39, '佛得角', 'Cape Verde', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(40, '开曼群岛', 'Cayman Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(41, '中非共和国', 'Central African Republic', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(42, '乍得', 'Chad', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(43, '智利', 'Chile', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(44, '中国', 'China', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(45, '圣诞岛', 'Christmas Island', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(46, '科科斯（基林）', 'Cocos (Keeling) Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(47, '哥伦比亚', 'Colombia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(48, '科摩罗', 'Comoros', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(49, '刚果（布）', 'Congo (Brazzaville)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(50, '刚果（金）', 'Congo (Kinshasa)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(51, '庫克群島', 'Cook Islands', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(52, '哥斯达黎加', 'Costa Rica', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(53, '科特迪瓦', 'Côte d\'Ivoire\r\n', '2020-02-28 02:51:37', '2020-02-28 03:38:22'),
(54, '克罗地亚', 'Croatia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(55, '古巴', 'Cuba', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(56, '库拉索', 'Curaçao\r\n', '2020-02-28 02:51:37', '2020-02-28 03:37:19'),
(57, '賽普勒斯', 'Cyprus', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(58, '捷克', 'Czech Republic', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(59, '丹麥', 'Denmark', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(60, '吉布提', 'Djibouti', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(61, '多米尼克', 'Dominica', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(62, '多米尼加', 'Dominican Republic', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(63, '顿涅茨克', 'Donetsk', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(64, '厄瓜多尔', 'Ecuador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(65, '埃及', 'Egypt', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(66, '薩爾瓦多', 'El Salvador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(67, '赤道几内亚', 'Equatorial Guinea', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(68, '厄立特里亚', 'Eritrea', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(69, '爱沙尼亚', 'Estonia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(70, '斯威士兰', 'Eswatini', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(71, '衣索比亞', 'Ethiopia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(72, '福克蘭群島', 'Falkland Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(73, '法罗群岛', 'Faroe Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(74, '斐济', 'Fiji', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(75, '芬兰', 'Finland', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(76, '法國', 'France', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(77, '法屬玻里尼西亞', 'French Polynesia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(78, '加彭', 'Gabon', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(79, '冈比亚', 'Gambia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(80, '格鲁吉亚', 'Georgia', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(81, '德國', 'Germany', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(82, '加纳', 'Ghana', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(83, '直布罗陀', 'Gibraltar', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(84, '希臘', 'Greece', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(85, '格陵兰', 'Greenland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(86, '格瑞那達', 'Grenada', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(87, '關島', 'Guam', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(88, '危地马拉', 'Guatemala', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(89, '根西', 'Guernsey', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(90, '几内亚', 'Guinea', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(91, '几内亚比绍', 'Guinea-Bissau', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(92, '圭亚那', 'Guyana', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(93, '海地', 'Haiti', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(94, '洪都拉斯', 'Honduras', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(95, '香港', 'Hong Kong', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(96, '匈牙利', 'Hungary', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(97, '冰島', 'Iceland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(98, '印度', 'India', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(99, '印尼', 'Indonesia', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(100, '伊朗', 'Iran', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(101, '伊拉克', 'Iraq', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(102, '爱尔兰', 'Ireland', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(103, '马恩岛', 'Isle of Man', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(104, '以色列', 'Israel', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(105, '意大利', 'Italy', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(106, '牙买加', 'Jamaica', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(107, '日本', 'Japan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(108, '澤西', 'Jersey', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(109, '约旦', 'Jordan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(110, '哈萨克斯坦', 'Kazakhstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(111, '肯尼亚', 'Kenya', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(112, '基里巴斯', 'Kiribati', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(113, '科索沃', 'Kosovo', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(114, '科威特', 'Kuwait', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(115, '吉尔吉斯斯坦', 'Kyrgyzstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(116, '老挝', 'Laos', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(117, '拉脫維亞', 'Latvia', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(118, '黎巴嫩', 'Lebanon', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(119, '賴索托', 'Lesotho', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(120, '利比里亚', 'Liberia', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(121, '利比亞', 'Libya', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(122, '列支敦斯登', 'Liechtenstein', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(123, '立陶宛', 'Lithuania', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(124, '卢甘斯克', 'Luhansk', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(125, '卢森堡', 'Luxembourg', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(126, '澳門', 'Macau', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(127, '马达加斯加', 'Madagascar', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(128, '马拉维', 'Malawi', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(129, '马来西亚', 'Malaysia', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(130, '馬爾地夫', 'Maldives', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(131, '马里', 'Mali', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(132, '馬爾他', 'Malta', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(133, '马绍尔群岛', 'Marshall Islands', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(134, '毛里塔尼亚', 'Mauritania', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(135, '模里西斯', 'Mauritius', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(136, '墨西哥', 'Mexico', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(137, '密克羅尼西亞聯邦', 'Micronesia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(138, '摩尔多瓦', 'Moldova', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(139, '摩納哥', 'Monaco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(140, '蒙古國', 'Mongolia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(141, '蒙特內哥羅', 'Montenegro', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(142, '蒙特塞拉特', 'Montserrat', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(143, '摩洛哥', 'Morocco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(144, '莫桑比克', 'Mozambique', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(145, '緬甸', 'Myanmar', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(146, '纳米比亚', 'Namibia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(147, '瑙鲁', 'Nauru', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(148, '尼泊尔', 'Nepal', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(149, '荷蘭', 'Netherlands', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(150, '新喀里多尼亞', 'New Caledonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(151, '新西蘭', 'New Zealand', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(152, '尼加拉瓜', 'Nicaragua', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(153, '尼日尔', 'Niger', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(154, '奈及利亞', 'Nigeria', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(155, '纽埃', 'Niue', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(156, '朝鲜', 'North Korea', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(157, '北馬其頓', 'North Macedonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(158, '北塞浦路斯', 'Northern Cyprus', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(159, '北馬里亞納群島', 'Northern Mariana Islands', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(160, '挪威', 'Norway', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(161, '阿曼', 'Oman', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(162, '巴基斯坦', 'Pakistan', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(163, '帛琉', 'Palau', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(164, '巴勒斯坦', 'Palestine', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(165, '巴拿马', 'Panama', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(166, '巴布亚新几内亚', 'Papua New Guinea', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(167, '巴拉圭', 'Paraguay', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(168, '秘魯', 'Peru', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(169, '菲律賓', 'Philippines', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(170, '皮特凯恩群岛', 'Pitcairn Islands', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(171, '波蘭', 'Poland', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(172, '葡萄牙', 'Portugal', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(173, '德涅斯特河沿岸', 'Pridnestrovie', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(174, '波多黎各', 'Puerto Rico', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(175, '卡塔尔', 'Qatar', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(176, '羅馬尼亞', 'Romania', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(177, '俄羅斯', 'Russia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(178, '卢旺达', 'Rwanda', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(179, '圣巴泰勒米', 'Saint Barthelemy', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(180, '圣基茨和尼维斯', 'Saint Christopher and Nevis', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(181, '圣赫勒拿、阿森松和特里斯坦-达库尼亚', 'Saint Helena, Ascension and Tristan da Cunha', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(182, '圣卢西亚', 'Saint Lucia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(183, '圣皮埃尔和密克隆', 'Saint Pierre and Miquelon', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(184, '圣文森特和格林纳丁斯', 'Saint Vincent and the Grenadines', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(185, '萨摩亚', 'Samoa', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(186, '圣马力诺', 'San Marino', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(187, '聖多美和普林西比', 'São Tomé and Príncipe\r\n', '2020-02-28 02:51:52', '2020-02-28 03:39:40'),
(188, '沙烏地阿拉伯', 'Saudi Arabia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(189, '塞内加尔', 'Senegal', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(190, '塞爾維亞', 'Serbia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(191, '塞舌尔', 'Seychelles', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(192, '塞拉利昂', 'Sierra Leone', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(193, '新加坡', 'Singapore', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(194, '荷屬聖馬丁', 'Sint Maarten', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(195, '斯洛伐克', 'Slovakia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(196, '斯洛維尼亞', 'Slovenia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(197, '所罗门群岛', 'Solomon Islands', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(198, '索馬利亞', 'Somalia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(199, '索馬利蘭', 'Somaliland', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(200, '南非', 'South Africa', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(201, '韩国', 'South Korea', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(202, '南奥塞梯', 'South Ossetia', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(203, '南蘇丹', 'South Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(204, '西班牙', 'Spain', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(205, '斯里蘭卡', 'Sri Lanka', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(206, '苏丹', 'Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(207, '苏里南', 'Suriname', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(208, '斯瓦尔巴', 'Svalbard', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(209, '瑞典', 'Sweden', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(210, '瑞士', 'Switzerland', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(211, '叙利亚', 'Syria', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(212, '中華民國', 'Taiwan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(213, '塔吉克斯坦', 'Tajikistan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(214, '坦桑尼亚', 'Tanzania', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(215, '泰國', 'Thailand', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(216, '梵蒂冈', 'The Holy See（Vatican City）', '2020-02-28 02:51:55', '2020-02-28 03:40:13'),
(217, '东帝汶', 'Timor-Leste', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(218, '多哥', 'Togo', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(219, '托克勞', 'Tokelau', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(220, '汤加', 'Tonga', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(221, '千里達及托巴哥', 'Trinidad and Tobago', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(222, '突尼西亞', 'Tunisia', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(223, '土耳其', 'Turkey', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(224, '土库曼斯坦', 'Turkmenistan', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(225, '特克斯和凯科斯群岛', 'Turks and Caicos Islands', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(226, '图瓦卢', 'Tuvalu', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(227, '乌干达', 'Uganda', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(228, '烏克蘭', 'Ukraine', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(229, '阿联酋', 'United Arab Emirates', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(230, '英國', 'United Kingdom', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(231, '美國', 'United States', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(232, '乌拉圭', 'Uruguay', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(233, '乌兹别克斯坦', 'Uzbekistan', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(234, '瓦努阿圖', 'Vanuatu', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(235, '委內瑞拉', 'Venezuela', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(236, '越南', 'Vietnam', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(237, '英屬維爾京群島', 'Virgin Islands, British', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(238, '瓦利斯和富圖納', 'Wallis and Futuna', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(239, '西撒哈拉', 'Western Sahara', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(240, '葉門', 'Yemen', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(241, '尚比亞', 'Zambia', '2020-02-28 02:51:58', '2020-02-28 02:51:58'),
(242, '辛巴威', 'Zimbabwe', '2020-02-28 02:51:58', '2020-02-28 02:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `ekyc`
--

CREATE TABLE `ekyc` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address_lineone` varchar(255) DEFAULT NULL,
  `address_linetwo` varchar(255) DEFAULT NULL,
  `postcode` int(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `proof_id_type` varchar(255) DEFAULT NULL,
  `ic_front` varchar(255) DEFAULT NULL,
  `ic_back` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ekyc`
--

INSERT INTO `ekyc` (`id`, `uid`, `username`, `firstname`, `lastname`, `email`, `phone_no`, `full_name`, `address_lineone`, `address_linetwo`, `postcode`, `state`, `country`, `proof_id_type`, `ic_front`, `ic_back`, `passport`, `nationality`, `date_created`, `date_updated`) VALUES
(1, '62a78787af9090e2bbb2b328eab0aef7', NULL, 'asdasd', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Aruba', 'Passport', NULL, NULL, 'mmexport1573396227303.jpg', NULL, '2020-03-24 08:18:55', '2020-03-24 08:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `email` varchar(200) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `uid`, `email`, `date_created`, `date_updated`) VALUES
(1, 'fb0753666f4d751e42cc09b5b07597c1', 'test@gmail.com', '2020-03-18 06:27:19', '2020-03-18 06:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `petrirating`
--

CREATE TABLE `petrirating` (
  `id` bigint(20) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `estimated_value` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `background` varchar(5000) NOT NULL,
  `founder_evaluation` varchar(5000) DEFAULT NULL,
  `product_evaluation` varchar(5000) DEFAULT NULL,
  `business_model` varchar(255) DEFAULT NULL,
  `industry` varchar(255) DEFAULT NULL,
  `team_evaluation` varchar(255) DEFAULT NULL,
  `market_readiness` varchar(255) DEFAULT NULL,
  `revenues_po` varchar(255) DEFAULT NULL,
  `petri_growth` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `petrirating`
--

INSERT INTO `petrirating` (`id`, `company_name`, `estimated_value`, `image`, `background`, `founder_evaluation`, `product_evaluation`, `business_model`, `industry`, `team_evaluation`, `market_readiness`, `revenues_po`, `petri_growth`, `date_created`, `date_updated`) VALUES
(1, 'Mypetlibrary', NULL, 'mypetslibrary.png', 'Mypetslibrary is Asia’s 1st established professional platform featuring pets that are able to connect professional sellers and buyers across nationwide.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:18:36'),
(2, 'Samofa', NULL, 'samofa.png', 'Samofa', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:19:10'),
(3, 'Chapter 28', NULL, 'chapter28.png', 'Chapter28 started as a natural cosmetic brand with the intention to promote a vegan campaign. Alice’s strong passion for the concept of natural beauty makes her step into the cosmetic industry and to bring more awareness toward using cosmetics that are fully plants based.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:19:40'),
(4, 'Revenews', NULL, 'revenews.png', 'Revenews has always been focused on the broader views of online revenue-sharing approaches. Revenews has become a leading voice for its dedication to objectivity and its fresh perspective.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:20:25'),
(5, 'Thousand Media', NULL, 'thousand-media.png', 'Thousand Media is an agency that provides unlimited Design/ Content creation and marketing solutions service.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:20:40'),
(6, 'Urban Hyguenist', NULL, 'urban-hygienist.png', 'Urban Hygienist provides an effective cleaning solution to control eczema, allergic rhinitis, and asthma by removing unhygiene stuff in your home with eco-friendly water filter technology.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:20:52'),
(7, 'Zuzudo Kitchen Bar', NULL, 'zuzudo.png', 'Zuzudo Kitchen Bar is a restaurant that serve Pork Free Japanese Food like Ramen, Various Don, Rice, CHicken Gyoza, Sushi and Snacks.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:21:04'),
(8, 'ChiNou Smart Solutions Sdn Bhd', NULL, 'ChiNou-Smart-Solutions.png', 'ChiNou Smart Solutions Sdn Bhd', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:21:14'),
(9, '80+', NULL, '80+.png', '80+ aims to make available specialty coffee capsules so that everyone with in the Asian region can enjoy a cup of inspiring indulgence, anytime, anywhere.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:21:23'),
(10, 'Brandworks', NULL, 'brandworks.png', 'Brandworks helps various brands tell meaningful stories across all of their channels. Brandworks provides digital strategy, channel strategy and campaign delivery.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:21:42'),
(11, 'Chillit Buddy', NULL, 'chillitbuddy.png', 'Chill\'it Buddy can be considered as a social news and content website that gathers and share the latest, trendiest and spiciest news from all around the world.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:21:54'),
(12, 'DCK', NULL, 'dck.png', 'DCK products are designed to satisfy all of their customers automotive needs, anytime, anywhere.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:22:05'),
(13, 'De Xin Guo Ji', NULL, 'dexinguoji.png', 'De Xin Guo Ji helps their clients trade forex with convenient and earn money with confidence.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:22:23'),
(14, 'Design Village', NULL, 'design-village.png', 'Design Village is Penang\'s outlet mall that hosts a broad variety of exciting international and local brands.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:22:36'),
(15, 'FOODee', NULL, 'foodee.png', 'FOODee is an app to help you solve your daily problems on what to eat.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:22:46'),
(16, 'Forever Travel Service', NULL, 'forever-travel-service.png', 'Forever Travel Service believes that travel is an enriching experience that not only frees and relax a person, but also brodens your horizons. Forever Travel Service shares their adventures and travels with you, along with the latest news & offers of Travel & Tour services.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:23:01'),
(17, 'Bworth Fur Kids Home', NULL, 'furkids.png', 'Bworth Fur kids Home is a no-kill, self support shelter, to promote kindness and compassion towards all animals, to encourage a more humane world for the furkids.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:23:39'),
(18, 'GIC Group', NULL, 'gic.png', 'They provides local & overseas properties investment, new projects marketing, investor club, real estate agency and hospitality management services.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:23:54'),
(19, 'IGL Coatings', NULL, 'igl.png', 'IGL Coatings is a coating company that formulates eco-friendly car coatings which is safe and outperform existing products in the market in terms of durability, quality and ease of application.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:24:07'),
(20, 'iSpendtribute', NULL, 'ist.png', 'iSpendtribute is a Social Enterprise that unites our community to cultivate a sharing culture by sustainably contributing to the society through our proprietary Spendtribute cycle.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:24:23'),
(21, 'Loadee', NULL, 'loadee.png', 'Loadee', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:24:38'),
(22, 'Mr Ringgit', NULL, 'mrringgit.png', 'Mr Ringgit provides loan services through app.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:24:48'),
(23, 'Sin-Pen Travel', NULL, 'sin-pen.png', 'Sin-Pen established since 1975. They specialized in domestic & international tour, daily departure coach to Genting, SUPER VIP coach which available for rent.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:25:00'),
(24, 'VogueBeaute', NULL, 'vb.png', 'VogueBeaute believe that beauty does not discriminate. Their products are formulated to suit every shade of skin tone and every unique personality.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:25:14'),
(25, 'World Class Digital Marketing', NULL, 'world-class-digital-marketing.png', 'World Class Digital Marketing', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:25:24'),
(26, 'Yupa', NULL, 'yupa.png', 'Yupa is a platform to let you discover and explore travel experiences, activities and interest where you can begin adventures of your style with the locals.', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', 'Coming Soon', '2020-03-26 02:55:37', '2020-03-26 04:25:34');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', '9a04ea5286dc214d621622e14a858dc4', 'admin', 0, '9a04ea5286dc214d621622e14a858dc4', '2020-03-18 06:28:23', '2020-03-18 06:28:23'),
(2, '62a78787af9090e2bbb2b328eab0aef7', '62a78787af9090e2bbb2b328eab0aef7', 'user1', 0, '62a78787af9090e2bbb2b328eab0aef7', '2020-03-18 06:29:53', '2020-03-18 06:29:53'),
(3, '62a78787af9090e2bbb2b328eab0aef7', '6cb200f4d92368d9f9719d5b76491d3a', 'user2', 1, '62a78787af9090e2bbb2b328eab0aef7', '2020-03-18 06:32:20', '2020-03-18 06:32:20'),
(4, 'bee7f19f17e7bb76b564819de156195f', 'bee7f19f17e7bb76b564819de156195f', 'user3', 0, 'bee7f19f17e7bb76b564819de156195f', '2020-03-20 06:51:50', '2020-03-20 06:51:50'),
(5, '76b2fe8baf9e2fa355c5430829b8029d', '76b2fe8baf9e2fa355c5430829b8029d', 'asd', 0, '76b2fe8baf9e2fa355c5430829b8029d', '2020-03-24 03:10:06', '2020-03-24 03:10:06'),
(6, '73c026a73215d530d8c1e3a4bcb18a19', '73c026a73215d530d8c1e3a4bcb18a19', 'qwe', 0, '73c026a73215d530d8c1e3a4bcb18a19', '2020-03-24 03:16:38', '2020-03-24 03:16:38'),
(7, '7a8cccd4b9e9140de18c8ba5b2883158', '7a8cccd4b9e9140de18c8ba5b2883158', 'asd', 0, '7a8cccd4b9e9140de18c8ba5b2883158', '2020-03-26 01:43:39', '2020-03-26 01:43:39'),
(8, '6087e80383cbd9d60def028ff0ffcc04', '6087e80383cbd9d60def028ff0ffcc04', 'asd', 0, '6087e80383cbd9d60def028ff0ffcc04', '2020-03-26 01:45:05', '2020-03-26 01:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `email_verified` bigint(20) DEFAULT 1,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `password_two` char(64) DEFAULT NULL,
  `salt_two` char(64) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `ekyc_update` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `favorite_project` varchar(255) DEFAULT NULL,
  `petri_rating` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `email_verified`, `password`, `salt`, `password_two`, `salt_two`, `phone_no`, `full_name`, `ekyc_update`, `nationality`, `favorite_project`, `petri_rating`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', 'admin', 'admin@gmail.com', 2, '2bd3fc76b60d220e67a40adb4c4a9be1d2cdde2afb4a406b6ff1c2daad40ea59', 'a221197fcfb97258210864e1175f32f221cdc16f', NULL, NULL, '+6012-7412369', '', NULL, 'Algeria', NULL, NULL, 1, 0, '2020-03-18 06:28:23', '2020-03-20 04:32:19'),
(2, '62a78787af9090e2bbb2b328eab0aef7', 'user1', 'user1@gmail.com', 2, 'afcbce782f93b1df4aa7a6eb3f4aa7bcd300e8908e86f888e851ec7b5708fcaa', '890866c395227af3bf50167143a073d4a48c4fa8', '432949109e600ce91fd53317d8037501ddca12b9021af10b2c0708c34e56627f', 'de8d86e7020a87ae0bff5952be47c3abb1a36da5', '+6012-7788997', '', 'YES', 'Colombia', '1,3', '2,4', 1, 1, '2020-03-18 06:29:53', '2020-03-26 08:01:52'),
(3, '6cb200f4d92368d9f9719d5b76491d3a', 'user2', 'user2@gmail.com', 1, '2d05413a95f29b01a7d503b303e0cb460430a90a7c1cfb8ffa1adab390707a0d', 'c0553a3b877e1762c48ad2ec9e5563faa7f0b55e', NULL, NULL, '+012-7412369', '', 'YES', 'Brazil', NULL, NULL, 1, 1, '2020-03-18 06:32:20', '2020-03-19 08:25:04'),
(4, 'bee7f19f17e7bb76b564819de156195f', 'user3', 'user3@gmail.com', 1, 'f388c933aeda47ff2f4fd47c3f1cd54cdf2dd548ab808cce3104743578b7fa8a', '4f7c36b2fae78e7cb950978e11ac45757ec6c23e', NULL, NULL, '123123', '', NULL, 'Australia', NULL, NULL, 1, 1, '2020-03-20 06:51:50', '2020-03-20 06:51:50'),
(8, '6087e80383cbd9d60def028ff0ffcc04', 'asd', 'asd@gg.cc', 1, '33503ee8c54ebb031ff031e7c39fdd021427f86c9e9d9958a7b925ebbb23579c', 'e3f203b2b24d9dbbf6d372b9dd02179490cbbd24', NULL, NULL, '6012-741741', '', NULL, 'Azerbaijan', NULL, NULL, 1, 1, '2020-03-26 01:45:05', '2020-03-26 01:45:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companyselection`
--
ALTER TABLE `companyselection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ekyc`
--
ALTER TABLE `ekyc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petrirating`
--
ALTER TABLE `petrirating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companyselection`
--
ALTER TABLE `companyselection`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `ekyc`
--
ALTER TABLE `ekyc`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `petrirating`
--
ALTER TABLE `petrirating`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
