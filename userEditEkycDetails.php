<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/Ekyc.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $userEkycRows = getUserEkyc($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $userEkycDetails = $userEkycRows[0];

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/member.php" />
<meta property="og:title" content="Member | Aidex" />
<title>Member | Aidex</title>

<link rel="canonical" href="https://aidex.sg/member.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
 	<div class="width100 overflow same-padding menu-distance min-height-with-menu-distance">
		<div class="two-right-content-div two-left float-left">

        <?php 
        $ekycUpdated = $userDetails->getEkycUpdated();

        if($ekycUpdated == "")
        {?>
            
                <h2 class="tab-h2">Please update Your eKYC details</h2>
        
        <?php
        }
        // else
        elseif($ekycUpdated == 'YES')
        {
        ?>


                <!-- <form action="utilities/registerEkycFunction.php" method="POST" enctype="multipart/form-data"> -->

                <form action="utilities/editEkycFunction.php" method="POST" enctype="multipart/form-data">
                
                    <h2 class="tab-h2">KYC Verification</h2>

                    <div class="input-div">
                        <p class="input-top-text">Country</p>
                        <select class="aidex-input clean" id="edit_ekyc_country" name="edit_ekyc_country" required>

                            <option>Please Select A Country</option>
                            <?php
                            for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                            {
                            ?>
                            <option value="<?php echo $countryList[$cntPro]->getEnName(); 
                            ?>"> 
                            <?php 
                            echo $countryList[$cntPro]->getEnName(); //take in display the options
                            ?>
                            </option>
                            <?php
                            }
                            ?>

                        </select>
                    </div>  

                    <div class="input-div">
                        <p class="input-top-text">Last Name</p>
                        <input class="aidex-input clean" type="text" placeholder="Last Name" id="edit_ekyc_lastname" name="edit_ekyc_lastname" required>
                    </div>  

                    <div class="input-div">
                        <p class="input-top-text">First Name</p>
                        <input class="aidex-input clean" type="text" placeholder="First Name" id="edit_ekyc_firstname" name="edit_ekyc_firstname" required>
                    </div>  

                    <div class="input-div">
                        <p class="input-top-text">Proof of ID</p>
                        <select class="aidex-input clean" onchange="random_function()" id="edit_ekyc_prooftype" name="edit_ekyc_prooftype" required>
                            <option>Please select one type of proof ID</option>
                            <option value="IC">IC</option>
                            <option value="Passport">Passport</option>
                        </select>
                    </div>  

                    <div id="IC" style="display: none">
                        <div class="input-div">
                            <p class="input-top-text">IC Front</p>
                            <input id="file-upload" type="file" name="file_icfront" id="file_icfront" accept="image/*" />      
                        </div>  
                        <div class="input-div">
                            <p class="input-top-text">IC Back</p>
                            <input id="file-upload" type="file" name="file_icback" id="file_icback" accept="image/*" />                    
                        </div> 
                    </div>

                    <div id="Passport" style="display: none">
                        <div class="input-div">
                            <p class="input-top-text">Passport</p>
                            <input id="file-upload" type="file" name="file_passport" id="file_passport" accept="image/*" />                    
                        </div>  
                    </div>
        
                    <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="editSubmit">Submit</button>

                </form>

            </div>    

        <?php
        }
        ?>
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/eKYC.png" class="width100" alt="eKYC" title="eKYC">
        </div>
    </div>

<?php include 'js.php'; ?>

<script>
    function random_function()
    {
        var input = document.getElementById("edit_ekyc_prooftype");

        if(ekyc_prooftype="IC")
        {
            var arr = document.getElementById("IC");
            arr.style.display = input.value == "IC" ? "block" : "none";
        }

        else(ekyc_prooftype="Passport")
        {
            var arr = document.getElementById("Passport");
            arr.style.display = input.value == "Passport" ? "block" : "none";
        }
    }
</script>

</body>
</html>                 