<header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
        	<a href="index.php" class="opacity-hover">
                <img src="img/white-logo.png" class="logo-img web-logo" alt="AIDEX" title="AIDEX">
                <img src="img/aidex.png" class="logo-img mobile-logo" alt="AIDEX" title="AIDEX">
           	</a>
   		</div>
        <div class="middle-menu-div web-menu">
        	<a class="white-text menu-padding opacity-hover" href="publicProject.php">Watchlist</a>
            <a class="white-text opacity-hover" href="index.php#div_refresh">Exchange</a>        	
        </div>
        <div class="right-menu-div float-right">
  

            <a class="white-text menu-padding opacity-hover open-login web-menu">Login</a>
            <a class="white-text opacity-hover open-signup web-menu">Sign Up</a>

                           	<div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a  href="publicProject.php">Watchlist</a></li>
                                  <li><a href="index.php">Exchange</a></li>
                                  <li><a class="open-login" >Login</a></li>
                                  <li><a class="open-signup">Sign Up</a></li>
                                </ul>
							</div><!-- /dl-menuwrapper -->           
            
        </div>
	</div>

</header>