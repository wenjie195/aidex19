<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>
<style media="screen">
body {
font-family: Helvetica Neue, Arial, sans-serif;
font-size: 14px;
color: #444;
}

#chart_div {
min-height: 850px;
}

</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 <div id="chart_div"></div>
<script type="text/javascript">
$(document).ready(function() {
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawChart);
    // https://financialmodelingprep.com/developer/docs
    var url = "https://financialmodelingprep.com/api/v3/historical-price-full/crypto/BTCUSD?serietype=line&serieformat=array&datatype=json";

  function drawChart () {

  $.ajax({
      url: url,
      type: "GET",
      crossDomain: true,
      success: function(response) {

         var graphData = response.historical;

      graphData.forEach((item) => {
                        if(typeof item['date'] === "string"){
                            var split = (item['date']).split("-");
                            item['date'] = new Date(split[0]+'/'+split[1]+'/'+split[2]);
                        }
                        });
                console.log(graphData) ;
        graphData.unshift(['Date', 'Close'])
        var data = google.visualization.arrayToDataTable(graphData);

        var options =  {
                    crosshair: { trigger: 'both',orientation: 'both' },
                    seriesType: "line",
                    colors:['#3365cc'],
                    series: {
                        0: {targetAxisIndex:1 },
                        1:{targetAxisIndex:0, color: '#f1efb8'},
                    },
                    height: 800,
                    backgroundColor: {fill: '#FFFFFF'},
                     chartArea: {
                        height:'95%',
                        width: "90%",
                    },
                    legend: 'none',
                    vAxes: {
                        0: {
                            textStyle: {
                                fontSize: 10,
                                color: 'black'
                            },
                            gridlines: {
                                color: '#dcd7da'
                            },
                        },
                        1: {
                            textStyle: {
                                fontSize: 10,
                                color: 'black'
                            },
                            gridlines: {
                                color: '#dcd7da'
                            },
                        }

                    },
        }

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

        chart.draw(data, options);

      },

      error: function(xhr, status) {
        alert("error");
      }

    });

  }


});

</script>
