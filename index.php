<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/" />
<meta property="og:title" content="ASEAN Investments Digital Exchange | Aidex" />
<title>ASEAN Investments Digital Exchange | Aidex</title>
<link rel="canonical" href="https://aidex.sg/" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'header.php'; ?> -->
<?php include 'header-after-login.php'; ?>
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
    <div class="landing-first-div width100 overflow">
        <div class="first-right-div web-none">
        	<img src="img/ai.jpg" alt="Artificial Intelligence (AI)" title="Artificial Intelligence (AI)" class="ai-img">
        </div>
    	<div class="first-left-div white-text overflow">
        	<h3 class="first-banner-h3 white-text">WELCOME TO</h3>
            <img src="img/white-big-logo.png" alt="AIDEX" title="AIDEX" class="banner-logo">
            <p class="first-banner-p white-text">The power of AIDEX artificial intelligence that revolutionizes ASEAN investments Digital Exchange. Our system allows clients to easily access money and generate returns with user-friendly platform.</p>
        	<a class="open-signup"><div class="blue-bg rounded-200-button white-text">Get Started</div></a>
        </div>
        <div class="first-right-div mobile-none">
        	<img src="img/ai.jpg" alt="Artificial Intelligence (AI)" title="Artificial Intelligence (AI)" class="ai-img">
        </div>

    </div>
    <div class="clear"></div>

    <div id="div_refresh" class="menu-padding-distance">

    </div>

    <div class="clear"></div>

    <div class="second-four-div width100 same-padding">
    	<img src="img/question.png" class="line-icon" alt="Question" title="Question">
        <p class="bold-subtitle-p separate-distance">
        	Join us now to get started.
        </p>
    	<div class="clear"></div>
        <div class="four-div">
        	<img src="img/investment.png" class="four-div-img" alt="Investment" title="Investment">
            <p class="four-div-p">
            	You have money to invest but you know little about crypto. You are not sure where to start.
            </p>
        </div>
        <div class="four-div four-mid-left-div">
        	<img src="img/digital-portfolio.png" class="four-div-img" alt="Digital Portfolio" title="Digital Portfolio">
            <p class="four-div-p">
            	You are increasing your exposure and investment in the digital portfolio.
            </p>
        </div>
        <div class="four-div-tempo-clear"></div>
        <div class="four-div four-mid-right-div">
        	<img src="img/search.png" class="four-div-img" alt="Search" title="Search">
            <p class="four-div-p">
            	You are searching for a digital exchange that provides excellent services and something unique.
            </p>
        </div>
        <div class="four-div">
        	<img src="img/company.png" class="four-div-img" alt="Company" title="Company">
            <p class="four-div-p">
            	You are a company or a group aiming for customization services from the digital exchange.
            </p>
        </div>
    </div>
	<div class="clear"></div>
    <div class="width100 same-padding padding-top-50">
    	<h2 class="line-h2"><img src="img/cryptocurrency.png" class="line-icon line-icon-spacing" alt="Cryptocurrency" title="Cryptocurrency"></h2>
        <div class="clear"></div>
        <div class="two-left-visual-div two-left float-left">
        	<img src="img/cryptocurrency-help.png" class="width100" alt="Help You with Cryptocurrency" title="Help You with Cryptocurrency">
        </div>
        <div class="two-right-content-div two-right float-right">
        	<p class="bold-subtitle-p two-content-top-p">
            	New to cryptocurrency? No worry, most of the people can do it, no doubt you can also do it. More importantly, we're going to help you from scratch.
            </p>
            <p class="two-content-p">
            	We have compiled the best guides by the expert to assist you to understand what is digital assets and how it is going to affect our future view towards money. Once you get the fundamental part, we will guide you through the technical part later one! Stay with us.
            </p>
            <a>
            	<div class="full-width-btn blue-bg blue-btn-hover">
                	Learn More About Cryptocurrency
                </div>
            </a>
        </div>
        <div class="clear"></div>
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/crptocurrency-security.png" class="width100" alt="Security" title="Security">
        </div>
        <div class="two-right-content-div two-left float-left">
        	<p class="bold-subtitle-p two-content-top-p">
            	AIDEX equip you with the ammo to stay in the battleground, and keep your safety regardless of your capabilities.
            </p>
            <p class="two-content-p">
            	We understand our client’s feelings and are looking for the best experience and value in the cryptocurrency exchange. Thus, we created a platform that is intuitive and convenient from deposits, trade to withdrawals.
            </p>
            <a>
            	<div class="full-width-btn blue-bg blue-btn-hover">
                	Know More About Our Features
                </div>
            </a>
        </div>
        <div class="clear"></div>
        <div class="two-left-visual-div two-left float-left">
        	<img src="img/high-security.png" class="width100" alt="High Security" title="High Security">
        </div>
        <div class="two-right-content-div two-right float-right">
        	<p class="bold-subtitle-p two-content-top-p">
            	We treat security with upmost important in our organization.
            </p>
            <p class="two-content-p">
            	To let our customer trade worry-free, we always on our guard and take necessary measures to protect your digital assets in our safe. Not only we have followed the best practices available in the market, but we also take one-step forward to stay ahead of our competitors in terms of security.
            </p>
            <a>
            	<div class="full-width-btn blue-bg blue-btn-hover long-blue-div">
                	Learn More About Our Security Capabilities
                </div>
            </a>
        </div>
        <div class="clear"></div>
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/cryptocurrency-assist.png" class="width100" alt="Assist" title="Assist">
        </div>
        <div class="two-right-content-div two-left float-left">
        	<p class="bold-subtitle-p two-content-top-p">
            	We will be here when you need our assist!
            </p>
            <p class="two-content-p">
            	Indeed, it is painful that exchanges have slow responsive customer service. In AIDEX, we deliver our top-notch services to you in the shortest possible time frame so that you'll gain the most out of our platform!
            </p>
            <a href="#">
            	<div class="full-width-btn blue-bg blue-btn-hover">
                	Visit Our Support Center
                </div>
            </a>
        </div>



    </div>
    <div class="clear"></div>
    <div class="width100 same-padding text-center some-spacing" id="email-div">
    	<img src="img/stay-tune.png" class="line-icon" alt="Stay Tuned" title="Stay Tuned" >
        <p class="bold-subtitle-p">
        	Stay Tuned!
        </p>
        <div class="two-left-visual-div two-left float-left margin-control">
        	<img src="img/member.png" class="width100" alt="Stay Tuned!" title="Stay Tuned!">
        </div>
        <div class="two-right-content-div two-right float-right text-left margin-control">
        	<p class="stay-tune-p">
            	Our website is COMING SOON.<br>We're working our best to create a website beyond your imagination.<br>
                We'll notify you once we are ready to launch the site.<br><br>
                If you have any query, please contact us at <b>support@aidex.sg</b><br>
                We're more than happy to receive your suggestion for our future improvement.<br><br><br>
                Best,<br>
                The AIDEX Team
            </p>
        	<!-- <form> -->
            <form action="utilities/addNewEmailFunction.php" method="POST">
             	<div class="input-div">
                    <p class="input-top-text">Email</p>
                    <!-- <input class="aidex-input clean" type="email" placeholder="Type Your Email"> -->
                    <input class="aidex-input clean" type="text" placeholder="Type Your Email" id="submit_email" name="submit_email" required>
                </div>

                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="refereeButton">Submit</button>

            </form>
        </div>

    </div>
	<div class="clear"></div>

    <div class="clear"></div>
    <div class="spacing-div"></div>


 <!-- Contact Admin Modal -->
<div id="contactadmin-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-contactadmin">&times;</span>
    <div class="clear"></div>
    <h2 class="blue-h2">Contact Admin</h2>
	<p class="contact-admin-p text-center">support@aidex.sg</p>
  </div>

</div>
<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>



<script type="text/javascript">
  $(document).ready(function(){
   $("#div_refresh").load("userTabs.php");
      setInterval(function() {
          $("#div_refresh").load("userTabs.php");
      }, 1000);
  });
  </script>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Admin page is under progressing !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Please verify your email !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Opps, something goes wrong !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Incorrect password";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "You have not register yet";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully ! <br> Please go your registered email for account verification ! ";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Registered, Fail to upload to to referral history !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to register";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Register details has been used by others !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Password length must more than 5 !";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Password and Re-type Password must be same !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Unable to get referral history data of sponsor !";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Unable to find related data sponsor ID !";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "sponsor ID is unavailible !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Reset password link has been sent to your email !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "No user with ths email !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Wrong email format !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 4)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfullly changed password !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Unable to change password !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Password and Re-type Password must be same !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Password length must more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong verification code !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
    if($_SESSION['messageType'] == 5)
    {
        if($_GET['type'] == 1)
        {
            // $messageType = "Register Successfully ! <br> Please go your registered email for account verification ! ";
            $messageType = "Register Successfully ! <br> Please go your registered email for account verification ! <br> Please check your spam mail box if you can't get the email in your inbox !";
        }
        echo '
        <script>
            putNoticeJavascript("Congratulations !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>
