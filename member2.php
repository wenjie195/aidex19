<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CompanySelection.php';
require_once dirname(__FILE__) . '/classes/PetriRating.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

// $companyDetails = getCompanySelection($conn);
// $aaRows = getCompanySelection($conn," WHERE estimated_value = '4000000' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/member.php" />
<meta property="og:title" content="Member | Aidex" />
<title>Member | Aidex</title>

<link rel="canonical" href="https://aidex.sg/member.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>

<div class="width100 overflow same-padding min-height-with-menu-distance menu-distance">
    <h2 class="line-h2"><img src="img/details.png" class="line-icon line-icon-spacing" alt="Info" title="Info"></h2>
    <div class="clear"></div>
    <div class="width100 profile-four-div-container">

    	<div class="shadow-white-div four-div">
        	<img src="img/coin.png" class="four-div-img" alt="UID" title="UID">
            <p class="four-div-small-p">
            	UID
            </p>
            <p class="four-div-big-p2">
            	<?php echo date('Ymd', strtotime($userDetails->getDateCreated()));?><?php echo $userDetails->getId();?>
            </p>
        </div>
    	<div class="shadow-white-div four-div four-mid-left-div">
        	<img src="img/username.png" class="four-div-img" alt="Username" title="Username">
            <p class="four-div-small-p">
            	Username
            </p>
            <p class="four-div-big-p2">
            	<?php echo $userDetails->getUsername();?>
            </p>
        </div>
    	<div class="four-div-tempo-clear"></div>
        <div class="shadow-white-div four-div four-mid-right-div">
        	<img src="img/email.png" class="four-div-img" alt="Email" title="Email">
            <p class="four-div-small-p">
            	Email
            </p>
            <p class="four-div-big-p2">
            	<?php echo $userDetails->getEmail();?>
            </p>
        </div>
    	<div class="shadow-white-div four-div">
        	<img src="img/digital-portfolio.png" class="four-div-img" alt="Mobile Number" title="Mobile Number">
            <p class="four-div-small-p">
            	Mobile Number
            </p>
            <p class="four-div-big-p2">
            	<?php echo $userDetails->getPhoneNo();?>
            </p>
        </div>
    </div>



    <div class="clear"></div>

	<h2 class="tab-h2 text-center some-margin-top2">My Watchlists</h2>

    <div class="tab">
      <button class="tablinks active" onclick="openList(event, 'Listed')">Public</button>
      <button class="tablinks" onclick="openList(event, 'Unlisted')">Petri Rating</button>
    </div>

    <div id="Listed" class="tabcontent" style="display:block;">
    	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>                                                  
                    </thead>
                </tr>
               <tr>
                    <td class="th0-color color-td company-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th7-color color-td"></td>                         
                    <td class="th8-color color-td"></td>
                </tr>           
                <tr>
                    <td class="th0 company-td">Companies</td>
                    <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                    <td class="th2" title="Details about the products">Product Evaluation</td>
                    <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                    <td class="th4" title="Internal & External analysis">Industry</td>
                    <td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                    <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                    <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                    <td class="th8" title="Overall growth of company">Petri Growth</td>                
                </tr>

                <?php $conn = connDB() ?>
                <?php if ($userDetails->getFavoriteProject()) 
                {
                    $favoriteProjectExp = explode(",",$userDetails->getFavoriteProject());
                    for ($i=0; $i <count($favoriteProjectExp) ; $i++)
                    {
                        $companySelectionDet = getCompanySelection($conn," WHERE id =?",array("id"),array($favoriteProjectExp[$i]), "s");
                        if ($companySelectionDet) 
                        {?>
                            <tr>
                                <td class="company-td">
                                    <div class="left-logo-div1">
                                        <!-- <img src="img/havan-clothing.png" class="company-logo" alt="Havan Clothing is a social enterprise that sells minimalistic pocket tees touting designs by underprivileged children. Their goal is to make our customers look good, do good and feel good." title="Havan Clothing is a social enterprise that sells minimalistic pocket tees touting designs by underprivileged children. Their goal is to make our customers look good, do good and feel good."> -->
                                        <!-- <img src="img/havan-clothing.png" class="company-logo"> -->
                                        <img src="public_img/<?php echo $companySelectionDet[0]->getImage();?>" class="company-logo">
                                        <?php $companySelectionDet[0]->getImage();?>
                                    </div>
                                    <div class="right-name-div">
                                        <b class="font-weight900" title="<?php echo $companySelectionDet[0]->getBackground();?>"><?php echo $companySelectionDet[0]->getCompanyName();?></b><p class="ev-p">EV: RM<?php echo $companySelectionDet[0]->getEstimatedValue();?></p>
                                        <!-- <?php //echo $companySelectionDet[0]->getCompanyName();?><br>
                                        EV : RM<?php //echo $companySelectionDet[0]->getEstimatedValue();?> -->
                                    </div>
                                    <!-- <div class="right-star-div checkbox1">
                                    </div>	 -->
                                </td>
                                <td><?php echo $companySelectionDet[0]->getFounderEvaluation();?></td>
                                <td><?php echo $companySelectionDet[0]->getProductEvaluation();?></td>
                                <td><?php echo $companySelectionDet[0]->getFounderEvaluation();?></td>
                                <td><?php echo $companySelectionDet[0]->getProductEvaluation();?></td>
                                <td><?php echo $companySelectionDet[0]->getFounderEvaluation();?></td>
                                <td><?php echo $companySelectionDet[0]->getProductEvaluation();?></td>
                                <td><?php echo $companySelectionDet[0]->getFounderEvaluation();?></td>
                                <td><?php echo $companySelectionDet[0]->getProductEvaluation();?></td>
                                <!-- <td class="two-stars-color star-css"><img src="img/star.png" class="star-png"><img src="img/star.png" class="star-png"></td> -->
                            </tr> 
                        <?php
                        }
                    }
                }
                ?>
            </table>
        </div>        
    </div>

    <div id="Unlisted" class="tabcontent">
          	<div class="table-scroll-div">
            <table class="company-name-table">
            	<tr>
                	<thead>
                    	<th class="th0 company-td"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1">Petri Rating Criteria</th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th1"></th>
                        <th class="th7"></th>
                        <th class="th8"></th>                                                
                    </thead>
                </tr>
                <tr>
                    <td class="th0-color color-td company-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th1-color color-td"></td>
                    <td class="th7-color color-td"></td>                         
                    <td class="th8-color color-td"></td>
                </tr>           
                <tr>
                    <td class="th0 company-td">Companies</td>
                    <td class="th1" title="Evaulate on founder abilities, skills, experience on managing the business">Founder Evaluation</td>
                    <td class="th2" title="Details about the products">Product Evaluation</td>
                    <td class="th3" title="Potential of the business model to growth">Business Model Evaluation</td>
                    <td class="th4" title="Internal & External analysis">Industry</td>
                    <td class="th5" title="Team's background and qualification evaluation">Team Evaluation</td>
                    <td class="th6" title="Product ready to market or not">Market Readiness Evaluation</td>
                    <td class="th7" title="Financial situation">Revenues Performance Outlook</td>
                    <td class="th8" title="Overall growth of company">Petri Growth</td>                
                </tr>
                <!-- <tr>
                    <td class="company-td">
                    <div class="left-logo-div1">
                        <img src="img/mypetslibrary.png" class="company-logo" alt="Mypetlibrary" title="Mypetslibrary is Asia’s 1st established professional platform featuring pets that are able to connect professional sellers and buyers across nationwide.">
                    </div>
                    <div class="right-name-div">
                        <b class="font-weight900" title="Mypetslibrary is Asia’s 1st established professional platform featuring pets that are able to connect professional sellers and buyers across nationwide.">Mypetlibrary</b>
                    </div>
                    </td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                    <td>Coming Soon</td>
                </tr> -->

                <?php $conn = connDB() ?>
                <?php if ($userDetails->getPetriRating()) 
                {
                    $petriRateExp = explode(",",$userDetails->getPetriRating());
                    for ($i=0; $i <count($petriRateExp) ; $i++)
                    {
                        $petriSelectionDet = getPetriRating($conn," WHERE id =?",array("id"),array($petriRateExp[$i]), "s");
                        if ($petriSelectionDet) 
                        {
                        ?>
                            <tr>
                                <td class="company-td">
                                    <div class="left-logo-div1">
                                        <img src="petrirate_img/<?php echo $petriSelectionDet[0]->getImage();?>" class="company-logo" alt="<?php echo $petriSelectionDet[0]->getCompanyName();?>" title="<?php echo $petriSelectionDet[0]->getBackground();?>">
                                    </div>
                                    <div class="right-name-div">
                                        <b class="font-weight900" title="<?php echo $petriSelectionDet[0]->getBackground();?>"><?php echo $petriSelectionDet[0]->getCompanyName();?></b>
                                    </div>
                                </td>
                                <td>Coming Soon</td>
                                <td>Coming Soon</td>
                                <td>Coming Soon</td>
                                <td>Coming Soon</td>
                                <td>Coming Soon</td>
                                <td>Coming Soon</td>
                                <td>Coming Soon</td>
                                <td>Coming Soon</td>
                            </tr> 
                        <?php
                        }
                    }
                }
                ?>

            </table>
        </div>        
    </div>

<!-- 
    <div class="width100 profile-four-div-container">

            <p class="four-div-small-p">
                Favorite Project
            </p>
            <p class="four-div-big-p2">
                <?php $conn = connDB() ?>
                <?php if ($userDetails->getFavoriteProject()) 
                {
                    $favoriteProjectExp = explode(",",$userDetails->getFavoriteProject());
                    for ($i=0; $i <count($favoriteProjectExp) ; $i++)
                    {
                        $companySelectionDet = getCompanySelection($conn," WHERE id =?",array("id"),array($favoriteProjectExp[$i]), "s");
                        if ($companySelectionDet) 
                        {
                            echo $companySelectionDet[0]->getCompanyName()."<br>";
                            echo $companySelectionDet[0]->getEstimatedValue()."<br>";
                        }
                    }
                }
                ?>
            </p>

    </div> -->

</div>
<style>
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background:#e8eef7; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #bed6ff; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #bed6ff; 
}	
</style>
<?php include 'js.php'; ?>

<script>
function openList(evt, listName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(listName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

</body>
</html>
